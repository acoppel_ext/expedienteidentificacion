-- Function: fnagregarenviocorreo(character varying, character varying, integer, character varying, integer, integer)

-- DROP FUNCTION fnagregarenviocorreo(character varying, character varying, integer, character varying, integer, integer);

CREATE OR REPLACE FUNCTION fnagregarenviocorreo(
    character varying,
    character varying,
    integer,
    character varying,
    integer,
    integer)
  RETURNS integer AS
$BODY$
	DECLARE
	cCurp			ALIAS FOR $1;
	cNss	 		ALIAS FOR $2;
	iTiposolicitud		ALIAS FOR $3;
	cCorreo			ALIAS FOR $4;
	iBandnotificaciones	ALIAS FOR $5;
	iBandedocta		ALIAS FOR $6;
	iRetorno int4;
BEGIN
	iRetorno=0;
	IF NOT EXISTS(SELECT curp,nss,tiposolicitud,correo,bandnotificaciones,bandedocta FROM tbcontrolenviocorreo 
			where curp=cCurp AND nss=cNss AND tiposolicitud=iTiposolicitud AND correo=cCorreo AND bandnotificaciones=iBandnotificaciones
			AND bandedocta=iBandedocta) THEN
		INSERT INTO tbcontrolenviocorreo(curp,nss,tiposolicitud,correo,bandnotificaciones,bandedocta)
		values(cCurp,cNss,iTiposolicitud,cCorreo,iBandnotificaciones,iBandedocta);
		iRetorno=1;
	ELSE
		iRetorno=2;
	END IF;
	RETURN iRetorno;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION fnagregarenviocorreo(character varying, character varying, integer, character varying, integer, integer)
  OWNER TO sysaforeglobal;
