var beneficiarios = {};
var iBoton = 0;
var iNumeroEmpleado = 0;
var cFolio = '';
var iTipoServicio = 0;
var iExisteEi = 0;
var sNss = "";
var cCurp = "";
var iTipoSolicitante = 0;
var iPromotorActivoSol = 0;
var iClaveConsarSol = '';
var cEsExtranjero = "";
var iRenapo = 0;
//arreglo con el codigo ascii de las teclas espacio, backspace, tab, etc.
var arrCaracteresPermitidos = [0, 46, 127, 8, 9, 37, 38, 32, 39, 40, 180, 190, 209, 241];
var arrCaracteresPermitidosCorreo = [0, 46, 95, 8, 45];
var dtFechaServicio = "";

//Bandera del domicilio laboral para identificar cuando venga incompleto.
var bDomicilioLaboral = false;

//Variable para verificar en que estatus se encuentra el expediente de identificación.
var iEstatusExpediente = 0;


//Variables para WS de Consulta de CURP por Datos RENAPO.
var cApellidoPaterno = "";
var cApellidoMaterno = "";
var cNombres = "";
var cSexo = "";
var cFechaNacimiento = "";
var iSlcEntidadNacimiento = "";
var arrEntidadNacimiento = [];

//Variable respuesta WS.
var iFolioAfore = 0;

//Variable bandera de exito en consulta respuesta.
var iRespuestaRenapo = -1;
var iTipoError = -1;
var sDecsError = "";
var mensaje = "";
var iSubProceso = 0;
var iVueltaRenapo = 0;
var codigopostal = 0;
var bProcesar = 0;
var cProcesar = 0;
var bBancoppel = 3;

/*Variable contador para diagnostico 1-14*/
var contWS114 = 0;
var chBoxTrabajadorfallecido = 0;

//Variables del Folio 1514
var iBandedocta = 0;
var iBandnotificaciones = 0;
var cCorreo = "";

// MOVIL
// Instruccion que permite conocer el sistema operativo de donde ingresa el usuario
var OSName = "Desconocido";
if (navigator.appVersion.indexOf("Win") != -1) OSName = "Windows";
if (navigator.appVersion.indexOf("Mac") != -1) OSName = "MacOS";
if (navigator.appVersion.indexOf("X11") != -1) OSName = "UNIX";
if (navigator.appVersion.indexOf("Linux") != -1) OSName = "Linux";
if (navigator.appVersion.indexOf("Android") != -1) OSName = "Android";

var opcionEnrol = 1;

//FOLIO 1514 bandera validacion correo
var bandCorreoValido = false;
var bandCorreoConfirmado = false;

//Instruccion que permite el no copiar y pegar con el mause
document.oncontextmenu = function () { return false }

$(document).ready(function () {
	prevenirPaste();
	ocultarBotones();
	var stateObject = {};
	var title = "Expediente de Identificaci&oacute;n";
	var newUrl = "indexExpedienteIdentificacion.php";
	var divRespuestaRenapo 	= document.getElementById('divRespuestaRenapo');
	var addRespuesta = '';
//	$("#btnCapturaBeneficiario").hide();
	//$("#contenedorPrincipal").hide();

	/*Obtener Variables pasadas por URL*/
	if (OSName == "Android") {
		iNumeroEmpleado = getParameterByName("numEmp");
		cFolio = getParameterByName("folio");
		iTipoServicio = getParameterByName("tipoServicio");
		iExisteEi = getParameterByName("existeEi");
		iRenapo = getParameterByName("renapo");
		if(iRenapo == 1)
		{
			divRespuestaRenapo.innerHTML = '';
			addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
			divRespuestaRenapo.innerHTML = addRespuesta;
		}
		/*$("body").on("keyup", "#txtCorreoElectronico", function (event)
		{
			var texto = document.getElementById("txtCorreoElectronico").value;
			texto = texto.toString();

			var expReg = /^(?=[a-zA-Z0-9]{0,20}[-._]{0,1}[a-zA-Z0-9]{0,20}$)[^]*$/;

			if (expReg.test(texto))
			{
				if($(this).val().length >= 40)
				{
					$(this).val($(this).val().substr(0, 40));

					if(event.which != 8 && event.which != 0)
					{
						event.preventDefault();
					}
				}
			}
			else
			{
				var texto = $("#txtCorreoElectronico").val().replace(/[a-zA-Z0-9][-._]{0,1}/gi, '');
				$("#txtCorreoElectronico").val(texto);
			}
		});*/


	} else {
		
		iNumeroEmpleado = getQueryVariable("numEmp");
		cFolio = getQueryVariable("folio");
		iTipoServicio = getQueryVariable("tipoServicio");
		iExisteEi = getQueryVariable("existeEi");
		iRenapo = getQueryVariable("renapo");
		if(iRenapo == 1)
		{
			divRespuestaRenapo.innerHTML = '';
			addRespuesta += '<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/RenapoExitoso.png" width = "132" height = "28"></input>';
			divRespuestaRenapo.innerHTML = addRespuesta;
		}
		
	}

	history.replaceState(stateObject, title, newUrl);
	if (iNumeroEmpleado == 0 || iNumeroEmpleado == null) {
		mensajeAdvertencia("Usted no es un promotor con clave CONSAR");
	}
	else {
		cargaPagina();
		validarPerfilPromotor(iNumeroEmpleado);
	}
	
	$("#slcCorreoElectronico").change(function (evento) {
		islcCorreo = $("#slcCorreoElectronico").val();
		$('#opcion7').removeAttr('disabled');
		$('#opcion8').removeAttr('disabled');
		if (islcCorreo == 7)
		{
			if(bProcesar == 1)
			{
				if(cProcesar == 0)
				{
					//alert("entro");
					$("#slcCorreoElectronico").hide();
					$("#ocultarotro").append("<input type='text' class='CajaTexto' id='txtOtroDominio' onchange='habilitaConfirmacionCorreo()' Placeholder='ESPECIFICAR DOMINIO'/>");	
						$("#txtOtroDominio").keyup(function(e)
						{
							var code = (e.keyCode ? e.keyCode : e.which);
							if(code == 27)
							{
								$("#slcCorreoElectronico").show();
								$("#txtOtroDominio").remove();
								$("#txtOtroDominioConfirm").remove();
								$("#slcCorreoElectronicoConfirma").show();
								$("#slcCorreoElectronico").val("-1");
								$("#slcCorreoElectronicoConfirma").val("-1");
								
								habilitaConfirmacionCorreo();
							}
						});
					
					cProcesar = 1;
				}
				else
				{
					
				}
			}
			else
			{
				$("#slcCorreoElectronico").hide();
				$("#ocultarotro").append("<input type='text' class='CajaTexto' id='txtOtroDominio' onchange='habilitaConfirmacionCorreo()' Placeholder='ESPECIFICAR DOMINIO'/>");	
					$("#txtOtroDominio").keyup(function(e)
					{
						var code = (e.keyCode ? e.keyCode : e.which);
						if(code == 27)
						{
							$("#slcCorreoElectronico").show();
							$("#txtOtroDominio").remove();
							$("#txtOtroDominioConfirm").remove();
							$("#slcCorreoElectronicoConfirma").show();
							$("#slcCorreoElectronico").val("-1");
							$("#slcCorreoElectronicoConfirma").val("-1");
							habilitaConfirmacionCorreo();
						}
					});
			}
		}
		else if (islcCorreo >= 0 && islcCorreo <= 6) {
			$("#txtOtroDominio").val("");
			$("#txtOtroDominio").css('visibility', 'hidden');
			$("#txtOtroDominio").prop("disabled", true);
		}
	});
	
	
	
	$("#slcCorreoElectronicoConfirma").click(function(){
		
		if($("#txtOtroDominioConfirm").val() == undefined && $("#slcCorreoElectronico").val() == 7)
		{
			$("#slcCorreoElectronicoConfirma").val("7");
		    $("#slcCorreoElectronicoConfirma").hide();
			$("#confirm2").append("<input type='text' class='CajaTexto' id='txtOtroDominioConfirm' onchange='validarConfirmacionCorreo()' Placeholder='ESPECIFICAR DOMINIO'/>");		
		}
		
	});
	
	

	
	
	

	$("#slcCorreoElectronicoConfirma").change(function (evento) {

		islcCorreo = $("#slcCorreoElectronicoConfirma").val();
		if (islcCorreo == 7)
		{
			if(bProcesar == 1)
			{
				if(cProcesar == 0)
				{
					//alert("entro");
					$("#slcCorreoElectronicoConfirma").hide();
					$("#confirm2").append("<input type='text' class='CajaTexto' id='txtOtroDominioConfirm' onchange='validarConfirmacionCorreo()' Placeholder='ESPECIFICAR DOMINIO'/>");	
					cProcesar = 1;
				}
				else
				{
					
				}
			}
			else
			{
				$("#slcCorreoElectronicoConfirma").hide();
				$("#confirm2").append("<input type='text' class='CajaTexto' id='txtOtroDominioConfirm' onchange='validarConfirmacionCorreo()' Placeholder='ESPECIFICAR DOMINIO'/>");	
			}
		}
		else if (islcCorreo >= 0 && islcCorreo <= 6) {
			$("#txtOtroDominioConfirm").val("");
			$("#txtOtroDominioConfirm").css('visibility', 'hidden');
			$("#txtOtroDominioConfirm").prop("disabled", true);
		}
	});

	$("#slcCorreoElectronicoConfirm").change(function (evento) {

		islcCorreo = $("#slcCorreoElectronicoConfirm").val();

		if (islcCorreo == 7) {
			$("#txtOtroDominioConfirm").val();
			$("#txtOtroDominioConfirm").css('visibility', 'visible');
			$("#txtOtroDominioConfirm").prop("disabled", false);
		}
		else if (islcCorreo >= 0 && islcCorreo <= 6) {
			$("#txtOtroDominioConfirm").val("");
			$("#txtOtroDominioConfirm").css('visibility', 'hidden');
			$("#txtOtroDominioConfirm").prop("disabled", true);
		}
	});

	$('#txtCorreoElectronico,#txtCorreoElectxtCorreoEletronicoConfirm,#txtOtroDominio,#txtOtroDominioConfirm').bind('change', aMinusculas);

	//Convierte a minúsculas el valor de la caja de texto llama al metodo
	function aMinusculas(e)
	{
		var sCadena = $( e.currentTarget ).val();

		sCadena = sCadena.toLowerCase();

		$( e.currentTarget ).val(sCadena);
	}

	$("#btnBuscarCodigoPostal").click(function(){
		consultarColonia();
	});

	$("#btnBuscarCodigoPostalLaboral").click(function () {
		consultarColoniaLaboral();
	});

	$("#btnGuardar").click(function () {
		var dtFechaNacimiento = $("#txtFechaNacimiento").val();
		var cFechaIdentificacionOficial = "";
		var cFechaComprobante = $("#dtFechaComprobante").val();
		var iTipoIdentificacion = $("#slcIdentificacionOficial").val() == 5 ? true : false;
		var cTelefono1 = $("#txtTel1").val();
		var cTelefono2 = $("#txtTel2").val();
		var cRfc = "";
		cRfc = $.trim($('#txtRfc').val());
		cRfc = cRfc.toUpperCase();
		document.getElementById("txtRfc").value = cRfc;
		//obtener el estatus del expediente
		validarExpedientePublicado();

		if (datosGenerales()) {
			if (validarRfc(cRfc)) {
				if (validaFechaDDMMAAAA(dtFechaNacimiento)) {
					if (datosNivelestudioOcupacionActividadEconomica()) {
						if (datosContactacion()) {
							if (datosDomicilio()) {
								if (datosDomicilioLaboral()) {
									//Valida que todos los telefonos capturados sean diferentes
									if (validarTelefonos(iTipoServicio)) {
										if(validarCamposSolicitante() && validarCamposPromotorActivo()){ //893.1
											if (iTipoServicio > 33) {
												if (datosDocumentacion()) {
													if ($("#slcComprobanteDomicilio").val() == 1) {
														if (iEstatusExpediente == 0) {
															if (cEsExtranjero == '1') {
																if ($("#slcNacionalidad").val() != 1) {
																	guardarDatos();
																}
																else {
																	mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																}
															} else {
																guardarDatos();
															}
														}
														else {
															mensajeAdvertenciaFinal("El expediente para este cliente ya fue publicado.");
														}
													}
													else {
														if (validaFechaDDMMAAAA(cFechaComprobante)) {
															if (iEstatusExpediente == 0) {
																if (cEsExtranjero == '1') {
																	if ($("#slcNacionalidad").val() != 1) {
																		guardarDatos();
																	}
																	else {
																		mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																	}
																} else {
																	guardarDatos();
																}
															}
															else {
																mensajeAdvertenciaFinal("El expediente para este cliente ya fue publicado.");
															}
														}
														else {
															mensajeAdvertencia("Favor de proporcionar una fecha de emisi&oacute;n correcta.");
														}
													}
												}
											}
											else {
												iBoton = 1;
												iRetorno = obtenerExcepcionEnrolamiento(cFolio);
	
												if (iRetorno >= 0) {
	
													iRetornoExcepcion = obtenerExcepcionSolicitante(cFolio);
													console.log('excepción ' + iRetornoExcepcion);
													if (iRetornoExcepcion > 0) {
														if (cEsExtranjero == '1') {
															if ($("#slcNacionalidad").val() != 1) {
																guardarDatos();
															}
															else {
																mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
															}
														} else {
															guardarDatos();
														}
														console.log('funcion de guardado con excepción');
													}
													else {
														if (OSName == "Android") {
															// FALTA GUARDAR HUELLAS EN TABLA AFOTEMPLATECLIENTE
															if (cEsExtranjero == '1') {
																if ($("#slcNacionalidad").val() != 1) {
																	guardarDatos();
																}
																else {
																	mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																}
															} else {
																guardarDatos();
															}
														} else {
															if (iRetorno != 4 && iRetorno != 3) {
																if (cEsExtranjero == '1') {
																	if ($("#slcNacionalidad").val() != 1) {
																		opcionEjecuta();
																	}
																	else {
																		mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																	}
																} else {
																	opcionEjecuta();
																}
															}
															else {
																if (cEsExtranjero == '1') {
																	if ($("#slcNacionalidad").val() != 1) {
																		guardarDatos();
																	}
																	else {
																		mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																	}
																} else {
																	guardarDatos();
																}
															}
														}
													}
												}
												else {
													console.log("Promotor: Ocurrio un error al consultar el tipo enrolamiento.");
													opcionEjecuta();
												}
											}
										}
									}

								}
							}
						}
					}
				}
				else {
					mensajeAdvertencia("La fecha de nacimiento del trabajador no es valida");
				}
			}
			else {
				mensajeAdvertencia("La estructura del RFC es inv&aacute;lida");
			}
		}
		else {
			if (datosGenerales()) {
				if (validarRfc(cRfc)) {
					if (validaFechaDDMMAAAA(dtFechaNacimiento)) {
						if (datosNivelestudioOcupacionActividadEconomica()) {
							if (datosContactacion()) {
								if (datosDomicilio()) {
									if (datosDomicilioLaboral()) {
										//Valida que todos los telefonos capturados sean diferentes
										if (validarTelefonos(iTipoServicio)) {
											if(validarCamposSolicitante() && validarCamposSolicitante()){ //893.1
												if (iTipoServicio > 33) {
													if (datosDocumentacion()) {
														if ($("#slcComprobanteDomicilio").val() == 1) {
															if (iEstatusExpediente == 0) {
																if (cEsExtranjero == '1') {
																	if ($("#slcNacionalidad").val() != 1) {
																		guardarDatos();
																	}
																	else {
																		mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																	}
																} else {
																	guardarDatos();
																}
															}
															else {
																mensajeAdvertenciaFinal("El expediente para este cliente ya fue publicado.");
															}
														}
														else {
															if (validaFechaDDMMAAAA(cFechaComprobante)) {
																if (iEstatusExpediente == 0) {
																	if (cEsExtranjero == '1') {
																		if ($("#slcNacionalidad").val() != 1) {
																			guardarDatos();
																		}
																		else {
																			mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																		}
																	} else {
																		guardarDatos();
																	}
																}
																else {
																	mensajeAdvertenciaFinal("El expediente para este cliente ya fue publicado.");
																}
															}
															else {
																mensajeAdvertencia("Favor de proporcionar una fecha de emisi&oacute;n correcta.");
															}
														}
													}
												}
												else {
													iBoton = 1;
													iRetorno = obtenerExcepcionEnrolamiento(cFolio);
													if (iRetorno >= 0) {
	
														if (iRetorno != 4 && iRetorno != 3) {
															if (cEsExtranjero == '1') {
																if ($("#slcNacionalidad").val() != 1) {
																	opcionEjecuta();
																}
																else {
																	mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																}
															} else {
																opcionEjecuta();
															}
														}
														else {
															if (cEsExtranjero == '1') {
																if ($("#slcNacionalidad").val() != 1) {
																	guardarDatos();
																}
																else {
																	mensajeAdvertencia("Promotor la nacionalidad seleccionada no corresponde con la del trabajador.");
																}
															} else {
																guardarDatos();
															}
														}
													}
													else {
														console.log("Promotor: Ocurrio un error al consultar el tipo enrolamiento.");
														opcionEjecuta();
													}
												}
											}
										}
									}
								}
							}
						}
					}
					else {
						mensajeAdvertencia("La fecha de nacimiento del trabajador no es valida");
					}
				}
				else {
					mensajeAdvertencia("La estructura del RFC es inv&aacute;lida");
				}
			}
		}
	});

	$("#btnImprimir").click(function () {
		ImprimirPDF();
	});
	$("#btnCapturaBeneficiario").click(function () {
		capturaBeneficiario();
	});

	$("#btnDigitalizar").click(function () {
		$('#btnDigitalizar').attr('disabled', true);
		iBoton = 2;
		opcionEjecuta();
	});

	$("#btnCancelar").click(function () {

		mensajeAdvertenciaFinal("Se ha cancelado la solicitud del expediente de identificaci&oacute;n");

	});

	$("#slcComprobanteDomicilio").change(function () {
		if ($("#slcComprobanteDomicilio").val() != 1) {
			document.getElementById("lblFechaComprobante").style.visibility = "visible";
			document.getElementById("dtFechaComprobante").style.visibility = "visible";
		}
		else {
			document.getElementById("lblFechaComprobante").style.visibility = "hidden";
			document.getElementById("dtFechaComprobante").style.visibility = "hidden";
		}

	});

	$("#divPdf").dialog
		({
			title: 'Impresi\u00f3n de Documento',
			autoOpen: false,
			resizable: false,
			width: 800,
			height: 830,
			modal: true,
			open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
			buttons:
			{
				"Aceptar": function () {
					$(this).dialog("close");
				},
			}

		});

});

//Metodo agregado para el folio 316.2
function obtenerExcepcionEnrolamiento() {
	var iResp = 0;
	$.ajax({
		async: false,
		cache: true,
		url: "php/expedienteIdentificacion.php",
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 35,
			cFolio: cFolio
		},
		success: function (data) {
			iResp = data.respuesta;
			console.log("Respuesta de la consulta de excepciones = " + iResp);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener la excepción del enrolamiento.");
			console.log(thrownError);
		}
	});

	return iResp;

}

function obtenerExcepcionSolicitante(cFolio) {
	var iRespExcp = 0;
	$.ajax({
		async: false,
		cache: true,
		url: "php/expedienteIdentificacion.php",
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 37,
			cFolio: cFolio
		},
		success: function (data) {
			iRespExcp = data.excepcion;
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener la excepción del enrolamiento1.");
			console.log(thrownError);
		}
	});

	return iRespExcp;

}

function prevenirPaste() {
	$("#contenedorPrincipal").find(':text').each(function () {
		var elemento = this;
		campo = elemento.id;
		$('#' + campo).bind('paste', function (e) { e.preventDefault(); });
	});
}

function validarExpedientePublicado() {
	var sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 25,
			cFolio: cFolio,
			iTipoServicio: iTipoServicio,
		},

		beforeSend: function () {
			bloquearUI("Validando Expediente");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al validar el estatus del expediente, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					iEstatusExpediente = respuesta.estatusExp;
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del estatus del expediente, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function consultarColonia() {
	$.ajax({
		url: '../../buscarColonia/frmbuscarColonia.html',
		async: true,
		cache: false,
		success: function (data) {
			//var datos = eval( '(' + data + ')' );
			$("#carga").html(data);
			iniDivBuscarColonia(recibirDomicilio);
			$("#idtxtCodPostal").val(codigopostal);
		},
		beforeSend: function () {
		},
		error: function (a, b, c) {

		}
	});
}
function consultarColoniaLaboral() {
	$.ajax({
		url: '../../buscarColonia/frmbuscarColonia.html',
		async: true,
		cache: false,
		success: function (data) {
			//var datos = eval( '(' + data + ')' );
			$("#carga").html(data);
			iniDivBuscarColonia(recibirDomicilioLaboral);
			$("#idtxtCodPostal").val(codigopostal);
		},
		beforeSend: function () {
		},
		error: function (a, b, c) {

		}
	});
}

function recibirDomicilio() {
	$('#txtPais').val('M\u00C9XICO');;
	$('#txtCodigoPostal').val(arrDatosColonia.codigopostal);
	$('#txtColonia').val(arrDatosColonia.nombrecolonia);
	$('#txtDeleMuni').val(arrDatosColonia.nombremunicipio);
	$('#txtEntidadFederativa').val(arrDatosColonia.nombreestado);
	$('#txtCiudad').val(arrDatosColonia.nombreciudad);


	$('#txtCpOculto').val(arrDatosColonia.codigopostal);
	$('#txtColoniaOculto').val(arrDatosColonia.idcolonia);
	$('#txtDeleMuniOculto').val(arrDatosColonia.idmunicipio);
	$('#txtEntidadFederativaOculto').val(arrDatosColonia.idestado);
	$('#txtCiudadOculto').val(arrDatosColonia.idciudad);
}

function recibirDomicilioLaboral() {
	$('#txtPaisLaboral').val('M\u00C9XICO');;
	$('#txtCodigoPostalLaboral').val(arrDatosColonia.codigopostal);
	$('#txtColoniaLaboral').val(arrDatosColonia.nombrecolonia);
	$('#txtDeleMuniLaboral').val(arrDatosColonia.nombremunicipio);
	$('#txtEntidadFederativaLaboral').val(arrDatosColonia.nombreestado);
	$('#txtCiudadLaboral').val(arrDatosColonia.nombreciudad);

	$('#txtCpLaboralOculto').val(arrDatosColonia.codigopostal);
	$('#txtColoniaLaboralOculto').val(arrDatosColonia.idcolonia);
	$('#txtDeleMuniLaboralOculto').val(arrDatosColonia.idmunicipio);
	$('#txtEntidadFederativaLaboralOculto').val(arrDatosColonia.idestado);
	$('#txtCiudadLaboralOculto').val(arrDatosColonia.idciudad);
}

function validarCampos() {

	$("#txtNss").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything

		if (!(e.which >= 48 && e.which <= 57)) {
			e.preventDefault();
		}

	});
	$("#txtApellidoPaterno").keypress(function (e) {

		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122) || e.which == 32)) {
				e.preventDefault();
			}
		}

	});

	if (OSName == "Android") {
		$('#txtCorreoElectronico').on('textInput', e => {

			var keyCode = e.originalEvent.data.charCodeAt(0);
			// keyCode is ASCII of character entered.

			if (!(keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 32 || keyCode == 46 || keyCode == 45 || keyCode == 95 || keyCode >= 48 && keyCode <= 57)) {

				e.preventDefault();

			} else {

				setTimeout(function () {

					var texto = document.getElementById("txtCorreoElectronico").value;

					texto = texto.replace('..', '.');
					texto = texto.replace('__', '_');
					texto = texto.replace('--', '-');
					texto = texto.replace('.-', '.');
					texto = texto.replace('-.', '-');
					texto = texto.replace('_-', '_');
					texto = texto.replace('-_', '-');
					texto = texto.replace('._', '.');
					texto = texto.replace('_.', '_');
					document.getElementById("txtCorreoElectronico").value = texto;

				}, 10);

			}
		});
		$('#txtCorreoElectronicoConfirm').on('textInput', e => {

			var keyCode = e.originalEvent.data.charCodeAt(0);
			// keyCode is ASCII of character entered.

			if (!(keyCode >= 65 && keyCode <= 90 || keyCode >= 97 && keyCode <= 122 || keyCode == 32 || keyCode == 46 || keyCode == 45 || keyCode == 95 || keyCode >= 48 && keyCode <= 57)) {

				e.preventDefault();

			} else {

				setTimeout(function () {

					var texto = document.getElementById("txtCorreoElectronicoConfirm").value;

					texto = texto.replace('..', '.');
					texto = texto.replace('__', '_');
					texto = texto.replace('--', '-');
					texto = texto.replace('.-', '.');
					texto = texto.replace('-.', '-');
					texto = texto.replace('_-', '_');
					texto = texto.replace('-_', '-');
					texto = texto.replace('._', '.');
					texto = texto.replace('_.', '_');
					document.getElementById("txtCorreoElectronicoConfirm").value = texto;

				}, 10);

			}
		});
		$("#txtOtroDominio").keypress(function (e) {
			if (!(arrCaracteresPermitidosCorreo.indexOf(e.which) > -1)) {
				if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
					e.preventDefault();
				}
			}
		});
		$("#txtOtroDominioConfirm").keypress(function (e) {
			if (!(arrCaracteresPermitidosCorreo.indexOf(e.which) > -1)) {
				if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
					e.preventDefault();
				}
			}
		});
		$(".mobile").show();
	} else {

		$("#txtCorreoElectronico").keypress(function (e) {
			if (!(arrCaracteresPermitidosCorreo.indexOf(e.which) > -1)) {
				if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
					e.preventDefault();
				}
			}
		});
		$("#txtCorreoElectronicoConfirma").keypress(function (e) {
			if (!(arrCaracteresPermitidosCorreo.indexOf(e.which) > -1)) {
				if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
					e.preventDefault();
				}
			}
		});
		$("#txtOtroDominio").keypress(function (e) {
			if (!(arrCaracteresPermitidosCorreo.indexOf(e.which) > -1)) {
				if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
					e.preventDefault();
				}
			}
		});
		$("#txtOtroDominioConfirma").keypress(function (e) {
			if (!(arrCaracteresPermitidosCorreo.indexOf(e.which) > -1)) {
				if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
					e.preventDefault();
				}
			}
		});
		$(".mobile").hide();
		$(".mobile").parent().remove();
	}


	$("#txtFechaNacimiento").datepicker({
		dateFormat: 'dd-mm-yy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		maxDate: "0D",
		changeYear: true,
		yearRange: "1900:+0",
		onClose: function (fecha) {
			var sFechaNacimiento = null;
			sFechaNacimiento = formatearFecha(2, fecha);
			llenarIdentificacionOficial(sFechaNacimiento);
		}
	});



	$("#dtFechaComprobante").datepicker({
		dateFormat: 'dd-mm-yy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		maxDate: "0D",

	});

	$("#txtCurp").keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
				e.preventDefault();
			}
		}

	});

	$("#txtNumExterior").keypress(function (e) {
		console.log(e.keyCode);
		//if the letter is not digit then display error and don't type anything
		if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 || e.keyCode >= 37 && e.keyCode <= 40) || e.which == 45 || e.which == 08 || e.which == 32 || e.keyCode == 46)) {
			return false;
		}

	});
	$("#txtNumInterior").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 || e.keyCode >= 37 && e.keyCode <= 40) || e.which == 45 || e.which == 08 || e.which == 32 || e.keyCode == 46)) {
			return false;
		}
	});
	$("#txtApellidoMaterno").keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122) || e.which == 32)) {
				e.preventDefault();
			}
		}
	});

	$("#txtRfc").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
				e.preventDefault();
			}
		}
	});


	$("#txtNombres").keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122) || e.which == 32)) {
				e.preventDefault();
			}
		}
	});

	$("#txtTel1").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});
	$("#txtTel2").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	});


	$("#txtCalle").keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 || e.which == 32)) {
				e.preventDefault();
			}
		}

	});


	$("#txtCalleLaboral").keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {

			if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 || e.which == 32)) {

				e.preventDefault();
			}
		}
	});

	$("#txtNumExteriorLaboral").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 || e.keyCode >= 37 && e.keyCode <= 40) || e.which == 45 || e.which == 08 || e.which == 32 || e.keyCode == 46)) {
			return false;
		}
	});
	$("#txtNumInteriorLaboral").keypress(function (e) {
		//if the letter is not digit then display error and don't type anything
		if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 || e.keyCode >= 37 && e.keyCode <= 40) || e.which == 45 || e.which == 08 || e.which == 32 || e.keyCode == 46)) {
			return false;
		}
	});

	$("#txtApellidoPaterno,#txtApellidoMaterno,#txtCurp,#txtRfc,#txtNombres,#txtCalle,#txtCalleLaboral,#txtNumInterior,#txtNumExterior,#txtNumInteriorLaboral,#txtNumExteriorLaboral").bind("change", aMayusculas);
}

function tipoConstancia() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		data: { iOpcion: 36, cFolio: cFolio },

		beforeSend: function () {
			bloquearUI("Consultando el tipo de constancia");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar la constancia.");
			console.log(thrownError);
		},

		success: function (respuesta) {
			var obj = jQuery.parseJSON(respuesta);
			desbloquearUI();

			try {

				if (obj.codigoRespuestaEx == 1) {
					// Si el solicitante es "Beneficiario, curador o Apoderado legal", en este caso siempre se oculta el checkbox.
					$("#chBoxTrabajadorfallecido").hide();
					$("#lblRegistroFallecido").hide();
					chBoxTrabajadorfallecido = 1;
				}
				else if (obj.codigoRespuestaEx == 2) {
					// Si el solicitante es "Titular", en este caso siempre se oculta el checkbox.
					$("#chBoxTrabajadorfallecido").hide();
					$("#lblRegistroFallecido").hide();
					chBoxTrabajadorfallecido = 0;
				}
				else {
					// Si el solicitante es "Titular", en este caso siempre se oculta el checkbox.
					$("#chBoxTrabajadorfallecido").hide();
					$("#lblRegistroFallecido").hide();
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar la constancia.");
				console.log(err);
			}
		}
	});
}

function guardarDatos() {
	var cCorreo = "";
	var bBandera = false;
	var sPhp = null;
	var objTrabajador = null;
	var objDomicilio = null;
	var objDomicilioLaboral = null;
	var objBeneficiarios = {};
	var cTipoDomicilio = "";
	var iPromotor = iNumeroEmpleado;
	var iEstatus = 1;
	var cNss = $("#txtNss").val();
	var cCurp = $("#txtCurp").val();
	var cRfc = $("#txtRfc").val();
	var cApellidoPaterno = $("#txtApellidoPaterno").val();
	var cApellidoMaterno = $("#txtApellidoMaterno").val();
	var cNombres = $("#txtNombres").val();
	var dtFechaNacimiento = formatearFecha(2, $("#txtFechaNacimiento").val());
	var iEntidadNacimiento = $("#slcEntidadNacimiento").val();
	var cNacionalidad = $("#slcNacionalidad").val();
	var cGenero = $("#slcGeneroTrabajador").val();
	var iNivelEstudio = $("#slcNivelEstudios").val();
	var iOcupacion = $("#slcOcupacionProfesion").val();
	var iActividadGiroNegocio = $("#slcActividadNegocio").val();
	var cTelefono1 = $("#txtTel1").val();
	var cTelefono2 = $("#txtTel2").val();
	var islcTel1 = $("#slcTel1").val();
	var islcTel2 = $("#slcTel2").val();
	var islcCorreo = $("#slcCorreoElectronico").val();
	var cCodigoPostal = $("#txtCpOculto").val();
	var cPais = $("#txtPais").val();
	var iEntidadFederativa = $("#txtEntidadFederativaOculto").val() != "" ? $("#txtEntidadFederativaOculto").val() : 0;
	var iMunicipio = $("#txtDeleMuniOculto").val() != "" ? $("#txtDeleMuniOculto").val() : 0;
	var iCiudad = $("#txtCiudadOculto").val() != "" ? $("#txtCiudadOculto").val() : 0;
	var iColonia = $("#txtColoniaOculto").val() != "" ? $("#txtColoniaOculto").val() : 0;
	var cCalle = $("#txtCalle").val();
	var cNumExterior = $("#txtNumExterior").val();
	var cNumInterior = $("#txtNumInterior").val();
	var cCodigoPostalLaboral = $("#txtCpLaboralOculto").val();
	var cPaisLaboral = $("#txtPaisLaboral").val();
	var iEntidadFederativaLaboral = $('#txtEntidadFederativaLaboralOculto').val() != "" ? $('#txtEntidadFederativaLaboralOculto').val() : 0;
	var iCiudadLaboral = $("#txtCiudadLaboralOculto").val() != "" ? $("#txtCiudadLaboralOculto").val() : 0;
	var iMunicipioLaboral = $("#txtDeleMuniLaboralOculto").val() != "" ? $("#txtDeleMuniLaboralOculto").val() : 0;
	var iColoniaLaboral = $('#txtColoniaLaboralOculto').val() != "" ? $('#txtColoniaLaboralOculto').val() : 0;
	var cCalleLaboral = $("#txtCalleLaboral").val();
	var cNumExteriorLaboral = $("#txtNumExteriorLaboral").val();
	var cNumInteriorLaboral = $("#txtNumInteriorLaboral").val();
	var dtFechaVigenciaIdOficial = formatearFecha(2, "");
	var dtFechaComprobanteDomicilio = formatearFecha(2, $("#dtFechaComprobante").val());
	//var chBoxTrabajadorfallecido = 0;
	var iSlcComprobanteDomicilio = $("#slcComprobanteDomicilio").val();
	var iSlcIndetificacionOficial = $("#slcIdentificacionOficial").val();

	if ($("#chBoxTrabajadorfallecido").prop("checked")) {
		chBoxTrabajadorfallecido = 1;
	}

	if ($("#txtCorreoElectronico").val() != "") {
		if (islcCorreo == 7) {
			cCorreo = $("#txtCorreoElectronico").val() + "@" + $("#txtOtroDominio").val();
		}
		else {
			cCorreo = $("#txtCorreoElectronico").val() + "@" + $("#slcCorreoElectronico option:selected").text();
		}
	}

	if ($("#dtFechaVigencia").val() == "") {

		dtFechaVigenciaIdOficial = "1900-01-01";
	}

	//validar el domicilio laboral, si algun campo viene vacio, se blanquea todo el domicilio laboral
	//solo se insertara el domicilio laboral si se captura la totalidad de los datos
	if (bDomicilioLaboral) {
		cCodigoPostalLaboral = "";
		cPaisLaboral = "";
		iEntidadFederativaLaboral = -1;
		iCiudadLaboral = -1;
		iMunicipioLaboral = -1;
		iColoniaLaboral = -1;
		cCalleLaboral = "";
		cNumExteriorLaboral = "";
		cNumInteriorLaboral = "";
	}



	objDomicilio = {
		cCodigoPostal: cCodigoPostal,
		cTipoDomicilio: cTipoDomicilio,
		cPais: 342,
		iEntidadFederativa: iEntidadFederativa,
		iMunicipio: iMunicipio,
		iCiudad: iCiudad,
		iColonia: iColonia,
		cCalle: cCalle,
		cNumExterior: cNumExterior,
		cNumInterior: cNumInterior

	};
	objDomicilioLaboral = {
		cCodigoPostalLaboral: cCodigoPostalLaboral,
		cTipoDomicilio: cTipoDomicilio,
		cPaisLaboral: cPaisLaboral,
		iEntidadFederativaLaboral: iEntidadFederativaLaboral,
		iMunicipioLaboral: iMunicipioLaboral,
		iCiudadLaboral: iCiudadLaboral,
		iColoniaLaboral: iColoniaLaboral,
		cCalleLaboral: cCalleLaboral,
		cNumExteriorLaboral: cNumExteriorLaboral,
		cNumInteriorLaboral: cNumInteriorLaboral
	};
	if (typeof arrBeneficiarios == 'undefined') {
		arrBeneficiarios = "";
	}

	objTrabajador = {
		cFolio: cFolio,
		iEstatus: iEstatus,
		cNss: cNss,
		cCurp: cCurp,
		cRfc: cRfc,
		cApellidoPaterno: cApellidoPaterno,
		cApellidoMaterno: cApellidoMaterno,
		cNombres: cNombres,
		dtFechaNacimiento: dtFechaNacimiento,
		iEntidadNacimiento: iEntidadNacimiento,
		cNacionalidad: cNacionalidad,
		cGenero: cGenero,
		iNivelEstudio: iNivelEstudio,
		iOcupacion: iOcupacion,
		iActividadGiroNegocio: iActividadGiroNegocio,
		cTelefono1: cTelefono1,
		cTelefono2: cTelefono2,
		islcTel1: islcTel1,
		islcTel2: islcTel2,
		cCorreo: cCorreo,
		islcCorreo: islcCorreo,
		domicilio: objDomicilio,
		beneficiarios: arrBeneficiarios,
		domicilioLaboral: objDomicilioLaboral,
		dtFechaVigenciaIdOficial: dtFechaVigenciaIdOficial,
		dtFechaComprobanteDomicilio: dtFechaComprobanteDomicilio,
		chBoxTrabajadorfallecido: chBoxTrabajadorfallecido,
		iTipoServicio: iTipoServicio,
		iExisteEi: iExisteEi,
		dtFechaServicio: dtFechaServicio,
		iPromotor: iPromotor,
		iSlcComprobanteDomicilio: iSlcComprobanteDomicilio,
		iSlcIndetificacionOficial: iSlcIndetificacionOficial
	};


	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 1,
			objTrabajador: objTrabajador,
			objDomicilio: objDomicilio,
			objDomicilioLaboral: objDomicilioLaboral
		},

		beforeSend: function () {
			bloquearUI("Guardando los datos del trabajador");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al guardar los datos, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);

		},

		success: function (respuesta) {
			desbloquearUI();

			try {
				if (respuesta.codigoRespuesta == 1) {
					if (iTipoServicio == 26 || iTipoServicio == 27 || iTipoServicio == 33) {
						if(iTipoSolicitante == 2 || iTipoSolicitante == 3 || iTipoSolicitante == 4){
							if(fnGuardarDatosSolicitante()){
								//mensajeExito("Se han guardado los datos correctamente");
								bBandera = true;
								$("#btnDigitalizar").show();
							}
						}else{
							//mensajeExito("Se han guardado los datos correctamente");
							bBandera = true;
							$("#btnDigitalizar").show();
						}
					}
					else {
						//mensajeExito("Se han guardado los datos correctamente");
						bBandera = true;
						$("#btnImprimir").show();
					}


					$("#btnGuardar").hide();
					$("#btnCapturaBeneficiario").hide();

				}
				else {
					mensajeAdvertencia(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al guardar los datos, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});

	try {
		guardaDatosSwitch(cCorreo)
	}
	catch (err) {
		mensajeAdvertenciaFinal("Ocurri&oacute; un error al guadar los datos de los switch.");
		console.log(err);
	}

	return bBandera;
}

function mensajeAdvertencia(sMensaje) {
	$("#divMensaje").html("");
	$("#divMensaje").append("<p style='text-align: justify;'><table><tr><td><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span></td><td>" + sMensaje + "</td></tr><table></p>");
	$("#divMensaje").dialog({
		resizable: false,
		height: 250,
		width: 500,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1500px'); },
		title: "ADVERTENCIA",
		buttons: {
			"Aceptar": function () {
				$(this).dialog("close");
			},
		}
	});
}

function mensajeAdvertenciaFinal(sMensaje) {

	$("#divMensaje").html("");
	$("#divMensaje").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>" + sMensaje + "</p>");
	$("#divMensaje").dialog({
		resizable: false,
		height: 250,
		width: 500,
		modal: true,

		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
		title: "ADVERTENCIA",
		buttons: {
			"Aceptar": function () {
				$(this).dialog("close");
				cerrarNavegador();
			},
		}
	});
}

function mensajeExito(sMensaje, funcionAceptar) {

	var newHtml = "<div class='exito'>";
	newHtml += "<p>";
	newHtml += sMensaje;
	newHtml += "</p></div>";
	$('#divMensaje').html(newHtml);
	$("#divMensaje").dialog({
		resizable: false,
		height: 230,
		width: 500,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
		title: "\u00C9XITO",
		buttons: {
			"Aceptar": function () {
				if (typeof funcionAceptar == 'function') {
					funcionAceptar();
				}
				$(this).dialog("close");
			},
		}
	});
}

function mensajeExitoFinal(sMensaje) {

	var newHtml = "<div class='exito'>";
	newHtml += "<p>";
	newHtml += sMensaje;
	newHtml += "</p></div>";
	$('#divMensaje').html(newHtml);
	$("#divMensaje").dialog({
		resizable: false,
		height: 230,
		width: 500,
		modal: true,
		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
		title: "\u00C9XITO",
		buttons: {
			"Aceptar": function () {
				$(this).dialog("close");
				cerrarNavegador();
			},
		}
	});
}

function ImprimirPDF() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({

		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 2,
			cFolio: cFolio,
		},


		beforeSend: function () {
			bloquearUI("Generando el documento PDF");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al realizar la impresi&oacute;n, por favor, reporte a Mesa de ayuda.");
			console.log(xhr);
		},
		success: function (respuesta) {


			try {
				if (respuesta.codigoRespuesta == 1) {

					//Esperar 2 Segundo para que se genere el PDF
					setTimeout(function () {
						desbloquearUI();

						var sHtml = '<iframe src=\"' + respuesta.rutapdf + '\" width=\"100%\" height=\"100%\" frameborder=\"0\"></iframe>';
						$('#divPdf').empty();
						$('#divPdf').html(sHtml);

						if (confirm("Favor de colocar una hoja en la impresora")) {
							$('#divPdf').dialog('open');
							$("#btnDigitalizar").show();
						}

					}, 2000);

				}
				else {
					desbloquearUI();
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {

				mensajeAdvertenciaFinal("Ocurri&oacute; un error al realizar la impresi&oacute;n, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function capturaBeneficiario() {
	sPhp = "/expedienteIdentificacion/indexBeneficiarios.html";

	$.ajax({
		url: sPhp,
		async: true,
		cache: false,
		success: function (data) {
			//var datos = eval( '(' + data + ')' );
			$("#carga").html(data);
			iniBeneficiarios(recibirBeneficiarios);
		},
		beforeSend: function () {
		},
		error: function (a, b, c) {
		}
	});
}

function recibirBeneficiarios() {
	console.log(arrBeneficiarios);
}


function llenarTelefonos() {
	sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 4 },
		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de tel&eacute;fonos");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de tel&eacute;fonos, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcTel1").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.telefono.length; i++) {
						$("#slcTel1").append("<option value=" + respuesta.telefono[i].idcatalogo + ">"
							+ respuesta.telefono[i].descripcion + "</option>");
					}
					$("#slcTel2").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.telefono.length; i++) {
						$("#slcTel2").append("<option value=" + respuesta.telefono[i].idcatalogo + ">"
							+ respuesta.telefono[i].descripcion + "</option>");
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de tel&eacute;fonos, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function formatearFecha(piTipoFormato, pcFechaEntrada) {
	var cFechaSalida;
	//Inicializamos la Fecha de Salida de acuerdo a lo que se
	//espera de salida de acuerdo al piTipoFormato.
	switch (piTipoFormato) {
		case 1:
			cFechaSalida = "19000101";
			break;
		case 2:
			cFechaSalida = "1900-01-01";
			break;
		case 3:
			cFechaSalida = "01-01-1900";
			break;
		case 4:
			cFechaSalida = "'MDY(01,01,1900)";
			break;
		case 5:
			cFechaSalida = "01/01/1900";
			break;
	}

	if (pcFechaEntrada.length > 0) {
		if (piTipoFormato == 1) //LA DEVUELVE EN: YYYYMMDD
		{
			cFechaSalida = pcFechaEntrada.substr(6, 4) + pcFechaEntrada.substr(3, 2) + pcFechaEntrada.substr(0, 2);
		}
		else if (piTipoFormato == 2) //LA DEVUELVE EN: YYYY-MM-DD
		{
			cFechaSalida = pcFechaEntrada.substr(6, 4) + '-' + pcFechaEntrada.substr(3, 2) + '-' + pcFechaEntrada.substr(0, 2);
		}
		else if (piTipoFormato == 3) //RECIBE YYYY-MM-DD DEVUELVE DD-MM-YYYY
		{
			cFechaSalida = pcFechaEntrada.substr(8, 2) + "-" + pcFechaEntrada.substr(5, 2) + "-" + pcFechaEntrada.substr(0, 4);
		}
		else if (piTipoFormato == 4) //LA DEVUELVE EN: MDY(MM,DD,YYYY)
		{
			cFechaSalida = 'MDY(' + pcFechaEntrada.substr(3, 2) + ',' + + pcFechaEntrada.substr(0, 2) + ',' + pcFechaEntrada.substr(6, 4) + ')';
		}
		else if (piTipoFormato == 5) //RECIBE DD-MM-YYYY DEVUELVE DD/MM/YYYY
		{
			cFechaSalida = pcFechaEntrada.substr(0, 2) + "/" + pcFechaEntrada.substr(3, 2) + "/" + pcFechaEntrada.substr(6, 4);
		}
	}
	return cFechaSalida;
}

function cargaPagina() {
	$("#btnGuardar").button();
	$("#btnImprimir").button();
	$("#btnCancelar").button();
	$("#btnCapturaBeneficiario").button();
	$("#btnDigitalizar").button();
	$('#btnBuscarCodigoPostal,#btnBuscarCodigoPostalLaboral').button
		({
			text: false,
			icons:
			{
				primary: 'ui-icon ui-icon-search'
			}
		});

	$("#txtOtroDominio").prop("disabled", true);
	$("#txtOtroDominio").css('visibility', 'hidden');
	$("#txtOtroDominioConfirm").prop("disabled", true);
	$("#txtOtroDominioConfirm").css('visibility', 'hidden');
	$("#btnImprimir").hide();
	$("#btnDigitalizar").hide();

	validarCampos();
	llenarTelefonos();
	llenarDominiosCorreo();
	llenarOcupacion();
	llenarActividadGiroNegocio();
	llenarEntidadNacimiento();
	llenarGenero();
	llenarNivelEstudio();
	llenarNacionalidad();
	llenarComprobanteDomicilio();
	fechaMaximaComprobanteDomicilio();
	if(iTipoServicio == 26 || iTipoServicio == 33){
		validarTipoSolicitante();
	}
	
	$("#divOculto").hide();
	$("#txtCodigoPostal,#txtEntidadFederativa,#txtDeleMuni,#txtColonia,#txtPais,#txtCiudad").attr("readonly", "true");
	$("#txtCodigoPostalLaboral,#txtEntidadFederativaLaboral,#txtDeleMuniLaboral").attr("readonly", "true");
	$("#txtColoniaLaboral,#txtPaisLaboral,#txtCiudadLaboral").attr("readonly", "true");
}

function llenarDominiosCorreo() {
	sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 5 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de dominios");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de dominios correo, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					if (OSName == "Android") {
						$("#slcCorreoElectronicoConfirm").append("<option value=-1></option>");
					}

					$("#slcCorreoElectronico").append("<option value=-1></option>");
					$("#slcCorreoElectronicoConfirma").append("<option value=-1></option>");

					for (var i = 0; i < respuesta.correo.length; i++) {
						if (OSName == "Android") {
							$("#slcCorreoElectronicoConfirm").append("<option value=" + respuesta.correo[i].idserviciocorreo + ">"
								+ respuesta.correo[i].descripcion + "</option>");
						}

						$("#slcCorreoElectronico").append("<option value=" + respuesta.correo[i].idserviciocorreo + ">"
							+ respuesta.correo[i].descripcion + "</option>");
						$("#slcCorreoElectronicoConfirma").append("<option value=" + respuesta.correo[i].idserviciocorreo +" id=opcion"+respuesta.correo[i].idserviciocorreo+">"
							+ respuesta.correo[i].descripcion + "</option>");
					}
					$("#slcCorreoElectronico").append("<option value=7>otro</option>");
					$("#slcCorreoElectronicoConfirma").append("<option value=7 id=opcion8>otro</option>");
					if (OSName == "Android") {
						$("#slcCorreoElectronicoConfirm").append("<option value=7>otro</option>");
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de dominios correo, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function llenarOcupacion() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 6 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de ocupaci&oacute;n");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de ocupaci&oacute;n , por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcOcupacionProfesion").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.ocupacion.length; i++) {
						$("#slcOcupacionProfesion").append("<option value=" + respuesta.ocupacion[i].idocupacion + ">"
							+ respuesta.ocupacion[i].descripcion + "</option>");
					}

				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de ocupaci&oacute;n, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function llenarGenero() {

	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 13 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de genero");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de Generos , por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					$("#slcGeneroTrabajador").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.generos.length; i++) {
						$("#slcGeneroTrabajador").append("<option value=" + respuesta.generos[i].idgenero + ">"
							+ respuesta.generos[i].descripcion + "</option>");
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de Generos, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}


function validarCurp(curp) {
	var esValida = 0;
	//AAAA######AAAAAA##
	if (curp.match(/^([A-Z])([A,E,I,O,U,X])([A-Z]{2})([0-9]{2})([0-1])([0-9])([0-3])([0-9])([M,H])([A-Z]{2})([B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3})([0-9,A-Z])([0-9])$/i)) {
		esValida = 1;
	}
	return esValida;
}

function datosGenerales() {

	var cCurp = $("#txtCurp").val();
	var cRfc = $("#txtRfc").val();
	var cNss = $("#txtNss").val();
	var cApellidoPaterno = $("#txtApellidoPaterno").val();
	var cApellidoMaterno = $("#txtApellidoMaterno").val();
	var cNombres = $("#txtNombres").val();
	var dtFechaNacimiento = formatearFecha(2, $("#txtFechaNacimiento").val());
	var iEntidadNacimiento = $("#slcEntidadNacimiento").val();
	var cGenero = $("#slcGeneroTrabajador").val();
	var cNacionalidad = $("#slcNacionalidad").val();
	var respuesta = false;

	if (cApellidoPaterno != "") {
		if (cApellidoMaterno != "" || cApellidoMaterno == "") {
			if (cNombres != "") {
				if (cCurp != "" && validarCurp(cCurp)) {
					if (cRfc.length >= 11 || cRfc.length <= 13) {
						if (dtFechaNacimiento != "1900-01-01") {
							if (iEntidadNacimiento != -1) {
								if (cGenero != -1) {
									if (cNacionalidad != "") {
										respuesta = true;
									}
									else {
										mensajeAdvertencia("Favor de capturar la nacionalidad para el trabajador para el trabajador");
									}
								}
								else {
									mensajeAdvertencia("Favor de capturar el genero para el trabajador");
								}
							}
							else {
								mensajeAdvertencia("Favor de capturar la Entidad de Nacimiento para el trabajador");
							}
						}
						else {
							mensajeAdvertencia("Favor de capturar la Fecha de Nacimiento para el trabajador");
						}
					}
					else {
						mensajeAdvertencia("Favor de capturar el RFC para el trabajador");
					}

				}
				else {
					mensajeAdvertencia("Favor de capturar el CURP o la CURP que capturo no es valida para el trabajador");
				}
			}
			else {
				mensajeAdvertencia("Favor de capturar el Nombre para el trabajador");
			}
		}
		else {
			mensajeAdvertencia("Favor de capturar el Apellido Materno para el trabajador");

		}
	}
	else {

		mensajeAdvertencia("Favor de capturar el Apellido Paterno para el trabajador");
	}
	return respuesta;
}

function datosNivelestudioOcupacionActividadEconomica() {
	var iNivelEstudio = $("#slcNivelEstudios").val();
	var iOcupacion = $("#slcOcupacionProfesion").val();
	var iActividadGiroNegocio = $("#slcActividadNegocio").val();
	var respuesta = false;

	if (iNivelEstudio != -1) {
		if (iOcupacion != -1) {
			if (iActividadGiroNegocio != -1) {
				respuesta = true;
			}
			else {
				mensajeAdvertencia("Favor de proporcionar una actividad o giro del negocio");
			}
		}
		else {
			mensajeAdvertencia("Favor de proporcionar una ocupaci&oacute;n")
		}
	}
	else {
		mensajeAdvertencia("Favor de proporcionar un nivel de estudios");
	}

	return respuesta;
}

function datosContactacion() {
	var cTelefono1 = $("#txtTel1").val();
	var cTelefono2 = $("#txtTel2").val();
	var islcTel1 = $("#slcTel1").val();
	var islcTel2 = $("#slcTel2").val();
	var cCorreo = $("#txtCorreoElectronico").val();
	var cOtroDominio = $("#txtOtroDominio").val();
	var islcCorreo = $("#slcCorreoElectronico").val();

	var cCorreoConfirm = $("#txtCorreoElectronicoConfirm").val();
	var cOtroDominioConfirm = $("#txtOtroDominioConfirm").val();
	var islcCorreoConfirm = $("#slcCorreoElectronicoConfirm").val();


	var cOtroCorreo = $("#txtOtroCorreo").val();
	var respuesta = false;

	if (cTelefono1 != "" && cTelefono1.length == 10) {
		if ((cTelefono2 != "" && cTelefono2.length == 10) || cTelefono2 == "") {
			if (islcTel1 != -1) {
				if (islcTel2 != -1 || islcTel2 == -1) {
					if (cCorreo != "") {
						if (islcCorreo != -1 || (islcCorreo == 7 && cOtroDominio.length > 0)) {
							if (OSName == "Android") {
								if (cCorreo == cCorreoConfirm && islcCorreo == islcCorreoConfirm && cOtroDominio == cOtroDominioConfirm) {
									respuesta = true;
								} else {
									mensajeAdvertencia("Favor verificar, correo y la confirmación no coinciden.");
								}
							} else {
								respuesta = true;
							}
						}
						else {
							mensajeAdvertencia("Favor de capturar el dominio de la cuenta de correo electr&oacute;nico.");
						}
					}
					else {
						if (OSName == "Android") {
							mensajeAdvertencia("Favor de capturar correo electr&oacute;nico y dominio valido.");
						} else {
							respuesta = true;
						}
					}
				}
				else {
					mensajeAdvertencia("Favor de elegir un tipo de tel&eacute;fono");
				}
			}
			else {
				mensajeAdvertencia("Favor de proporcionar un tipo de tel&eacute;fono");
			}
		}
		else {
			mensajeAdvertencia("El tel&eacute;fono 2 de contacto es inv&aacute;lido");
		}
	}
	else {
		mensajeAdvertencia("El tel&eacute;fono 1 de contacto es inv&aacute;lido");
	}
	return respuesta;
}

function datosDomicilio() {

	var cCodigoPostal = $("#txtCpOculto").val();
	var cPais = $("#txtPais").val();
	var iEntidadFederativa = $("#txtColoniaOculto").val();
	var iMunicipio = $("#txtDeleMuniOculto").val();
	var iCiudad = $("#txtCiudad").val();
	var iColonia = $("#txtEntidadFederativaOculto").val();
	var cCalle = $("#txtCalle").val();
	var cNumExterior = $("#txtNumExterior").val();
	var cNumInterior = $("#txtNumInterior").val();
	var respuesta = false;

	if (cCodigoPostal != "") {
		if (cPais != "") {
			if (iEntidadFederativa != -1) {
				if (iMunicipio != -1) {
					// if (iCiudad != -1)
					// {
					if (iColonia != -1) {
						if (cCalle != "") {
							if (cNumExterior != "") {
								if (cNumInterior != "" || cNumInterior == "") {
									respuesta = true;
								}
								// else
								// {
								// 	mensajeAdvertencia("Favor de proporcionar un n&uacute;mero interior");
								// }
							}
							else {
								mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
							}
						}
						else {
							mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
						}
					}
					else {
						mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
					}
					// }
					// else
					// {
					// 	mensajeAdvertencia("Favor de proporcionar su ciudad");
					// }
				}
				else {
					mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
				}
			}
			else {
				mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
			}
		}
		else {
			mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
		}
	}
	else {
		mensajeAdvertencia("Promotor, favor de capturar todos los campos obligatorios");
	}
	return respuesta;
}

function datosDomicilioLaboral() {
	var cCodigoPostalLaboral = $("#txtCpLaboralOculto").val();
	var cPaisLaboral = $("#txtPaisLaboral").val();
	var iEntidadFederativaLaboral = $("#txtColoniaLaboralOculto").val();
	var iCiudadLaboral = $("#txtCiudadLaboral").val();
	var iMunicipioLaboral = $("#txtDeleMuniLaboralOculto").val();
	var iColoniaLaboral = $("#txtEntidadFederativaLaboralOculto").val();
	var cCalleLaboral = $("#txtCalleLaboral").val();
	var cNumExteriorLaboral = $("#txtNumExteriorLaboral").val();
	var cNumInteriorLaboral = $("#txtNumInteriorLaboral").val();
	var respuesta = false;

	if (cCodigoPostalLaboral == "" || cPaisLaboral == "" || iEntidadFederativaLaboral == -1 || iCiudadLaboral == -1 || iMunicipioLaboral == -1 || iColoniaLaboral == -1 || cCalleLaboral == "" || cNumExteriorLaboral == "") {
		respuesta = true;
		bDomicilioLaboral = true;
	}
	else {
		respuesta = true;
	}

	/*
	if (cCodigoPostalLaboral != "" || cCodigoPostalLaboral =="" )
	{
		if (cPaisLaboral != "" || cPaisLaboral == "")
		{
			if (iEntidadFederativaLaboral != -1 || iEntidadFederativaLaboral == -1)
			{
				if (iCiudadLaboral != -1 || iCiudadLaboral == -1)
				{
					if (iMunicipioLaboral != -1 || iMunicipioLaboral == -1)
					{
						if (iColoniaLaboral != -1 || iColoniaLaboral == -1)
						{
							if (cCalleLaboral != "" || cCalleLaboral == "")
							{
								if (cNumExteriorLaboral != "" || cNumExteriorLaboral == "")
								{
									if (cNumInteriorLaboral != "" || cNumInteriorLaboral == "")
									{
										respuesta = true;
									}
									else
									{
										mensajeAdvertencia("Favor de proporcionar un numero interior laboral");
									}
								}
								else
								{
									mensajeAdvertencia("Favor de proporcionar un numero exterior laboral");
								}
							}
							else
							{
								mensajeAdvertencia("Favor de proporcionar la calle de su domicilio laboral");
							}
						}
						else
						{
							mensajeAdvertencia("Favor de proporcionar su colonia laboral");
						}
					}
					else
					{
						mensajeAdvertencia("Favor de proporcionar su ciudad laboral");
					}
				}
				else
				{
					mensajeAdvertencia("Favor de proporcionar su municipio laboral");
				}
			}
			else
			{
				mensajeAdvertencia("Favor de proporcionar su entidad federativa laboral");
			}
		}
		else
		{
			mensajeAdvertencia("Favor de proporcionar su pais laboral");
		}
	}
	else
	{
		mensajeAdvertencia("Favor de buscar su codigo postal laboral");
	}
	*/
	return respuesta;
}

//Funcion de javascript utilizada para validar los telefonos de contacto
//para que no se utilice el mismo
function validarTelefonos(iIdentificadorServicio) {
	//Variable de respuesta
	var bRespuesta = false;

	//Obtiene el valor del telefono1, telefono2
	var cTelefono1 = $("#txtTel1").val();
	var cTelefono2 = $("#txtTel2").val();

	//Valida que el tipo de servicio provenga de afiliacion
	if (iIdentificadorServicio == 26 || //Registro
		iIdentificadorServicio == 27 || //Traspaso
		iIdentificadorServicio == 33) //Independientes
	{
		//Valida si el telefono 2 es igual a alguno de los demas telefonos capturados
		if (cTelefono2 == cTelefono1) {
			//limpia los controles en caso de que el telefono capturado este repetido  y envia mensaje
			$("#txtTel2").val("");
			cTelefono2 = "";
			mensajeAdvertencia("El tel&eacute;fono de contacto 1 no debe ser igual al tel&eacute;fono de contacto 2.");
		} else {
			if (validarTelefonoListaNegra()) {
				bRespuesta = true;
			} else {
				mensajeAdvertencia("No se logro validar los telefonos de la lista negra.");
			}

		}
	}

	return bRespuesta;
}

function datosDocumentacion() {
	var iIdentificacionOficial = $("#slcIdentificacionOficial").val();
	var iComprobanteDomicilio = $("#slcComprobanteDomicilio").val();
	//var dtFechaVigencia = $("#dtFechaVigencia").val();
	var dtFechaComprobante = $("#dtFechaComprobante").val();
	var islcIdentificacionOficial = $("#slcIdentificacionOficial").val();
	var bBandera = false;

	if (islcIdentificacionOficial == 5) {
		if (iIdentificacionOficial != -1) {
			if (iComprobanteDomicilio != -1) {
				/*if (dtFechaVigencia == "")
				{*/
				if (dtFechaComprobante != "") {
					bBandera = true;
				}
				else {
					if ($("#slcComprobanteDomicilio").val() == 1) {
						bBandera = true;
					}
					else {
						mensajeAdvertencia("Proporcionar una fecha de comprobante de domicilio");
					}
				}
				/*}
				else
				{
					mensajeAdvertencia("Proporcionar una fecha de vigencia de la identificacion oficial");
				}*/
			}
			else {
				mensajeAdvertencia("Seleccionar un comprobante de domicilio");
			}
		}
		else {
			mensajeAdvertencia("Seleccionar una identificacion oficial");
		}
	}

	if (islcIdentificacionOficial != 5) {
		if (iIdentificacionOficial != -1) {
			if (iComprobanteDomicilio != -1) {
				/*if (dtFechaVigencia != "")
				{*/
				if (dtFechaComprobante != "") {
					bBandera = true;
				}
				else {
					if ($("#slcComprobanteDomicilio").val() == 1) {
						bBandera = true;
					}
					else {
						mensajeAdvertencia("Proporcionar una fecha de comprobante de domicilio");
					}
				}
				/*}
				else
				{
					mensajeAdvertencia("Proporcionar una fecha de vigencia de la identificacion oficial");
				}*/
			}
			else {
				mensajeAdvertencia("Seleccionar un comprobante de domicilio");
			}
		}
		else {
			mensajeAdvertencia("Seleccionar una identificacion oficial");
		}
	}

	return bBandera;
}

function llenarActividadGiroNegocio() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 7 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de Actividad o Giro del negocio");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de Actividad o giro del negocio, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcActividadNegocio").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.actividadgiro.length; i++) {
						$("#slcActividadNegocio").append("<option value=" + respuesta.actividadgiro[i].idactividadgiro + ">"
							+ respuesta.actividadgiro[i].descripcion + "</option>");
					}

				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta de Actividad o giro del negocio, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function llenarEntidadNacimiento() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 8 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de entidad de nacimiento");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de Entidad de Nacimiento , por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcEntidadNacimiento").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.entidadNacimiento.length; i++) {
						$("#slcEntidadNacimiento").append("<option value=" + respuesta.entidadNacimiento[i].identidadnacimiento + ">"
							+ respuesta.entidadNacimiento[i].descripcion + "</option>");
						arrEntidadNacimiento[i] = respuesta.entidadNacimiento[i].descorta;

					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de Entidad de Nacimiento, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}


function llenarIdentificacionOficial(sFechaNacimiento) {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 12,
			sFechaNacimiento: sFechaNacimiento
		},

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de identificaci&oacute;n");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de identificaci&oacute;n oficial , por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcIdentificacionOficial").empty();
					$("#slcIdentificacionOficial").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.identificacionesOficiales.length; i++) {
						$("#slcIdentificacionOficial").append("<option value=" + respuesta.identificacionesOficiales[i].tpoidentificador + ">"
							+ respuesta.identificacionesOficiales[i].descripcion + "</option>");
					}

				}
				else {
					mensajeAdvertencia("Favor de introducir una fecha de nacimiento");
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de identificaci&oacute;n oficial, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function llenarNivelEstudio() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 14 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de nivel de estudio");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de nivel de estudio , por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcNivelEstudios").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.nivelEstudio.length; i++) {
						$("#slcNivelEstudios").append("<option value=" + respuesta.nivelEstudio[i].idnivelestudio + ">"
							+ respuesta.nivelEstudio[i].descripcion + "</option>");
					}

				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de nivel de estudio, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function llenarNacionalidad() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 16 },

		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de nacionalidad");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de nacionalidad, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					//$("#slcNacionalidad").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.nacionalidad.length; i++) {
						$("#slcNacionalidad").append("<option value=" + respuesta.nacionalidad[i].idnacionalidad + ">"
							+ respuesta.nacionalidad[i].descripcion + "</option>");
					}

				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de nacionalidad, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function llenarComprobanteDomicilio() {
	sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 17 },


		beforeSend: function () {
			bloquearUI("Consultando el cat&aacute;logo de comprobante de domicilio");
		},

		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar el cat&aacute;logo de comprobante de domicilio, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {

					$("#slcComprobanteDomicilio").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.comprobanteDomicilio.length; i++) {
						$("#slcComprobanteDomicilio").append("<option value=" + respuesta.comprobanteDomicilio[i].idcomprobante + ">"
							+ respuesta.comprobanteDomicilio[i].descripcion + "</option>");
					}

				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de comprobante de domicilio, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

/**************************************<WEBSERVICE EXE>**************************************/
/**
 * OPCION WEBSERVICE
 */
function opcionEjecuta() {
	var sParametros = '';
	var sComando = '';
	var sGenero = "";
	var sFolioLocal = "";
	var cEntidadNacimiento1 = "";

	var sMensajeBloqueo = "";

	switch (iBoton) {
		case 1:
			//Esta conformado por: folio, numeroempleado y 1 fijo.
			if (OSName == "Android") {
				if (opcionEnrol == 1) {
					sMensajeBloqueo = "Capturando huellas del cliente...";
					bloquearUI(sMensajeBloqueo);
					setTimeout(function () {
						Android.llamaComponenteHuellas(4, 0, 1, 0, iNumeroEmpleado, iTipoServicio, cCurp);
					}, 4000);
				} else {
					sMensajeBloqueo = "Capturando huellas del cliente...";
					bloquearUI(sMensajeBloqueo);
					setTimeout(function () {
						Android.llamaComponenteHuellas(4, 0, 1, 1, iNumeroEmpleado, iTipoServicio, cCurp);
					}, 4000);
				}
			} else {
				sParametros = cFolio + " " + iNumeroEmpleado + " 1";
				sRuta = "C:\\sys\\mAfore\\ClientesAforeOL.exe";
				sMensajeBloqueo = "Capturando huellas del cliente...";
			}
			break;
		case 2:
			if (OSName == "Android") {
				sMensajeBloqueo = "Digitalizando documentos...";
				bloquearUI(sMensajeBloqueo);
				setTimeout(function () {
					Android.llamaComponenteDigitalizador(cFolio, iNumeroEmpleado, 0, 6, iTipoServicio);
				}, 4000);
			} else {
				if (iTipoServicio == 26 || iTipoServicio == 27 || iTipoServicio == 33) {
					sFolioLocal = cFolio;
				}
				else {
					sFolioLocal = cFolio + "-S";
				}
				sParametros = "2" + " " + "6" + " " + sFolioLocal + " " + iNumeroEmpleado;
				sRuta = "C:\\SYS\\PAFSCANIMGNET\\DIGIDOCTOSCLIENTE.EXE";
				sMensajeBloqueo = "Digitalizando documentos...";
			}
			break;
		default:
			break;
	}
	bloquearUI(sMensajeBloqueo);
	ejecutaWebService(sRuta, sParametros);
}

/**
 * EJECUTA WEBSERVICE
 */
function ejecutaWebService(sRuta, sParametros) {
	if (OSName == "Windows") {
		soapData = "",
			httpObject = null,
			docXml = null,
			iEstado = 0,
			sMensaje = "";
		sUrlSoap = "http://127.0.0.1:20044/";

		soapData =
			'<?xml version=\"1.0\" encoding=\"UTF-8\"?>' +
			'<SOAP-ENV:Envelope' +
			' xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\"' +
			' xmlns:SOAP-ENC=\"http://schemas.xmlsoap.org/soap/encoding/\"' +
			' xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"' +
			' xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"' +
			' xmlns:ns2=\"urn:ServiciosWebx\">' +
			'<SOAP-ENV:Body  SOAP-ENV:encodingStyle=\"http://schemas.xmlsoap.org/soap/encoding/\">' +
			'<ns2:ejecutarAplicacion>' +
			'<inParam>' +
			'<Esperar>1</Esperar>' +
			'<RutaAplicacion>' + sRuta + '</RutaAplicacion>' +
			'<parametros>' + sParametros + '</parametros>' +
			'</inParam>' +
			'</ns2:ejecutarAplicacion>' +
			'</SOAP-ENV:Body>' +
			'</SOAP-ENV:Envelope>';

		httpObject = getHTTPObject();

		if (httpObject) {
			if (httpObject.overrideMimeType) {
				httpObject.overrideMimeType("false");
			}

			httpObject.open('POST', sUrlSoap, false); //-- no asincrono
			httpObject.setRequestHeader("Accept-Language", null);
			httpObject.onreadystatechange = function () {
				if (httpObject.readyState == 4 && httpObject.status == 200) {
					parser = new DOMParser();
					docXml = parser.parseFromString(httpObject.responseText, "text/xml");
					iEstado = docXml.getElementsByTagName('Estado')[0].childNodes[0].nodeValue;
					//Llamada a la función que recibe la respuesta del WebService
					respuestaWebService(iEstado);
				}
				else {
					console.log(httpObject.status);
				}
			};
			httpObject.send(soapData);
		}
	}
}

function getHTTPObject() {
	var xhr = false;
	if (window.ActiveXObject) {
		try {
			xhr = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				xhr = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				xhr = false;
			}
		}
	} else if (window.XMLHttpRequest) {
		try {
			xhr = new XMLHttpRequest();
		} catch (e) {
			xhr = false;
		}
	}
	return xhr;
}

/**
 * RESPUESTA WEBSERVICE
 */
function respuestaWebService(iRespuesta) {
	desbloquearUI();
	var sMensaje = "RESPUESTA APPLET: " + iRespuesta;
	if (iBoton == 1) {
		if (iRespuesta == 1 || iRespuesta == 7) //si es exito, se desabilita el boton
		{
			if (iEstatusExpediente == 0)
				guardarDatos();
			else
				mensajeAdvertenciaFinal("El expediente para este cliente ya fue publicado.");
		}
	}
	else if (iBoton == 2) {
		if (iRespuesta == 1) {
			cerrarNavegador();//790 se cierra el navegador en lugar de mandar el mensaje exitoso
			/* mensajeExitoFinal("El Expediente de Identificaci&oacute;n se gener&oacute; con &eacute;xito"); *///se comenta el msj exitoso folio 790
			$("#contenedorPrincipal").hide();
		}
		else
			mensajeAdvertenciaFinal("El Expediente de Identificaci&oacute;n no se digitaliz&oacute; correctamente, por favor, reporte a Mesa de Ayuda.");
		//actualizarEstatusExpediente(iRespuesta);
	}
}

/**************************************</WEBSERVICE EXE>**************************************/


function obtenerEntidadNacimiento(cIdEstado) {
	var sSiglas = "";
	var sPhp = 'php/expedienteIdentificacion.php';

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 9,
			cIdEstado: cIdEstado
		},
		error: function (xhr, ajaxOptions, thrownError) {
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener la entidad de nacimiento , por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			try {
				if (respuesta.codigoRespuesta == 1) {
					sSiglas = respuesta.siglasEstado;
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener la entidad de nacimiento, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});

	return sSiglas;
}

function getQueryVariable(varGet) {
	//Obtener la url compleata del navegador
	var sUrl = window.location.search.substring(1);
	//Separar los parametros junto con su valor
	var vars = sUrl.split("&");
	for (var i = 0; i < vars.length; i++) {
		//Obtener el parametro y su valor en arreglo de 2
		var par = vars[i].split("=");
		if (par[0] == varGet) // [0] nombre de variable, [1] valor
		{
			return par[1];
		}
	}
	return (false);
}

function validarPerfilPromotor(iNumeroEmpleado) {

	var sPhp = "php/expedienteIdentificacion.php";
	consultaestatusbancoppel();

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 15,
			iNumeroEmpleado: iNumeroEmpleado
		},

		beforeSend: function () {
			bloquearUI("Consultando folio asignado para el ejecutivo ");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error de conexi&oacute;n grave, por favor reporte a Mesa de ayuda.");
			console.log = thrownError;
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta === 1) {
					if (cFolio.length > 0) {
						if (iTipoServicio.length > 0) {
							if (iExisteEi.length > 0) {
								$("#contenedorPrincipal").show();
								if (iTipoServicio == 26 || iTipoServicio == 27 || iTipoServicio == 33) {
									obtenerInformacionTrabajadorAfiliacion();
									
									if(iTipoServicio == 27){
										if(verificarServicioEi()){ // Servicio 0407
											obtenerExpedienteProcesar(); 
										}
									}
									
									var tiempo = 1000;
									/*Se comenta los metodos extraerDatosBancoppel y extraerDatosRenapo ya que se cargara
									 la fecha de nacimiento, entidad, genero y nacionalidad a partir de la curp derivada de la incidencia
									 409 en la lista de pendientes de incidencias afore*/
									// setTimeout(function (){
									/*extraerDatosBancoppel(cFolio);
								// },tiempo);
								//Se agrega funcionalidad para que obtenga datos del trabajador por medio
								//de la consulta de CURP a RENAPO si esta se realizo para este Folio.
								extraerDatosRenapo(cFolio);*/
								}
								else if (iExisteEi == 1) {
									obtenerExpedienteDeIdentificacion();
								}
								else {
									obtenerInformacionTrabajador();
								}
							}
							else {
								$("#contenedorPrincipal").hide();
								mensajeAdvertencia("El Expediente de Identificaci&oacute;n no ha sido lanzado correctamente, por favor, reporte a Mesa de Ayuda.");
							}
						}
						else {
							$("#contenedorPrincipal").hide();
							mensajeAdvertencia("El Expediente de Identificaci&oacute;n no ha sido lanzado correctamente, por favor, reporte a Mesa de Ayuda.");
						}
					}
					else {
						$("#contenedorPrincipal").hide();
						mensajeAdvertencia("El Expediente de Identificaci&oacute;n no ha sido lanzado correctamente, por favor, reporte a Mesa de Ayuda.");
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ha ocurrido un error al consultar la respuesta del servicio, por favor reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function ocultarBotones() {
	$("#txtCpOculto").hide();
	$("#txtColoniaOculto").hide();
	$("#txtDeleMuniOculto").hide();
	$("#txtEntidadFederativaOculto").hide();
	$("#txtCiudadOculto").hide();
	$("#txtCpLaboralOculto").hide();
	$("#txtColoniaLaboralOculto").hide();
	$("#txtDeleMuniLaboralOculto").hide();
	$("#txtEntidadFederativaLaboralOculto").hide();
	$("#txtCiudadLaboralOculto").hide();
	$("#txtNssOculto").hide();
}

function obtenerInformacionTrabajadorAfiliacion() {
	var sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 18,
			cFolio: cFolio,
			iTipoServicio: iTipoServicio
		},
		beforeSend: function () {
			bloquearUI("Consultando informacion del trabajador ");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error de conexi&oacute;n grave, por favor reporte a Mesa de ayuda.");
			console.log = thrownError;
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta === 1) {
					var cRfc = "";
					cCurp = "";
					
					$("#txtApellidoPaterno").val(respuesta.trabajador[0].appaterno);
					$("#txtApellidoMaterno").val(respuesta.trabajador[0].apmaterno);
					$("#txtNombres").val(respuesta.trabajador[0].nombres);
					$("#txtCurp").val(respuesta.trabajador[0].curp);
					cCurp = $("#txtCurp").val();
					cRfc = cCurp.substr(0, 10);
					$("#txtRfc").val(cRfc);

					if (respuesta.trabajador[0].nss.charAt(0) != 'I') {
						$("#txtNss").val(respuesta.trabajador[0].nss);
					}
					$("#txtNssOculto").val(respuesta.trabajador[0].nss)
					$("#slcTel1").val(respuesta.trabajador[0].tipotelefono);
					$("#txtTel1").val(respuesta.trabajador[0].telefonocelular);
					$("#slcTel2").val(respuesta.trabajador[0].comptelefonicacontacto);
					$("#txtTel2").val(respuesta.trabajador[0].telefonocontacto);
					if (respuesta.trabajador[0].codigopostal != '00000') {
						$("#txtCodigoPostal").val(respuesta.trabajador[0].codigopostal);
						codigopostal = respuesta.trabajador[0].codigopostal;
					}
					
					//$("#txtCodigoPostal").val(respuesta.trabajador[0].codigopostal);
					$("#txtEntidadFederativa").val(respuesta.trabajador[0].estado);
					$("#txtCiudad").val(respuesta.trabajador[0].ciudad);
					$("#txtDeleMuni").val(respuesta.trabajador[0].delegmunic);
					$("#txtColonia").val(respuesta.trabajador[0].colonia);
					$("#txtCalle").val(respuesta.trabajador[0].calle);
					$("#txtNumExterior").val(respuesta.trabajador[0].numexterior);
					$("#txtNumInterior").val(respuesta.trabajador[0].numinterior);

					$("#txtEntidadFederativaOculto").val(respuesta.trabajador[0].idestado);
					$("#txtCiudadOculto").val(respuesta.trabajador[0].idciudad);
					$("#txtDeleMuniOculto").val(respuesta.trabajador[0].iddelegmunic);
					$("#txtColoniaOculto").val(respuesta.trabajador[0].idcolonia);
					$("#txtCpOculto").val(respuesta.trabajador[0].codigopostal);



					//////Inicio cargar los datos anteriores 94.1
					$("#slcOcupacionProfesion").val(respuesta.trabajador[1].ocupacion);
					$("#slcActividadNegocio").val(respuesta.trabajador[1].actividadgironegocio);
					$("#slcNivelEstudios").val(respuesta.trabajador[1].nivelestudio);

					$("#slcTel2").val(respuesta.trabajador[1].tipotelefono02);
					$("#txtTel2").val(respuesta.trabajador[1].telefono02);
					$("#txtCorreoElectronico").val(respuesta.trabajador[1].correoelectronico);

					if (respuesta.trabajador[1].tipocorreoelectronico == 'hotmail.com') {
						$("#slcCorreoElectronico").val(1);
					}
					if (respuesta.trabajador[1].tipocorreoelectronico == 'yahoo.com.mx') {
						$("#slcCorreoElectronico").val(2);
					}
					if (respuesta.trabajador[1].tipocorreoelectronico == 'yahoo.com') {
						$("#slcCorreoElectronico").val(3);
					}
					if (respuesta.trabajador[1].tipocorreoelectronico == 'gmail.com') {
						$("#slcCorreoElectronico").val(4);
					}
					if (respuesta.trabajador[1].tipocorreoelectronico == 'outlook.es') {
						$("#slcCorreoElectronico").val(5);
					}
					if (respuesta.trabajador[1].tipocorreoelectronico == 'outlook.com') {
						$("#slcCorreoElectronico").val(6);
					}
					if (respuesta.trabajador[1].tipocorreoelectronico == 'otro') {
						$("#slcCorreoElectronico").val(7);
					}

					$("#slcCorreoElectronico").trigger("change");
					
					habilitaConfirmacionCorreo();

					$("#txtCodigoPostalLaboral").val(respuesta.trabajador[1].codigopostallaboral);
					$("#txtEntidadFederativaLaboral").val(respuesta.trabajador[1].estadolaboral);
					$("#txtDeleMuniLaboral").val(respuesta.trabajador[1].delegmuniclaboral);
					$("#txtCiudadLaboral").val(respuesta.trabajador[1].ciudadlaboral);
					$("#txtColoniaLaboral").val(respuesta.trabajador[1].colonialaboral);
					$("#txtCalleLaboral").val(respuesta.trabajador[1].callelaboral);
					$("#txtNumExteriorLaboral").val(respuesta.trabajador[1].numexteriorlaboral);
					$("#txtNumInteriorLaboral").val(respuesta.trabajador[1].numinteriorlaboral);

					$("#txtCpLaboralOculto").val(respuesta.trabajador[1].codigopostallaboral);
					$('#txtEntidadFederativaLaboralOculto').val(respuesta.trabajador[1].idestadolaboral);
					$("#txtCiudadLaboralOculto").val(respuesta.trabajador[1].idciudadlaboral);
					$("#txtDeleMuniLaboralOculto").val(respuesta.trabajador[1].iddelegmuniclaboral);
					$('#txtColoniaLaboralOculto').val(respuesta.trabajador[1].idcolonialaboral);


					//////Fin cargar los datos anteriores 94.1



					$("#txtFechaNacimiento").val(formatearFecha(3, respuesta.trabajador[0].fechanacimiento));

					sFechaNacimiento = formatearFecha(2, $("#txtFechaNacimiento").val());
					llenarIdentificacionOficial(sFechaNacimiento);

					var entidadDatosNom = respuesta.trabajador[0].entidadnacimiento;
					if (entidadDatosNom == '9') { entidadDatosNom = '09'; }
					if (entidadDatosNom == '8') { entidadDatosNom = '08'; }
					if (entidadDatosNom == '7') { entidadDatosNom = '07'; }
					if (entidadDatosNom == '6') { entidadDatosNom = '06'; }
					if (entidadDatosNom == '5') { entidadDatosNom = '05'; }
					if (entidadDatosNom == '4') { entidadDatosNom = '04'; }
					if (entidadDatosNom == '3') { entidadDatosNom = '03'; }
					if (entidadDatosNom == '2') { entidadDatosNom = '02'; }
					if (entidadDatosNom == '1') { entidadDatosNom = '01'; }
					$("#slcEntidadNacimiento").val(entidadDatosNom);

					$("#slcNacionalidad").val(respuesta.trabajador[0].nacionalidad);



					//Genero
					var genero = respuesta.trabajador[0].genero;
					if (genero == '1') {
						$("#slcGeneroTrabajador").val(1);
					}
					else if (genero == '2') {
						$("#slcGeneroTrabajador").val(2);
					}

					//Nacionalidad
					cEsExtranjero = respuesta.trabajador[0].esextranjero;
					if (cEsExtranjero == '1') {
						$("#slcNacionalidad").prop("disabled", false);
					}
					else {
						$("#slcNacionalidad").prop("disabled", true);
					}

					dtFechaServicio = respuesta.trabajador[0].fechageneraconstancia;

					$("#slcIdentificacionOficial").hide();
					$("#slcComprobanteDomicilio").hide();
					//$("#dtFechaVigencia").hide();
					$("#dtFechaComprobante").hide();
					$("#lblIdentificacionOficial").hide();
					$("#lblFechaVigencia").hide();
					$("#lblComprobanteDomicilio").hide();
					$("#lblFechaComprobante").hide();
					$("#InformacionDocumentacion").hide();

					$("#txtApellidoPaterno").prop("disabled", true);
					$("#txtApellidoMaterno").prop("disabled", true);
					$("#txtNombres").prop("disabled", true);
					$("#txtCurp").prop("disabled", true);
					$("#txtNss").prop("disabled", true);
					$("#slcTel1").prop("disabled", true);
					$("#txtTel1").prop("disabled", true);

					/*//Valida si se capturo el telefono de contacto 2 en la solicitud de constancia
					if( $("#txtTel2").val() == "" )
					{
						//Habilita el control para que puedan indicar el tipo de telefono en caso de
						//que no se haya capturado el telefono en la solicitud de constancia
						$("#slcTel2").prop("disabled", false);
						//habilita el control para que puedan capturar telefono de contacto en caso de
						//que no se haya capturado en la solicitud de constancia
						$("#txtTel2").prop("disabled", false);
					}
					else
					{
						$("#slcTel2").prop("disabled", true);
						$("#txtTel2").prop("disabled", true);
					}*/

					$("#txtCodigoPostal").prop("disabled", true);
					$("#txtEntidadFederativa").prop("disabled", false);
					$("#txtCiudad").prop("disabled", false);
					$("#txtDeleMuni").prop("disabled", false);
					$("#txtColonia").prop("disabled", false);
					$("#txtCalle").prop("disabled", false);
					$("#txtNumExterior").prop("disabled", false);
					$("#txtNumInterior").prop("disabled", false);
					$("#txtEntidadFederativaOculto").prop("disabled", true);
					$("#txtCiudadOculto").prop("disabled", true);
					$("#txtDeleMuniOculto").prop("disabled", true);
					$("#txtColoniaOculto").prop("disabled", true);
					$("#txtCpOculto").prop("disabled", true);
					$("#btnBuscarCodigoPostal").prop("disabled", false);


					$("#slcOcupacionProfesion").prop("disabled", false);
					$("#slcActividadNegocio").prop("disabled", false);
					$("#slcNivelEstudios").prop("disabled", false);
					//$("#slcNacionalidad").prop("disabled",false);
					$("#slcEntidadNacimiento").prop("disabled", true);
					$("#slcCorreoElectronico").prop("disabled", false);
					$("#slcGeneroTrabajador").prop("disabled", true);
					$("#txtFechaNacimiento").prop("disabled", true);
					$("#slcComprobanteDomicilio").prop("disabled", false);
					$("#slcIdentificacionOficial").prop("disabled", false);

					if (iTipoServicio == 27 || iTipoServicio == 33) {
						$("#chBoxTrabajadorfallecido").prop("disabled", true);
					}
					if(iTipoServicio == 27)
					{
						obtenerExpedienteProcesar();
						if(bProcesar == 1)
						{
							
						}
						else
						{
							if(bBancoppel == 1)
							{
								consultarColonia();
							}
						}
					}
					else
					{
						if(bBancoppel == 1)
						{
							consultarColonia();
						}
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
					$("#contenedorPrincipal").hide();
				}
			}
			catch (err) {

				mensajeAdvertenciaFinal("Ha ocurrido un error al consultar la respuesta del servicio, por favor reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function obtenerInformacionTrabajador() {
	var sPhp = "php/expedienteIdentificacion.php";
	var sFechaNacimiento = "";
	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 19,
			cFolio: cFolio
		},
		beforeSend: function () {
			bloquearUI("Consultando informacion del trabajador ");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error de conexi&oacute;n grave, por favor reporte a Mesa de ayuda.");
			console.log(xhr);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta === 1) {
					var cRfc = "";
					var cCurp = "";
					
					$("#txtApellidoPaterno").val(respuesta.trabajador.cApellidoPaterno);
					$("#txtApellidoMaterno").val(respuesta.trabajador.cApellidoMaterno);
					$("#txtNombres").val(respuesta.trabajador.cNombre);
					$("#txtCurp").val(respuesta.trabajador.cCurp);

					cCurp = $("#txtCurp").val();
					cRfc = cCurp.substr(0, 10);
					$("#txtRfc").val(cRfc);

					if (respuesta.trabajador.cNss.charAt(0) != 'I') {
						$("#txtNss").val(respuesta.trabajador.cNss);
					}
					$("#txtNssOculto").val(respuesta.trabajador.cNss)
					$("#txtFechaNacimiento").val(respuesta.trabajador.cFechaNacimiento);
					$("#slcEntidadNacimiento").val(respuesta.trabajador.cEntidadNacimiento);
					$("#slcGeneroTrabajador").val(respuesta.trabajador.iSexo);

					dtFechaServicio = respuesta.trabajador.cFechaServicio;

					sFechaNacimiento = $("#txtFechaNacimiento").val();
					llenarIdentificacionOficial(sFechaNacimiento);

					$("#txtFechaNacimiento").val(formatearFecha(3, sFechaNacimiento));

					if (iTipoServicio != 2022) {
						$("#txtApellidoPaterno").val(respuesta.trabajador.cApellidoPaterno);
						$("#txtApellidoMaterno").val(respuesta.trabajador.cApellidoMaterno);
						$("#txtNombres").val(respuesta.trabajador.cNombre);
						$("#txtCurp").val(respuesta.trabajador.cCurp);

						cCurp = $("#txtCurp").val();
						cRfc = cCurp.substr(0, 10);
						$("#txtRfc").val(cRfc);

						if (respuesta.trabajador.cNss.charAt(0) != 'I') {
							$("#txtNss").val(respuesta.trabajador.cNss);
						}

						$("#txtNssOculto").val(respuesta.trabajador.cNss)
						$("#txtFechaNacimiento").val(respuesta.trabajador.cFechaNacimiento);
						$("#slcEntidadNacimiento").val(respuesta.trabajador.cEntidadNacimiento);
						$("#slcGeneroTrabajador").val(respuesta.trabajador.iSexo);

						dtFechaServicio = respuesta.trabajador.cFechaServicio;

						sFechaNacimiento = $("#txtFechaNacimiento").val();
						llenarIdentificacionOficial(sFechaNacimiento);

						$("#txtFechaNacimiento").val(formatearFecha(3, sFechaNacimiento));


						$("#txtApellidoPaterno").prop("disabled", true);
						$("#txtApellidoMaterno").prop("disabled", true);
						$("#txtNombres").prop("disabled", true);
						$("#txtCurp").prop("disabled", true);
						$("#txtNss").prop("disabled", true);
						$("#txtFechaNacimiento").prop("disabled", true);
						$("#slcEntidadNacimiento").prop("disabled", true);
						$("#slcGeneroTrabajador").prop("disabled", true);
						$("#slcNacionalidad").prop("disabled", true);

						$("#txtRfc").prop("disabled", false);
						$("#slcOcupacionProfesion").prop("disabled", false);
						$("#slcActividadNegocio").prop("disabled", false);
						$("#slcNivelEstudios").prop("disabled", false);
						$("#txtCalle").prop("disabled", false);
						$("#txtNumInterior").prop("disabled", false);
						$("#txtNumExterior").prop("disabled", false);
						$("#btnBuscarCodigoPostal").prop("disabled", false);
						$("#txtTel1").prop("disabled", false);
						$("#txtTel2").prop("disabled", false);
						$("#slcTel1").prop("disabled", false);
						$("#slcTel2").prop("disabled", false);
						$("#txtCorreoElectronico").prop("disabled", false);
						$("#slcCorreoElectronico").prop("disabled", false);
						$("#txtCalleLaboral").prop("disabled", false);
						$("#txtNumInteriorLaboral").prop("disabled", false);
						$("#txtNumExteriorLaboral").prop("disabled", false);
						$("#btnBuscarCodigoPostalLaboral").prop("disabled", false);
						$("#slcIdentificacionOficial").attr("disabled", false);
						$("#slcComprobanteDomicilio").attr("disabled", false);
						$("#chBoxTrabajadorfallecido").hide();
						$("#chkRegistroTrabajador").hide();
						$("#lblRegistroFallecido").hide();
					}
					else {

						$("#txtApellidoPaterno").val(respuesta.trabajador.cApellidoPaterno);
						$("#txtApellidoMaterno").val(respuesta.trabajador.cApellidoMaterno);
						$("#txtNombres").val(respuesta.trabajador.cNombre);
						$("#txtCurp").val(respuesta.trabajador.cCurp);

						cCurp = $("#txtCurp").val();
						cRfc = cCurp.substr(0, 10);
						$("#txtRfc").val(cRfc);

						if (respuesta.trabajador.cNombre.charAt(0) != 'I') {
							$("#txtNss").val(respuesta.trabajador.cNss);
						}
						$("#txtNssOculto").val(respuesta.trabajador.cNss)
						$("#txtFechaNacimiento").val(respuesta.trabajador.cFechaNacimiento);
						$("#slcEntidadNacimiento").val(respuesta.trabajador.cEntidadNacimiento);
						$("#slcGeneroTrabajador").val(respuesta.trabajador.iSexo);

						dtFechaServicio = respuesta.trabajador.cFechaServicio;

						sFechaNacimiento = $("#txtFechaNacimiento").val();
						llenarIdentificacionOficial(sFechaNacimiento);

						$("#txtFechaNacimiento").val(formatearFecha(3, sFechaNacimiento));

						$("#txtApellidoPaterno").prop("disabled", false);
						$("#txtApellidoMaterno").prop("disabled", false);
						$("#txtNombres").prop("disabled", false);
						$("#txtCurp").prop("disabled", false);
						$("#txtNss").prop("disabled", true);
						$("#txtFechaNacimiento").prop("disabled", true);
						$("#slcEntidadNacimiento").prop("disabled", true);
						$("#slcGeneroTrabajador").prop("disabled", true);
						$("#slcNacionalidad").prop("disabled", false);

						$("#txtRfc").prop("disabled", false);
						$("#slcOcupacionProfesion").prop("disabled", false);
						$("#slcActividadNegocio").prop("disabled", false);
						$("#slcNivelEstudios").prop("disabled", false);
						$("#txtCalle").prop("disabled", false);
						$("#txtNumInterior").prop("disabled", false);
						$("#txtNumExterior").prop("disabled", false);
						$("#btnBuscarCodigoPostal").prop("disabled", false);
						$("#txtTel1").prop("disabled", false);
						$("#txtTel2").prop("disabled", false);
						$("#slcTel1").prop("disabled", false);
						$("#slcTel2").prop("disabled", false);
						$("#txtCorreoElectronico").prop("disabled", false);
						$("#slcCorreoElectronico").prop("disabled", false);
						$("#txtCalleLaboral").prop("disabled", false);
						$("#txtNumInteriorLaboral").prop("disabled", false);
						$("#txtNumExteriorLaboral").prop("disabled", false);
						$("#btnBuscarCodigoPostalLaboral").prop("disabled", false);
						$("#slcIdentificacionOficial").attr("disabled", false);
						$("#slcComprobanteDomicilio").attr("disabled", false);

						$("#chBoxTrabajadorfallecido").hide();
						$("#chkRegistroTrabajador").hide();
						$("#lblRegistroFallecido").hide();
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
					$("#contenedorPrincipal").hide();
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ha ocurrido un error al consultar la respuesta del servicio, por favor reporte a Mesa de ayuda.");
				$("#contenedorPrincipal").hide();
				console.log(err);
			}
		}
	});
}

function obtenerExpedienteDeIdentificacion() {
	var sPhp = "php/expedienteIdentificacion.php";
	var sFechaNacimiento = "";
	var arrayCorreo = {};

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 20,
			cFolio: cFolio
		},
		beforeSend: function () {
			bloquearUI("Consultando expediente de indentificaci&oacute;n del trabajador ");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();

			mensajeAdvertenciaFinal("Ocurri&oacute; un error de conexi&oacute;n grave, por favor reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta === 1) {

					var cPais = "M\u00C9XICO";
					var cCorreoElectronico = "";
					var cRfc = "";
					var cCurp = "";

					$("#txtApellidoPaterno").val(respuesta.trabajador[0].apellidopaterno);
					$("#txtApellidoMaterno").val(respuesta.trabajador[0].apellidomaterno);
					$("#txtNombres").val(respuesta.trabajador[0].nombre);
					$("#txtCurp").val(respuesta.trabajador[0].curp);

					cCurp = $("#txtCurp").val();
					cRfc = cCurp.substr(0, 10);



					if (respuesta.trabajador[0].nss.charAt(0) != 'I') {
						$("#txtNss").val(respuesta.trabajador[0].nss);
					}

					$("#txtNssOculto").val(respuesta.trabajador[0].nss)
					if (respuesta.trabajador[0].rfc != "") {
						$("#txtRfc").val(respuesta.trabajador[0].rfc);
					}
					else {
						cRfc = cCurp.substr(0, 10);
						$("#txtRfc").val(cRfc);
					}

					$("#slcGeneroTrabajador").val(respuesta.trabajador[0].genero);
					$("#slcEntidadNacimiento").val(respuesta.trabajador[0].entidaddenacimiento);
					$("#slcNacionalidad").val(respuesta.trabajador[0].nacionalidad);
					$("#slcTel1").val(respuesta.trabajador[0].tipotelefono01);
					$("#txtTel1").val(respuesta.trabajador[0].telefono01);
					$("#slcTel2").val(respuesta.trabajador[0].tipotelefono02);
					$("#txtTel2").val(respuesta.trabajador[0].telefono02);
					$("#slcNivelEstudios").val(respuesta.trabajador[0].nivelestudio);
					$("#slcOcupacionProfesion").val(respuesta.trabajador[0].ocupacion);
					$("#slcActividadNegocio").val(respuesta.trabajador[0].actividadgironegocio);
					$("#txtFechaNacimiento").val(formatearFecha(3, respuesta.trabajador[0].fechanacimiento));
					if (respuesta.trabajador[0].correoelectronico.length > 0) {
						arrayCorreo = respuesta.trabajador[0].correoelectronico.split("@");
						$("#txtCorreoElectronico").val(arrayCorreo[0]);
						$("#slcCorreoElectronico option").each(function () {
							this.selected = (this.text == arrayCorreo[1]);
						});
						if ($("#slcCorreoElectronico").val() == -1) {
							$("#slcCorreoElectronico").val(7);
							$("#txtOtroDominio").val(arrayCorreo[1]);
							$("#txtOtroDominio").prop({ disabled: false });
						}
						$("#slcCorreoElectronico").trigger("change");
					}
						habilitaConfirmacionCorreo();
					$("#txtCpOculto").val(respuesta.domicilio[0].codigopostal);
					$("#txtEntidadFederativaOculto").val(respuesta.domicilio[0].entidadfederativa);
					$("#txtCiudadOculto").val(respuesta.domicilio[0].ciudad);
					$("#txtDeleMuniOculto").val(respuesta.domicilio[0].delegmunicipio);
					$("#txtColoniaOculto").val(respuesta.domicilio[0].colonia);

					// if(respuesta.domicilio[0].codigopostal != '00000')
					// {
					// $("#txtCodigoPostal").val(respuesta.domicilio[0].codigopostal);
					// }
					alert("calle");
					$("#txtCalle").val(respuesta.domicilio[0].calle);
					$("#txtCodigoPostal").val(respuesta.domicilio[0].codigopostal);
					$("#txtEntidadFederativa").val(respuesta.domicilio[0].nombreestado);
					$("#txtCiudad").val(respuesta.domicilio[0].nombreciudad);
					$("#txtDeleMuni").val(respuesta.domicilio[0].nombremunicipio);
					$("#txtColonia").val(respuesta.domicilio[0].nombrecolonia);
					$("#txtPais").val(cPais);
					$("#txtNumExterior").val(respuesta.domicilio[0].numeroexterior);
					$("#txtNumInterior").val(respuesta.domicilio[0].numerointerior);


					if (respuesta.domicilio.length == 2) {
						$("#txtCpLaboralOculto").val();
						$("#txtEntidadFederativaLaboralOculto").val(respuesta.domicilio[1].entidadfederativa);
						$("#txtCiudadLaboralOculto").val(respuesta.domicilio[1].ciudad);
						$("#txtDeleMuniLaboralOculto").val(respuesta.domicilio[1].delegmunicipio);
						$("#txtColoniaLaboralOculto").val(respuesta.domicilio[1].colonia);


						$("#txtCalleLaboral").val(respuesta.domicilio[1].calle);
						$("#txtCodigoPostalLaboral").val(respuesta.domicilio[1].codigopostal);
						$("#txtEntidadFederativaLaboral").val(respuesta.domicilio[1].nombreestado);
						$("#txtCiudadLaboral").val(respuesta.domicilio[1].nombreciudad);
						$("#txtDeleMuniLaboral").val(respuesta.domicilio[1].nombremunicipio);
						$("#txtColoniaLaboral").val(respuesta.domicilio[1].nombrecolonia);
						$("#txtNumExteriorLaboral").val(respuesta.domicilio[1].numeroexterior);
						$("#txtNumInteriorLaboral").val(respuesta.domicilio[1].numerointerior);
					}

					//dtFechaServicio = formatearFecha(3,respuesta.trabajador[0].dtFechaServicio);
					dtFechaServicio = respuesta.trabajador[0].dtFechaServicio;

					sFechaNacimiento = formatearFecha(2, $("#txtFechaNacimiento").val());
					llenarIdentificacionOficial(sFechaNacimiento);



					if (iTipoServicio != 2022) {

						$("#txtNss").prop("disabled", true);
						$("#txtApellidoPaterno").prop("disabled", true);
						$("#txtApellidoMaterno").prop("disabled", true);
						$("#txtNombres").prop("disabled", true);
						$("#txtCurp").prop("disabled", true);
						$("#txtNss").prop("disabled", true);
						$("#txtRfc").prop("disabled", true);
						$("#txtFechaNacimiento").prop("disabled", true);
						$("#slcGeneroTrabajador").prop("disabled", true);
						$("#slcNacionalidad").prop("disabled", true);
						$("#slcTel1").prop("disabled", true);
						$("#txtTel1").prop("disabled", true);
						$("#slcTel2").prop("disabled", true);
						$("#txtTel2").prop("disabled", true);
						$("#slcNivelEstudios").prop("disabled", true);
						$("#slcOcupacionProfesion").prop("disabled", true);
						$("#slcActividadNegocio").prop("disabled", true);
						$("#txtCodigoPostalLaboral").prop("disabled", true);
						$("#btnBuscarCodigoPostalLaboral").prop("disabled", true);
						$("#txtPaisLaboral").prop("disabled", true);
						$("#txtEntidadFederativaLaboral").prop("disabled", true);
						$("#txtDeleMuniLaboral").prop("disabled", true);
						$("#txtCiudadLaboral").prop("disabled", true);
						$("#txtColoniaLaboral").prop("disabled", true);
						$("#txtCalleLaboral").prop("disabled", true);
						$("#txtNumExteriorLaboral").prop("disabled", true);
						$("#txtNumInteriorLaboral").prop("disabled", true);
						$("#txtCorreoElectronico").prop("disabled", true);

						$("#txtCalle").prop("disabled", false);
						$("#txtNumInterior").prop("disabled", false);
						$("#txtNumExterior").prop("disabled", false);
						$("#btnBuscarCodigoPostal").prop("disabled", false);
						$("#txtTel1").prop("disabled", false);
						$("#txtTel2").prop("disabled", false);
						$("#slcTel1").prop("disabled", false);
						$("#slcTel2").prop("disabled", false);
						$("#txtCorreoElectronico").prop("disabled", false);
						$("#slcCorreoElectronico").prop("disabled", false);
						$("#slcComprobanteDomicilio").prop("disabled", false);
						$("#slcIdentificacionOficial").prop("disabled", false);
						$("#btnCapturaBeneficiario").prop("disabled", true);

						$("#chBoxTrabajadorfallecido").hide();
						$("#chkRegistroTrabajador").hide();
						$("#lblRegistroFallecido").hide();
					}
					else {

						$("#txtApellidoPaterno").prop("disabled", false);
						$("#txtApellidoMaterno").prop("disabled", false);
						$("#txtNombres").prop("disabled", false);
						$("#txtCurp").prop("disabled", false);
						$("#txtNss").prop("disabled", false);
						$("#txtRfc").prop("disabled", false);
						$("#slcGeneroTrabajador").prop("disabled", false);
						$("#slcNacionalidad").prop("disabled", false);
						$("#slcTel1").prop("disabled", false);
						$("#txtTel1").prop("disabled", false);
						$("#slcTel2").prop("disabled", false);
						$("#txtTel2").prop("disabled", false);
						$("#slcNivelEstudios").prop("disabled", false);
						$("#slcOcupacionProfesion").prop("disabled", false);
						$("#slcActividadNegocio").prop("disabled", false);
						$("#txtCodigoPostalLaboral").prop("disabled", false);
						$("#btnBuscarCodigoPostalLaboral").prop("disabled", false);
						$("#txtPaisLaboral").prop("disabled", false);
						$("#txtEntidadFederativaLaboral").prop("disabled", false);
						$("#txtDeleMuniLaboral").prop("disabled", false);
						$("#txtCiudadLaboral").prop("disabled", false);
						$("#txtColoniaLaboral").prop("disabled", false);
						$("#txtCalleLaboral").prop("disabled", false);
						$("#txtNumExteriorLaboral").prop("disabled", false);
						$("#txtNumInteriorLaboral").prop("disabled", false);
						$("#txtCorreoElectronico").prop("disabled", false);

						$("#slcEntidadNacimiento").prop("disabled", false);
						$("#slcCorreoElectronico").prop("disabled", false);

						$("#slcIdentificacionOficial").prop("disabled", false);
						$("#slcComprobanteDomicilio").prop("disabled", false);


						$("#chBoxTrabajadorfallecido").hide();
						$("#chkRegistroTrabajador").hide();
						$("#lblRegistroFallecido").hide();
						$("#txtNss").prop("disabled", true);


					}

				}
				else {

					$("#contenedorPrincipal").hide();
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}
			}
			catch (err) {

				mensajeAdvertenciaFinal("Ha ocurrido un error al consultar la respuesta del servicio, por favor reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function fechaMaximaComprobanteDomicilio() {
	var sPhp = "php/expedienteIdentificacion.php";

	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: { iOpcion: 21 },
		beforeSend: function () {
			bloquearUI("Consultando la fecha maxima");
		},

		error: function (xhr, ajaxOptions, thrownError) {

			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error en la carga de la fecha maxima del comprobante de domicilio, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {

			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					$("#dtFechaComprobante").datepicker("option", "minDate", respuesta.fechaMaxima);
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de nacionalidad, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

function actualizarEstatusExpediente(iRespuesta) {
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 22,
			cFolio: cFolio,
			iTipoServicio: iTipoServicio,
			iRespuesta: iRespuesta
		},
		beforeSend: function () {
			bloquearUI("Consultando Estatus de la digitalizacion");
		},

		error: function (xhr, ajaxOptions, thrownError) {

			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error en la consulta de actualizacion del estatus, por favor, reporte a Mesa de ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {

			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					cerrarNavegador();//se cierra el navegador para evitarl el msj exitoso folio 790
					//mensajeExitoFinal("El Expediente de Identificaci&oacute;n se gener&oacute; con &eacute;xito");
					$("#contenedorPrincipal").hide();
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}

			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de nacionalidad, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

//Convierte a mayusculas el valor de la caja de texto llama a al metodo
function aMayusculas(e) {
	var sCadena = $(e.currentTarget).val();

	sCadena = sCadena.toUpperCase();

	$(e.currentTarget).val(sCadena);
}


function bloquearUI(mensaje) {
	//armamos un codigo html para representar el dialogo de cargando
	var html = "<div id='bloquea'>";
	html += "<span class='Espera'>";
	html += "</span><p style='font-size:1.2em;float:left; margin: 7px 0px 0px 25px;' >" + mensaje + "</p>";
	html += "</div>";

	//inicializamos el bloqueo de pantalla con sus respectivas opciones.
	$.blockUI({ message: html, css: { 'border-radius': '17px', 'width': 'auto' } });
}//function bloquearUI

//desbloqueamos la pantalla
function desbloquearUI() {
	$.unblockUI();
}//function desbloquearUI

//funcion que permite cerrar la ventana
function cerrarNavegador() {
	if (OSName == "Android") {
		Android.volverMenu('1');
	} else {
		//firefox
		if (navigator.appName.indexOf('Netscape') >= 0)
			javascript: window.close();
		else if (navigator.appName.indexOf('Microsoft') >= 0) {//internet explorer
			var ventana = window.self;
			ventana.opener = window.self;
			ventana.close();
		}
	}
}

function validaFechaDDMMAAAA(fecha) {
	var dtCh = "-";
	var minYear = 1900;
	var maxYear = 2100;

	function isInteger(s) {
		var i;
		for (i = 0; i < s.length; i++) {
			var c = s.charAt(i);

			if (((c < "0") || (c > "9")))
				return false;
		}
		return true;
	}

	function stripCharsInBag(s, bag) {
		var i;
		var returnString = "";

		for (i = 0; i < s.length; i++) {
			var c = s.charAt(i);

			if (bag.indexOf(c) == -1) returnString += c;
		}
		return returnString;
	}

	function daysInFebruary(year) {
		return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
	}

	function DaysArray(n) {
		for (var i = 1; i <= n; i++) {
			this[i] = 31
			if (i == 4 || i == 6 || i == 9 || i == 11) { this[i] = 30 }

			if (i == 2) { this[i] = 29 }
		}
		return this
	}
	function isDate(dtStr) {
		var daysInMonth = DaysArray(12)
		var pos1 = dtStr.indexOf(dtCh)
		var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
		var strDay = dtStr.substring(0, pos1)
		var strMonth = dtStr.substring(pos1 + 1, pos2)
		var strYear = dtStr.substring(pos2 + 1)
		strYr = strYear

		if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
		if (strMonth.charAt(0) == "0" && strMonth.length > 1)
			strMonth = strMonth.substring(1)
		for (var i = 1; i <= 3; i++) {
			if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
		}
		month = parseInt(strMonth)
		day = parseInt(strDay)
		year = parseInt(strYr)

		if (pos1 == -1 || pos2 == -1) {
			return false
		}

		if (strMonth.length < 1 || month < 1 || month > 12) {
			return false
		}

		if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
			return false
		}

		if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
			return false
		}
		if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
			return false
		}

		return true

	}

	if (isDate(fecha)) {
		return true;
	}

	else {
		return false;
	}
}

function LimpiarCadenaSoloNumeros(e) {
	var sCadena = $(e.currentTarget).val();

	//elimino todos los caracteres que no sean numeros o punto
	sCadena = sCadena.replace(/[^0-9.]/g, "");

	$(e.currentTarget).val(sCadena);
}


function dateDiffInYears(dateold, datenew) {
	var ynew = datenew.getUTCFullYear();
	var mnew = datenew.getUTCMonth() + 1;
	var dnew = datenew.getUTCDate();

	var yold = dateold.getUTCFullYear();
	var mold = dateold.getUTCMonth() + 1;
	var dold = dateold.getUTCDate();

	var diff = ynew - yold;
	if (mold > mnew) {
		diff--;
	}
	else {
		if (mold == mnew) {
			if (dold > dnew) {
				diff--;
			}
		}
	}
	return diff;
}

function extraerDatosBancoppel(cFolio) {
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: true,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 28,
			cFolio: cFolio,
		},

		beforeSend: function () {
			bloquearUI("Validando Expediente");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al validar el estatus del expediente, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					//RFC
					$("#txtRfc").val(respuesta.trabajador[0].rfc);

					//Fecha de Nacimiento.
					if (respuesta.trabajador[0].fechanacimiento != "") {
						$("#txtFechaNacimiento").val(formatearFecha(3, respuesta.trabajador[0].fechanacimiento));
						sFechaNacimiento = formatearFecha(2, $("#txtFechaNacimiento").val());
						llenarIdentificacionOficial(sFechaNacimiento);
					}

					//Genero
					var genero = respuesta.trabajador[0].genero;
					if (genero == 'M') {
						$("#slcGeneroTrabajador").val(1);
					}
					else if (genero == 'F') {
						$("#slcGeneroTrabajador").val(2);
					}
					//Correo Electronico
					var chCorreo = respuesta.trabajador[0].email;
					for (var i = 0; i < chCorreo.length; i++) {
						var cadenaemail = chCorreo.charAt(i);
						if (cadenaemail == "@") {
							var ante = chCorreo.substring(0, i);
							$("#txtCorreoElectronico").val(ante);
							var tipoCorreo = chCorreo.substring(i + 1, chCorreo.length);
							habilitaConfirmacionCorreo();
						}
					}

					if (tipoCorreo == 'gmail.com') {
						$("#slcCorreoElectronico").val(4);
					}
					else if (tipoCorreo == 'hotmail.com') {
						$("#slcCorreoElectronico").val(1);
					}
					else if (tipoCorreo == 'yahoo.com.mx') {
						$("#slcCorreoElectronico").val(2);
					}
					else if (tipoCorreo == 'yahoo.com') {
						$("#slcCorreoElectronico").val(3);
					}
					else if (tipoCorreo == 'outlook.es') {
						$("#slcCorreoElectronico").val(5);
					}
					else if (tipoCorreo == 'outlook.com') {
						$("#slcCorreoElectronico").val(6);
					}
					else {
						$("#slcCorreoElectronico").val(7);
						$("#txtOtroDominio").val(tipoCorreo);
					}


					//Profesion
					/*var profesion = respuesta.trabajador[0].profesion;
					var prof = parseInt(profesion);
					$("#slcOcupacionProfesion").val(prof);*/

					//Escolaridad
					var escolaridad = respuesta.trabajador[0].escolaridad;
					var esc = parseInt(escolaridad);
					//Homologamos de acuerdo a los catalogos de bancoppel
					if (esc == 6) {
						$("#slcNivelEstudios").val(4);
					}
					else if (esc == 4) {
						$("#slcNivelEstudios").val(2);
					}
					else if (esc == 2 || esc == 3 || esc == 5) {
						$("#slcNivelEstudios").val(-1);
					}
					else if (esc == 1) {
						$("#slcNivelEstudios").val(1);
					}
					//Entidad Nacimiento
					var entidadNacimiento = respuesta.trabajador[0].entidadnacimiento;
					$("#slcEntidadNacimiento").val(entidadNacimiento);

					//Ocupacion o Profesion
					var Chocupacion = respuesta.trabajador[0].profesion;
					var ChOcu_Prof = parseInt(Chocupacion);
					//Homologacion de la Ocupacion de Acuerdo a los catalogos de Bancoppel.
					if (ChOcu_Prof == 10 || ChOcu_Prof == 40 || ChOcu_Prof == 41 || ChOcu_Prof == 46 || ChOcu_Prof == 54 || ChOcu_Prof == 517) {
						//1 Profesionistas
						$("#slcOcupacionProfesion").val(1);
					}
					else if (ChOcu_Prof == 11 || ChOcu_Prof == 16 || ChOcu_Prof == 21 || ChOcu_Prof == 23 || ChOcu_Prof == 26 || ChOcu_Prof == 327) {
						//2 Tecnicos
						$("#slcOcupacionProfesion").val(2);
					}
					else if (ChOcu_Prof == 316 || ChOcu_Prof == 317 || ChOcu_Prof == 57) {
						//3 Trabajadores de la Eduacacion
						$("#slcOcupacionProfesion").val(3);
					}
					else if (ChOcu_Prof == 25) {
						//4 TRABAJADORES DEL ARTE, ESPECTÝCULOS Y DEPORTES
						$("#slcOcupacionProfesion").val(4);
					}
					else if (ChOcu_Prof == 58) {
						//5 FUNCIONARIOS Y DIRECTIVOS DE LOS SECTORES PÚBLICO, PRIVADO Y SOCIAL
						$("#slcOcupacionProfesion").val(5);
					}
					else if (ChOcu_Prof == 12 || ChOcu_Prof == 17 || ChOcu_Prof == 24 || ChOcu_Prof == 52 || ChOcu_Prof == 511 || ChOcu_Prof == 516) {
						//6 TRABAJADORES EN ACTIVIDADES AGRÝCOLAS, GANADERAS, SILVÝCOLAS Y DE CAZA Y PESCA
						$("#slcOcupacionProfesion").val(6);
					}
					else if (ChOcu_Prof == 514) {
						//7 JEFES, SUPERVISORES Y OTROS TRABAJADORES DE CONTROL EN LA FABRICACIÓN ARTESANAL E INDUSTRIAL Y EN ACTIVIDADES DE REPARACIÓN Y MANTENIMIENTO
						$("#slcOcupacionProfesion").val(7);
					}
					else if (ChOcu_Prof == 13 || ChOcu_Prof == 36 || ChOcu_Prof == 313 || ChOcu_Prof == 55) {
						//10 AYUDANTES, PEONES Y SIMILARES EN EL PROCESO DE FABRICACIÓN ARTESANAL E INDUSTRIAL Y EN ACTIVIDADES DE REPARACIÓN Y MANTENIMIENTO
						$("#slcOcupacionProfesion").val(10);
					}
					else if (ChOcu_Prof == 27 || ChOcu_Prof == 522 || ChOcu_Prof == 345) {
						//11 CONDUCTORES Y AYUDANTES DE CONDUCTORES DE MAQUINARIA MÓVIL Y MEDIOS DE TRANSPORTE
						$("#slcOcupacionProfesion").val(11);
					}
					else if (ChOcu_Prof == 336) {
						//13 TRABAJADORES DE APOYO EN ACTIVIDADES ADMINISTRATIVAS
						$("#slcOcupacionProfesion").val(13);
					}
					else if (ChOcu_Prof == 22 || ChOcu_Prof == 30 || ChOcu_Prof == 31 || ChOcu_Prof == 33 || ChOcu_Prof == 34 || ChOcu_Prof == 35 || ChOcu_Prof == 37
						|| ChOcu_Prof == 39 || ChOcu_Prof == 310 || ChOcu_Prof == 311 || ChOcu_Prof == 312 || ChOcu_Prof == 315
						|| ChOcu_Prof == 320 || ChOcu_Prof == 321 || ChOcu_Prof == 323 || ChOcu_Prof == 324 || ChOcu_Prof == 325 || ChOcu_Prof == 326
						|| ChOcu_Prof == 328 || ChOcu_Prof == 330 || ChOcu_Prof == 331 || ChOcu_Prof == 333 || ChOcu_Prof == 334
						|| ChOcu_Prof == 335 || ChOcu_Prof == 340 || ChOcu_Prof == 341 || ChOcu_Prof == 342 || ChOcu_Prof == 343
						|| ChOcu_Prof == 344 || ChOcu_Prof == 347 || ChOcu_Prof == 348 || ChOcu_Prof == 349 || ChOcu_Prof == 353
						|| ChOcu_Prof == 50 || ChOcu_Prof == 51 || ChOcu_Prof == 53 || ChOcu_Prof == 510 || ChOcu_Prof == 512 || ChOcu_Prof == 521) {
						//14 COMERCIANTES, EMPLEADOS DE COMERCIO Y AGENTES DE VENTAS
						$("#slcOcupacionProfesion").val(14);
					}
					else if (ChOcu_Prof == 15) {
						//15 VENDEDORES AMBULANTES Y TRABAJADORES AMBULANTES EN SERVICIOS
						$("#slcOcupacionProfesion").val(15);
					}
					else if (ChOcu_Prof == 59 || ChOcu_Prof == 319) {
						//16 TRABAJADORES EN SERVICIOS PERSONALES
						$("#slcOcupacionProfesion").val(16);
					}
					else if (ChOcu_Prof == 42 || ChOcu_Prof == 43 || ChOcu_Prof == 44 || ChOcu_Prof == 45 || ChOcu_Prof == 47) {
						//18 TRABAJADORES EN SERVICIOS DE PROTECCIÓN Y VIGILANCIA Y FUERZAS ARMADAS
						$("#slcOcupacionProfesion").val(18);
					}
					else if (ChOcu_Prof == 14 || ChOcu_Prof == 18 || ChOcu_Prof == 20 || ChOcu_Prof == 28 || ChOcu_Prof == 32 || ChOcu_Prof == 38 || ChOcu_Prof == 314
						|| ChOcu_Prof == 318 || ChOcu_Prof == 322 || ChOcu_Prof == 329 || ChOcu_Prof == 337 || ChOcu_Prof == 338
						|| ChOcu_Prof == 339 || ChOcu_Prof == 346 || ChOcu_Prof == 350 || ChOcu_Prof == 351 || ChOcu_Prof == 352 || ChOcu_Prof == 56
						|| ChOcu_Prof == 513 || ChOcu_Prof == 515 || ChOcu_Prof == 518 || ChOcu_Prof == 519 || ChOcu_Prof == 520
						|| ChOcu_Prof == 523 || ChOcu_Prof == 699 || ChOcu_Prof == 799 || ChOcu_Prof == 899 || ChOcu_Prof == 999 || ChOcu_Prof == 109) {
						//19 OTROS TRABAJADORES CON OCUPACIONES NO ESPECIFICADAS
						$("#slcOcupacionProfesion").val(19);
					}


					//Actividad Giro o Negocio

					var ChGiro = respuesta.trabajador[0].profesion;
					var ChGiro_Act = parseInt(ChGiro);


					if (ChGiro_Act == 10 || ChGiro_Act == 14 || ChGiro_Act == 18 || ChGiro_Act == 20 || ChGiro_Act == 25 || ChGiro_Act == 26 || ChGiro_Act == 28
						|| ChGiro_Act == 30 || ChGiro_Act == 31 || ChGiro_Act == 35 || ChGiro_Act == 38 || ChGiro_Act == 39
						|| ChGiro_Act == 310 || ChGiro_Act == 311 || ChGiro_Act == 314 || ChGiro_Act == 316 || ChGiro_Act == 317 || ChGiro_Act == 319
						|| ChGiro_Act == 327 || ChGiro_Act == 329 || ChGiro_Act == 338 || ChGiro_Act == 343 || ChGiro_Act == 351
						|| ChGiro_Act == 40 || ChGiro_Act == 41 || ChGiro_Act == 42 || ChGiro_Act == 43 || ChGiro_Act == 44 || ChGiro_Act == 45
						|| ChGiro_Act == 46 || ChGiro_Act == 47 || ChGiro_Act == 56 || ChGiro_Act == 57 || ChGiro_Act == 58 || ChGiro_Act == 59
						|| ChGiro_Act == 514 || ChGiro_Act == 519 || ChGiro_Act == 520 || ChGiro_Act == 523 || ChGiro_Act == 799 || ChGiro_Act == 899) {
						//9 SERVICIOS COMUNALES, SOCIALES Y PERSONALES
						$("#slcActividadNegocio").val(9);
					}
					else if (ChGiro_Act == 37 || ChGiro_Act == 336 || ChGiro_Act == 348 || ChGiro_Act == 517) {
						//8 SERVICIOS FINANCIEROS, SEGUROS Y BIENES INMUEBLES
						$("#slcActividadNegocio").val(8);
					}
					else if (ChGiro_Act == 27 || ChGiro_Act == 33 || ChGiro_Act == 345 || ChGiro_Act == 54 || ChGiro_Act == 522) {
						//7 TRANSPORTE, ALMACENAMIENTO Y COMUNICACIONES
						$("#slcActividadNegocio").val(7);
					}
					else if (ChGiro_Act == 15 || ChGiro_Act == 22 || ChGiro_Act == 34 || ChGiro_Act == 312 || ChGiro_Act == 318 || ChGiro_Act == 320 || ChGiro_Act == 321
						|| ChGiro_Act == 322 || ChGiro_Act == 323 || ChGiro_Act == 324 || ChGiro_Act == 325 || ChGiro_Act == 326
						|| ChGiro_Act == 328 || ChGiro_Act == 330 || ChGiro_Act == 331 || ChGiro_Act == 333 || ChGiro_Act == 334 || ChGiro_Act == 335
						|| ChGiro_Act == 337 || ChGiro_Act == 339 || ChGiro_Act == 340 || ChGiro_Act == 342 || ChGiro_Act == 344
						|| ChGiro_Act == 346 || ChGiro_Act == 347 || ChGiro_Act == 349 || ChGiro_Act == 350 || ChGiro_Act == 352 || ChGiro_Act == 353
						|| ChGiro_Act == 50 || ChGiro_Act == 51 || ChGiro_Act == 53 || ChGiro_Act == 510 || ChGiro_Act == 512 || ChGiro_Act == 518
						|| ChGiro_Act == 521) {
						//6 COMERCIO, RESTAURANTES Y HOTELES
						$("#slcActividadNegocio").val(6);
					}
					else if (ChGiro_Act == 16 || ChGiro_Act == 341) {
						//5 ELECTRICIDAD, GAS Y AGUA POTABLE
						$("#slcActividadNegocio").val(5);
					}
					else if (ChGiro_Act == 11 || ChGiro_Act == 313 || ChGiro_Act == 55) {
						//4 CONSTRUCCION
						$("#slcActividadNegocio").val(4);
					}
					else if (ChGiro_Act == 13 || ChGiro_Act == 21 || ChGiro_Act == 23 || ChGiro_Act == 36 || ChGiro_Act == 513) {
						//3 INDUSTRIA MANUFACTURERA
						$("#slcActividadNegocio").val(3);
					}
					else if (ChGiro_Act == 515) {
						//2 MINERIA
						$("#slcActividadNegocio").val(2);
					}
					else if (ChGiro_Act == 12 || ChGiro_Act == 17 || ChGiro_Act == 24 || ChGiro_Act == 52 || ChGiro_Act == 511 || ChGiro_Act == 516) {
						//1 AGROPECUARIO, SILVICULTURA Y PESCA
						$("#slcActividadNegocio").val(1);
					}
					else if (ChGiro_Act == 699 || ChGiro_Act == 999 || ChGiro_Act == 109) {
						$("#slcActividadNegocio").val(-1);
					}

					//Telefono 2
					$("#txtTel2").val(respuesta.trabajador[0].telefonofijo);


				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del estatus del expediente, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}

//Funcion para consiguir los datos del trabajador que  se puedan obtener a través de RENAPO.
function extraerDatosRenapo(cFolio) {
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 29,
			cFolio: cFolio,
		},
		beforeSend: function () {
			bloquearUI("Validando Expediente");
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al validar el estatus del expediente, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if (respuesta.codigoRespuesta == 1) {
					//Fecha de Nacimiento.
					if (respuesta.trabajador[0].fechanacimiento != "") {
						$("#txtFechaNacimiento").val(formatearFecha(3, respuesta.trabajador[0].fechanacimiento));
						sFechaNacimiento = formatearFecha(2, $("#txtFechaNacimiento").val());
						llenarIdentificacionOficial(sFechaNacimiento);
					}

					//Entidad Nacimiento.
					if (respuesta.trabajador[0].entidadnacimiento != "") {
						var entidadNacimiento = respuesta.trabajador[0].entidadnacimiento;
						$("#slcEntidadNacimiento").val(entidadNacimiento);
					}

					//Genero.
					var genero = respuesta.trabajador[0].genero;
					if (genero == 'H') {
						$("#slcGeneroTrabajador").val(1);
					}
					else if (genero == 'M') {
						$("#slcGeneroTrabajador").val(2);
					}

					//Nacionalidad.
					if (respuesta.trabajador[0].nacionalidad != "") {
						var nacionalidad = respuesta.trabajador[0].nacionalidad;
						$("#slcNacionalidad").val(nacionalidad);
					}
				}
				else {
					mensajeAdvertenciaFinal(respuesta.descripcion);
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al cargar la respuesta del estatus del expediente, por favor, reporte a Mesa de ayuda.");
				console.log(err);
			}
		}
	});
}


function validarTelefonoListaNegra() {

	var bRetorna = false;
	var cTelefono1 = $("#txtTel1").val();
	var cTelefono2 = $("#txtTel2").val();

	var sMensajeTelefono1 = "";
	var sMensajeTelefono2 = "";

	iIdentificador = 0;
	iIdentificador2 = 0;
	iIdentificador3 = 0;
	//iIdentificador4 = 0 ;

	$.ajax({
		async: false,
		cache: false,
		data: {
			iOpcion: 34,
			Telefono1: cTelefono1,
			Telefono2: cTelefono2,
			cFolio: cFolio,
			empleado: iNumeroEmpleado,	//392.1
		},
		url: 'php/expedienteIdentificacion.php',
		type: 'POST',
		dataType: 'JSON',
		success: function (data) {

			sMensajeTelefono1 = data[0].mensaje;
			sMensajeTelefono2 = data[1].mensaje;

			iIdentificador = data[0].identificador;
			iIdentificador2 = data[1].identificador;

			if (iIdentificador == 0 || iIdentificador == 2) {
				if (iIdentificador2 == 0 || iIdentificador2 == 2 || cTelefono2 == '') {
					bRetorna = true;
				}
				else {
					sTitle = "AVISO";
					sMensaje = (sMensajeTelefono2);
					mensajeAdvertencia(sMensaje, sTitle);
				}
			}
			else {
				sTitle = "AVISO";
				sMensaje = (sMensajeTelefono1);
				mensajeAdvertencia(sMensaje, sTitle);
			}
		},
		error: function (a, b, c) {
			sTitle = " ";
			sMensaje = " Ocurrio un error al realizar las validaciones de los telefonos  ";
			mensajeAdvertencia(sMensaje, sTitle);
		}

	});
	return bRetorna;
}

function validarRfc(rfc) {
	var esValida = 0;
	if (rfc.length == 10) {
		if (validarEstructuraRFC10(rfc))
			esValida = 1;
		else
			esValida = 1;
	}
	else if (rfc.length == 13) {
		if (validarEstructuraRFC13(rfc))
			esValida = 1;
		else
			esValida = 0;
	}
	else {
		esValida = 0;
	}

	return esValida;
}

function validarEstructuraRFC10(rfc) {
	var esValida = 0;
	if (rfc.match(/^([A-Za-z]{4})([0-9]{6})$/i)) {//AAAA######A#
		esValida = 1;
	}
	return esValida;
}

function validarEstructuraRFC13(rfc) {
	var esValida = 0;
	if (rfc.match(/^([A-Za-z]{4})([0-9]{6})([A-Za-z0-9]{3})$/i)) {//AAAA######A#
		esValida = 1;
	}
	return esValida;
}
// function limpiarCodigoPostalDom()
// {
// document.getElementById("txtCodigoPostal").value = "";
// //document.getElementById("txtCodigoPostal").reset();
// //$('#txtCodigoPostal').val("");
// }

// MOVIL
/* Obtener parametros get desde el webview android */
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


// MOVIL
/* Funcion que recibe la respuesta del componente FirmaDigital */
function getComponenteDigitalizar(estatusProceso) {
	if (estatusProceso == 1 || estatusProceso == "1") {
		actualizarEstatusExpediente(1);
	}
	respuestaWebService(parseInt(estatusProceso));
}

//893.1 Consulta el tipo de solicitante y si �ste es un promotor activo
function validarTipoSolicitante(){
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 38,
			cFolio: cFolio,
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener el tipo de solicitante, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if(respuesta.estatus == 1){
					iTipoSolicitante = parseInt(respuesta.tiposolicitante);
					iPromotorActivoSol = parseInt(respuesta.promotoractivo);
					iClaveConsarSol = respuesta.claveconsar;

					if(iTipoSolicitante == 3 || iTipoSolicitante == 4){
						mostrarCheckPromotorActivo(iClaveConsarSol);
					}
					if(iTipoSolicitante == 2 || iTipoSolicitante == 3 || iTipoSolicitante == 4){
						mostrarDatosSolicitante();
					}
					if(iTipoSolicitante == 2){
						llenarCatalogoParentesco();
					}
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener el tipo de solicitante, favor de reportarlo a Mesa de Ayuda");
				console.log(err);
			}
		}
	});
}
function mostrarCheckPromotorActivo(){
	var bDisabled = false;
	var sMsgCheck = "";
	if(iPromotorActivoSol == 1){
		sMsgCheck = "Promotor Activo Interno";
		bDisabled = true;
	}else{
		sMsgCheck = "&iquestEl Solicitante es un Agente Promotor Activo en otra Administradora?";
	}
	
	$('#divCheckPromotor').append("<input type='checkbox' id='cboxPromotor'> <label class = 'Etiqueta' for='cbox2'>"+sMsgCheck+"</label>");
	
	$("#cboxPromotor").change(function() {
		if(this.checked) {
			if($("#divPromotor").children().length == 0){
				$("#divPromotor").append("<table width='100%'><tr align = 'center'><td colspan = '6' class = 'Seccion'><label>Datos de Promotor Activo</label>"+
				"</td></tr></table><table width='30%'><tr><td><label class='Etiqueta'>N&uacute;mero de Agente Promotor</label></td><td><input type='text' class='CajaTexto' ondrop='return false;' id='txtNumAgente' maxlength='10'/>"+
				"</td></tr> <tr><td></td><td><p id='pMsgConsar' style ='margin: 0 0; font-size: 11; color: red; display: none;'>Promotor favor de capturar el n&uacute;mero correcto</p></td></tr>"+
				"<tr><td><label class='Etiqueta'>Parentesco</label></td><td><select class='ComboBox' id='slcParentescoSol' style = 'width: 154px'> </select> </select>"+
				"</td></tr></table>");

				llenarCatalogoParentesco();

				$("#divPromotor").show();
				$("#txtNumAgente").keyup(function () {
					this.value = this.value.replace(/[^0-9]/g,'');
				});

				$('#txtNumAgente').on("copy paste",function(e) {
					e.preventDefault();
				});

			}else{
				$("#divPromotor").show();
			}
		}else{
			$("#divPromotor").hide();
		}
	});

	$('#cboxPromotor').prop('checked', bDisabled);
	$('#cboxPromotor').prop('disabled', bDisabled);
	$('#cboxPromotor').trigger("change");
	$('#txtNumAgente').prop('disabled', bDisabled);
	if(iPromotorActivoSol == 1){
		$("#txtNumAgente").val(iClaveConsarSol);
	}
}
function mostrarDatosSolicitante(){
	var sHtmlSolicitante = "";
	if(iTipoSolicitante == 2){//Beneficiario
		sHtmlSolicitante = "<table width='100%'><td colspan = '6' class = 'Seccion'><label>Datos de solicitante</label></td></tr>"+
		"<tr><td><label class='Etiqueta'>Apellido Paterno</label></td><td><input type='text' class='CajaTexto' id='txtApPaternoSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>Fecha Nacimiento</label></td><td><input type='text' class='CajaTexto' id='txtFechaNacSol' readonly/></td>"+
		"<td><label class='Etiqueta'>Nacionalidad</label></td><td><select class='ComboBox' id='slcNacSol' style = 'width: 154px'> </select></td></tr>"+
		"<tr><td><label class='Etiqueta'>Apellido Materno</label></td><td><input class='CajaTexto' id='txtApMaternoSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>Parentesco</label></td><td><select class='ComboBox' id='slcParentescoSol' style = 'width: 154px'> </select></td>"+
		"<td><label class='Etiqueta'>RFC</label></td><td><input class='CajaTexto' id='txtRFCSol' /></td></tr>"+
		"<tr><td><label class='Etiqueta'>Nombres</label></td><td><input type='text' class='CajaTexto' id='txtNombresSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>Entidad</label></td><td><select class='ComboBox' id='slcEntidadSol' style = 'width: 154px'> </select></td></tr>"+
		"<tr><td><label class='Etiqueta'>CURP</label></td><td><input type='text' class='CajaTexto' id='txtCurpSol' maxlength = '18'/></td>"+
		"<td><label class='Etiqueta'>G&eacute;nero</label></td><td><select class='ComboBox' id='slcGeneroSol' style = 'width: 154px'> </select></td></tr></table>";
	}else if(iTipoSolicitante == 3){//Apoderado Legal
		sHtmlSolicitante = "<table width='100%'><tr align = 'center'><td colspan = '6'  class = 'Seccion'><label>Datos de solicitante</label></td></tr>"+
		"<tr><td><label class='Etiqueta'>Apellido Paterno</label></td><td><input type='text' class='CajaTexto' id='txtApPaternoSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>Entidad</label></td><td><select class='ComboBox' id='slcEntidadSol' style = 'width: 154px'> </select></td>"+
		"<td><label class='Etiqueta'>N&uacute;mero de Notario</label></td><td><input type='text' class='CajaTexto' id='txtNumNotario' maxlength = '40'/></td></tr>"+
		"<tr><td><label class='Etiqueta'>Apellido Materno</label></td><td><input class='CajaTexto' id='txtApMaternoSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>Nacionalidad</label></td><td><select class='ComboBox' id='slcNacSol'style = 'width: 154px'> </select></td>"+
		"<td><label class='Etiqueta'>Nombre de Notario</label></td><td><input type='text' class='CajaTexto' id='txtNomNotarioSol' maxlength = '40'/></td></tr>"+
		"<tr><td><label class='Etiqueta'>Nombres</label></td><td><input type='text' class='CajaTexto' id='txtNombresSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>G&eacute;nero</label></td><td><select class='ComboBox' id='slcGeneroSol' style = 'width: 154px'> </select></td>"+
		"<td><label class='Etiqueta'>N&uacute;mero de Testimonio</label></td><td><input class='CajaTexto' id='txtNumTestimonio' maxlength = '40'/></td></tr>"+
		"<tr><td><label class='Etiqueta'>CURP</label></td><td><input type='text' class='CajaTexto' id='txtCurpSol' maxlength = '18'/></td>"+
		"<td><label class='Etiqueta'>RFC</label></td><td><input class='CajaTexto' id='txtRFCSol' /></td>"+
		"<td><label class='Etiqueta'>Fecha de Testimonio</label></td><td><input type='text' class='CajaTexto' id='txtFechaTestimonio' readonly/></td></tr>"+
		"<tr><td><label class='Etiqueta'>Fecha Nacimiento</label></td><td><input type='text' class='CajaTexto' id='txtFechaNacSol' readonly/></td>"+
		"<td><label class='Etiqueta'>N&uacute;mero de Hoja</label></td><td><input type='text' class='CajaTexto' id='txtNumHoja' maxlength = '40'/></td></tr></table>";

	}else if (iTipoSolicitante == 4){//Curador
		sHtmlSolicitante = "<table width='100%'><tr align = 'center'><td colspan = '6'  class = 'Seccion'><label>Datos de solicitante</label></td></tr>"+
		"<tr><td><label class='Etiqueta'>Apellido Paterno</label></td><td><input type='text' class='CajaTexto' id='txtApPaternoSol'/ maxlength = '40'></td>"+
		"<td><label class='Etiqueta'>Fecha Nacimiento</label></td><td><input type='text' class='CajaTexto' id='txtFechaNacSol' readonly/></td>"+
		"<td><label class='Etiqueta'>RFC</label></td><td><input class='CajaTexto' id='txtRFCSol' /></td></tr>"+
		"<tr><td><label class='Etiqueta'>Apellido Materno</label></td><td><input class='CajaTexto' id='txtApMaternoSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>Entidad</label></td><td><select class='ComboBox' id='slcEntidadSol' style = 'width: 154px'> </select></td>"+
		"<td><label class='Etiqueta'>Nombre del Juzgado</label></td><td><input type='text' class='CajaTexto' id='txtNomJuzgado' maxlength = '40'/></td></tr>"+
		"<tr><td><label class='Etiqueta'>Nombres</label></td><td><input type='text' class='CajaTexto' id='txtNombresSol' maxlength = '40'/></td>"+
		"<td><label class='Etiqueta'>G&eacute;nero</label></td><td><select class='ComboBox' id='slcGeneroSol' style = 'width: 154px'> </select></td>"+
		"<td><label class='Etiqueta'>N&uacute;mero de Juicio</label></td><td><input class='CajaTexto' id='txtNumJuicio' maxlength = '40'/></td></tr>"+
		"<tr><td><label class='Etiqueta'>CURP</label></td><td><input type='text' class='CajaTexto' id='txtCurpSol' maxlength = '18'/></td>"+
		"<td><label class='Etiqueta'>Nacionalidad</label></td><td><select class='ComboBox' id='slcNacSol' style = 'width: 154px'> </select></td>"+
		"</tr></table>";
	}
	$('#divSolicitante').append(sHtmlSolicitante);
	$("#slcGeneroSol").html($("#slcGeneroTrabajador > option").clone());
	$("#slcNacSol").html($("#slcNacionalidad > option").clone());
	$("#slcEntidadSol").html($("#slcEntidadNacimiento > option").clone());
	validacionesCampos();
	$("#txtFechaNacSol, #txtFechaTestimonio").datepicker({
		dateFormat: 'dd-mm-yy',
		monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		dateFormat: 'dd-mm-yy',
		dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
		maxDate: "0D",
		changeYear: true,
		yearRange: "1900:+0"
	});

	consultarDatosSolicitante();
	
}

function llenarCatalogoParentesco(){
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 39,
			cFolio: cFolio,
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener el tipo de solicitante, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if(respuesta.codigoRespuesta == 1){
					$("#slcParentescoSol").append("<option value=-1></option>");
					for (var i = 0; i < respuesta.parentescos.length; i++) {
						$("#slcParentescoSol").append("<option value=" + respuesta.parentescos[i].idcatalogo + ">"
							+ respuesta.parentescos[i].descripcion + "</option>");
						arrEntidadNacimiento[i] = respuesta.parentescos[i].descripcion;

					}
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener el tipo de solicitante, favor de reportarlo a Mesa de Ayuda");
				console.log(err);
			}
		}
	});
}

//893.1 Consulta la informacion del solicitante tercera figura
function consultarDatosSolicitante(){
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 40,
			cFolio: cFolio,
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener el tipo de solicitante, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if(respuesta.estatus == 1){
					$('#txtApPaternoSol').val(respuesta.datossolicitante.appaternosol.trim());
					$('#txtApMaternoSol').val(respuesta.datossolicitante.apmaternosol.trim());
					$('#txtNombresSol').val(respuesta.datossolicitante.nombresol.trim());
					$('#txtFechaNacSol').val(formatearFecha(3,respuesta.datossolicitante.fechanacsol));
					$('#slcEntidadSol').val(respuesta.datossolicitante.entidadsol.padStart(2, "0"));
					$('#slcGeneroSol').val(respuesta.datossolicitante.generosol);
					$('#txtRFCSol').val(respuesta.datossolicitante.rfc);
					$('#txtCurpSol').val(respuesta.datossolicitante.curpsol);

					if(respuesta.datossolicitante.nombresol.trim() != ''){
						$('#txtApPaternoSol').prop('disabled', true);
						$('#txtApMaternoSol').prop('disabled', true);
						$('#txtNombresSol').prop('disabled', true);
					}
					$('#txtFechaNacSol').prop('disabled', true);
					$('#slcEntidadSol').prop('disabled', true);
					$('#slcGeneroSol').prop('disabled', true);
					$('#txtCurpSol').prop('disabled', true);

					if(respuesta.datossolicitante.esextranjero == 0){
						$('#slcNacSol').prop('disabled', true)
					}
				}else{
					mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener los datos del solicitante, favor de reportarlo a Mesa de Ayuda");
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al obtener los datos del solicitante, favor de reportarlo a Mesa de Ayuda");
				console.log(err);
			}
		}
	});
}
function validacionesCampos(){
	//Texto
	$('#txtApPaternoSol,#txtApMaternoSol,#txtNombresSol, #txtNomNotarioSol, #txtNomJuzgado').keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!((e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122) || e.which == 32)) {
				e.preventDefault();
			}
		}
	});

	//Numericos
	$("#txtNumNotario, #txtNumTestimonio, #txtNumHoja, #txtNumJuicio").keyup(function () {
		this.value = this.value.replace(/[^0-9]/g,'');
	});
	
	//Texto y numericos
	$("#txtCurpSol, #txtRFCSol").keypress(function (e) {
		if (!(arrCaracteresPermitidos.indexOf(e.which) > -1)) {
			if (!(e.which >= 65 && e.which <= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57)) {
				e.preventDefault();
			}
		}
	});

	$('#txtApPaternoSol,#txtApMaternoSol,#txtNombresSol, #txtNomNotarioSol, #txtNomJuzgado, #txtCurpSol, #txtRFCSol').bind("change", aMayusculas);
}
function validarCamposSolicitante() {
	
	var cExtranjero = '';
	var bValidarNac = false;
	bRespuesta = true;
	arrCampos = ['txtApPaternoSol', 'txtNombresSol','txtCurpSol','txtFechaNacSol', 'txtRFCSol'];
	arrSelectores = ['slcEntidadSol','slcGeneroSol', 'slcNacSol'];

	if(iTipoSolicitante == 2 || iTipoSolicitante == 3 || iTipoSolicitante == 4){
		if(iTipoSolicitante == 2){ //Beneficiario
			arrSelectores.push('slcParentescoSol');
		}else if (iTipoSolicitante == 3){ //Apoderado Legal
			arrCampos.push('txtNumHoja','txtNumNotario', 'txtNomNotarioSol', 'txtNumTestimonio', 'txtFechaTestimonio');
		}else if (iTipoSolicitante == 4){ //Curador
			arrCampos.push('txtNomJuzgado', 'txtNumJuicio');
		}
	
		for(var i = 0; i < arrCampos.length; i++){
			if($('#'+arrCampos[i]).val() == ""){
				bRespuesta = false;
				break;
			}
		}
		for(var i = 0; i < arrSelectores.length && bRespuesta; i++){
			if($('#'+arrSelectores[i]).val() == "-1"){
				bRespuesta = false;
				break;
			}
		}
		if($('#txtCurpSol').val().length < 18){
			bRespuesta = false;
			
		if($('#txtRFCSol').val().length < 10)
			bRespuesta = false;
		}


		cExtranjero = $('#txtCurpSol').val()
		cExtranjero = cExtranjero.substring(11, 13)
		
		if (cExtranjero == 'NE') {
			bValidarNac = true;
		}

		if(!bRespuesta){
			mensajeAdvertencia("Favor de capturar todos los datos del solicitante");
		}
		else if (bValidarNac) {
			if ($("#slcEntidadSol").val() == 1) {
				mensajeAdvertencia("Promotor: La nacionalidad seleccionada no coincide con la del beneficiario");
				bRespuesta = false;
			}
		}
	}

	return bRespuesta;
}
function validarCamposPromotorActivo(){
	bRespuesta = true;
	if(iTipoSolicitante == 2 || iTipoSolicitante == 3 || iTipoSolicitante == 4){
		if($('#cboxPromotor').is(':checked')){
			if($('#txtNumAgente').val().length < 10){
				$('#pMsgConsar').show();
				bRespuesta = false;
			}else{
				$('#pMsgConsar').hide();
			}
			if($('#slcParentescoSol').val() == "-1"){
				bRespuesta = false;
			}
			if(!bRespuesta){
				mensajeAdvertencia("Favor de capturar los datos de Promotor Activo")
			}
		}
	}
	return bRespuesta;
}
function fnGuardarDatosSolicitante(){
	bRespuesta = false;
	//Datos solicitante -> Tercera figura
	var sApellidoPatSol = $("#txtApPaternoSol").val();
	var sApellidoMatSol = $("#txtApMaternoSol").val();
	var sNombresSol = $("#txtNombresSol").val();
	var sCurpSol = $("#txtCurpSol").val();
	var sFechaNacSol = $("#txtFechaNacSol").val() ? formatearFecha(2, $("#txtFechaNacSol").val()) : '';
	var iEntidadSol = $("#slcEntidadSol").val();
	var iNacionalidadSol = $("#slcNacSol").val();
	var iGeneroSol = $("#slcGeneroSol").val();
	var sRFCSol = $("#txtRFCSol").val();
	var iNumeroHoja = $("#txtNumHoja").val() ? $("#txtNumHoja").val() : 0;
	var iNumeroNotario = $("#txtNumNotario").val() ? $("#txtNumNotario").val() : 0;
	var sNombreNotario = $("#txtNomNotarioSol").val() ? $("#txtNomNotarioSol").val() : '';
	var iNumeroTestimonio = $("#txtNumTestimonio").val() ? $("#txtNumTestimonio").val() : 0;
	var sFechaTestimonio = $("#txtFechaTestimonio").val() ? formatearFecha(2, $("#txtFechaTestimonio").val()) : '';
	var sNombreJuzgado = $("#txtNomJuzgado").val() ? $("#txtNomJuzgado").val() : ''; 
	var iNumeroJuicio = $("#txtNumJuicio").val() ? $("#txtNumJuicio").val() : 0;
	var iPromotorActivo = $('#cboxPromotor').is(':checked') || iPromotorActivoSol == 1 ? 1 : 0;
	var iPromotorInterno = iPromotorActivoSol;
	var iClaveConsar = $("#txtNumAgente").val() ? $("#txtNumAgente").val() : 0;
	var iParentesco = $("#slcParentescoSol").val() != -1 && $("#slcParentescoSol").val() != undefined ? $("#slcParentescoSol").val() : 0;

	sTipoSolicitante = iTipoSolicitante == 2 ? '02':
						iTipoSolicitante == 3 ? '04':
						iTipoSolicitante == 4 ? '10': ''; 

	objSolicitante = {
		sTipoSolicitante : sTipoSolicitante,
		sApellidoPatSol: sApellidoPatSol,
		sApellidoMatSol: sApellidoMatSol,
		sNombresSol: sNombresSol,
		sCurpSol: sCurpSol,
		sFechaNacSol: sFechaNacSol,
		iEntidadSol: iEntidadSol,
		iNacionalidadSol: iNacionalidadSol,
		iGeneroSol: iGeneroSol,
		sRFCSol: sRFCSol,
		iNumeroHoja: iNumeroHoja,
		iNumeroNotario: iNumeroNotario,
		sNombreNotario: sNombreNotario,
		iNumeroTestimonio: iNumeroTestimonio,
		sFechaTestimonio: sFechaTestimonio,
		sNombreJuzgado: sNombreJuzgado,
		iNumeroJuicio: iNumeroJuicio,
		iPromotorActivo: iPromotorActivo,
		iPromotorInterno: iPromotorInterno,
		iClaveConsar: iClaveConsar,
		iParentesco: iParentesco
	};

	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 41,
			cFolio: cFolio,
			objSolicitante : objSolicitante
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al guardar los datos del solicitante, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if(respuesta.estatus == 1){
					bRespuesta = true;
				}else{
					mensajeAdvertenciaFinal("Ocurri&oacute; un error al guardar los datos del solicitante, favor de reportarlo a Mesa de Ayuda");
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al guardar los datos del solicitante, favor de reportarlo a Mesa de Ayuda");
				console.log(err);
			}
		}
	});
	return bRespuesta;
}

//Función que verifica si el servicio 0407 fue ejecutado
function verificarServicioEi(){
	var bRespuesta = false;
	var sCurp = $("#txtCurp").val();
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 42,
			cCurp: sCurp,
			iNumeroEmpleado : iNumeroEmpleado,
			tipoServicio : '0407'
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar estatus del servicio de verificaci&oacuten del trabajador, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if(respuesta.estatus == 1){
					if(respuesta.estatusverificacion == 1){
						bRespuesta = true;
					}
				}else{
					mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar estatus del servicio de verificaci&oacuten del trabajador, favor de reportarlo a Mesa de Ayuda");
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar estatus del servicio de verificaci&oacuten del trabajador, favor de reportarlo a Mesa de Ayuda");
				console.log(err);
			}
		}
	});
	return bRespuesta;
}

function obtenerExpedienteProcesar(){
	var bRespuesta = false;
	var sCurp = $("#txtCurp").val();
	
	
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 43,
			cCurp: sCurp
		},
		error: function (xhr, ajaxOptions, thrownError) {
			desbloquearUI();
			mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar el expediente electr&oacute;nico del trabajador, favor de reportarlo a Mesa de Ayuda.");
			console.log(thrownError);
		},
		success: function (respuesta) {
			desbloquearUI();
			try {
				if(respuesta.estatus == 1){
					//---------------------------DOMICILIO PERSONAL-----------------------------
					$("#txtPais").val(respuesta.datostrabajador.pais);
					
					if($("#txtPais").val() == '')
					{
						
					}
					else
					{
						bProcesar = 1;
					
					$("#txtEntidadFederativa").val(respuesta.datostrabajador.entidadfederativa);
					$("#txtCiudad").val(respuesta.datostrabajador.ciudadpoblacion);
					$("#txtDeleMuni").val(respuesta.datostrabajador.delegacionmunicipio);
					$("#txtColonia").val(respuesta.datostrabajador.colonia);
					$("#txtCalle").val(respuesta.datostrabajador.calle);
					$("#txtNumExterior").val(respuesta.datostrabajador.numeroexterior);
					$("#txtNumInterior").val(respuesta.datostrabajador.numerointerior);
					
					if (respuesta.datostrabajador.codigopostal != '00000') {
						$("#txtCodigoPostal").val(respuesta.datostrabajador.codigopostal);
					}
					
					$("#txtEntidadFederativaOculto").val(respuesta.datostrabajador.identidadfederativa);
					$("#txtCiudadOculto").val(respuesta.datostrabajador.idciudadpoblacion);
					$("#txtDeleMuniOculto").val(respuesta.datostrabajador.iddelegacionmunicipio);
					$("#txtColoniaOculto").val(respuesta.datostrabajador.idcolonia);
					$("#txtCpOculto").val(respuesta.datostrabajador.codigopostal);
					//---------------------------TERMINA PERSONAL------------------------------
					
					// //---------------------------DOMICILIO LABORAL-----------------------------
					$("#txtPaisLaboral").val(respuesta.datostrabajador.paislab);
					$("#txtEntidadFederativaLaboral").val(respuesta.datostrabajador.entidadfederativalab);
					$("#txtCiudadLaboral").val(respuesta.datostrabajador.ciudadpoblacionlab);
					$("#txtDeleMuniLaboral").val(respuesta.datostrabajador.delegacionmunicipiolab);
					$("#txtColoniaLaboral").val(respuesta.datostrabajador.colonialab);
					$("#txtCalleLaboral").val(respuesta.datostrabajador.callelab);
					$("#txtNumExteriorLaboral").val(respuesta.datostrabajador.numeroexteriorlab);
					$("#txtNumInteriorLaboral").val(respuesta.datostrabajador.numerointeriorlab);
					
					if (respuesta.datostrabajador.codigopostallab != '00000') {
						$("#txtCodigoPostalLaboral").val(respuesta.datostrabajador.codigopostallab);
					}
					
					$("#txtEntidadFederativaLaboralOculto").val(respuesta.datostrabajador.identidadfederativalab);
					$("#txtCiudadLaboralOculto").val(respuesta.datostrabajador.idciudadpoblacionlab);
					$("#txtDeleMuniLaboralOculto").val(respuesta.datostrabajador.iddelegacionmunicipiolab);
					$("#txtColoniaLaboralOculto").val(respuesta.datostrabajador.idcolonialab);
					$("#txtCpLaboralOculto").val(respuesta.datostrabajador.codigopostallab);
					// //---------------------------TERMINA LABORAL-----------------------------
					
					
					//---------------------------OCUPACION-----------------------------
					$("#slcOcupacionProfesion").val(parseInt(respuesta.datostrabajador.ocupacion));
					$("#slcActividadNegocio").val(parseInt(respuesta.datostrabajador.actividadeconomica));
					$("#slcNivelEstudios").val(parseInt(respuesta.datostrabajador.estudios));
					
					if(respuesta.datostrabajador.correoelectronico !== ""){
						var arrCorreo = (respuesta.datostrabajador.correoelectronico).split("@");
						var correoTrab = arrCorreo[0];
						var tipoCorreo = arrCorreo[1];
	
						$("#txtCorreoElectronico").val(correoTrab);
					
						
						switch(tipoCorreo.trim().toLocaleLowerCase()){
							case 'hotmail.com':
								$("#slcCorreoElectronico").val(1);
								break;
							case 'yahoo.com.mx':
								$("#slcCorreoElectronico").val(2);
								break;
							case 'yahoo.com':
								$("#slcCorreoElectronico").val(3);
								break;
							case 'gmail.com':
								$("#slcCorreoElectronico").val(4);
								break;
							case 'outlook.es':
								$("#slcCorreoElectronico").val(5);
								break;
							case 'outlook.com':
								$("#slcCorreoElectronico").val(6);
								break;
							default:
								$("#slcCorreoElectronico").val(7);
								$("#slcCorreoElectronico").trigger("change");
								$("#txtOtroDominio").val(tipoCorreo.trim());
								break;
						}
						habilitaConfirmacionCorreo();
					}
					}
					
				}else{
					/*mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar el expediente electr&oacute;nico del trabajador, favor de reportarlo a Mesa de Ayuda");*/
				}
			}
			catch (err) {
				mensajeAdvertenciaFinal("Ocurri&oacute; un error al consultar el expediente electr&oacute;nico del trabajador, favor de reportarlo a Mesa de Ayuda");
				console.log(err);
			}
		}
	});
	return bRespuesta;
}

function consultaestatusbancoppel(){
	
	var sPhp = "php/expedienteIdentificacion.php";
	$.ajax({
		async: false,
		cache: true,
		url: sPhp,
		type: 'POST',
		dataType: 'JSON',
		data: {
			iOpcion: 44,
			cFolio: cFolio
		},
		error: function (xhr, ajaxOptions, thrownError) {
			
		},
		success: function (respuesta) {
			desbloquearUI();
			
			bBancoppel = respuesta.fnllenadobitacoraafiliacionbancoppel;
			
		}
	});
}

// Folio 1514 - Funcion para el estatus del switch de estado de cta.
function SwitchEdo(){
    $('input[name="checkbox1"]').each(function() {
        //Si está marcada ejecuta la condición verdadera.
        if(this.checked==true){
            iBandedocta=1;
            console.log("El estatus del switch 1 es:");
			$("#ley1").css("color", "#000000");
        }else{
            iBandedocta=0;
            //Si se ha desmarcado se ejecuta el siguiente mensaje.
            console.log("El estatus del switch 1 es:");
			$("#ley1").css("color", "#ababab");
        }
        console.log(iBandedocta);
      });
}

// Folio 1514 - Funcion para el estatus del switch de notificaciones
function switchNoti(){
    $('input[name="checkbox2"]').each(function() {
        //Si está marcada ejecuta la condición verdadera.
        if(this.checked==true){
            iBandnotificaciones=1;
            console.log("El estatus del switch 2 es:");
			//poner en negro
			$("#ley2").css("color", "#000000");
        }else{
            iBandnotificaciones=0;
            //Si se ha desmarcado se ejecuta el siguiente mensaje.
            console.log("El estatus del switch 2 es:");
			$("#ley2").css("color", "#ababab");
        }
        console.log(iBandnotificaciones);
      });
}

// Folio 1514 - Guarda los estatus de los switch y los datos necesarios a la tbcontrolenviocorreo
function guardaDatosSwitch(cCorreo){
	var sCurp = $("#txtCurp").val();
	var sNss = $("#txtNss").val();
	if(cCorreo != "" && bandCorreoValido == true && bandCorreoConfirmado==true){
		$.ajax({
			async: false,
			cache: true,
			url: "php/expedienteIdentificacion.php",
			type: 'POST',
			dataType: "JSON",
						data:{	iOpcion:45,
								cCurp:sCurp,
								iNss:sNss,
								iTiposolicitud:iTipoServicio,
								cCorreo:cCorreo,
								iBandnotificaciones:iBandnotificaciones,
								iBandedocta:iBandedocta},
			success: function(respuesta){
				try {
					if (respuesta.respuesta == 1) {
						console.log("Se REGISTRO");
					}else if(respuesta.respuesta == 2){
						console.log("DUPLICADO");

					}else{
						console.log("No se registro");

					}
				}
				catch (err) {
					mensajeAdvertencia("Ocurri&oacute; un error al intentar guardar los datos del switch.");
					console.log(err);
				}
			}  
		
		});	
	}
}

// FOLIO 1514 - Valida la confrmacion del correo electronico
function validarConfirmacionCorreo(){
	var dominioConfirm = "";
	var dominio = "";
	
	
	if($("#slcCorreoElectronico").val() != 7)
	{
		$("#opcion7").attr("disabled", "disabled");
		$("#opcion8").attr("disabled", "disabled");
	}
	else
	{
		$("#opcion7").click();
	}
	
	if($("#slcCorreoElectronicoConfirma").val() == 7){
		dominioConfirm = $("#txtOtroDominioConfirm").val();
	}else{
		dominioConfirm = $("#slcCorreoElectronicoConfirma").val();
	}

	if($("#slcCorreoElectronico").val() == 7){
		dominio = $("#txtOtroDominio").val();
	}else{
		dominio = $("#slcCorreoElectronico").val();
	}

    if($("#txtCorreoElectronico").val() == $("#txtCorreoElectronicoConfirma").val() && dominio == dominioConfirm){
		 //Si son iguales
		 if($('#Edo').is(':checked')) 
		 {
			
		 }
		 else
		 {
			$("#Edo").click();
		 }
		 
		 if( $('#Not').is(':checked')) 
		 {
			
		 }
		 else
		 {
			$("#Not").click();
		 }
		 $('#tdSwitch1').css("visibility","visible");
		 $('#tdSwitch2').css("visibility","visible");
		 $("#tercermensaje").hide();
		 bandCorreoConfirmado = true;
    }else if($("#txtCorreoElectronico").val() != $("#txtCorreoElectronicoConfirma").val() || dominio != dominioConfirm){
		 //Si no son iguales
		 $("#tercermensaje").hide();
		 $('#tdSwitch1').css("visibility","hidden");
		 $('#tdSwitch2').css("visibility","hidden");
		 if($("#txtCorreoElectronicoConfirma").val() != "" && dominioConfirm != -1){
			 if($("#slcCorreoElectronicoConfirma").val() != 7){
				//mensajeSnackbar("Correo electrónico no coincide, favor de revisar los campos");
				$("#tercermensaje").show();
			 }else if($("#slcCorreoElectronicoConfirma").val() == 7 && $("#txtOtroDominioConfirm").val() != "" && $("#txtOtroDominioConfirm").val() != undefined){
				//mensajeSnackbar("Correo electrónico no coincide, favor de revisar los campos");
				$("#tercermensaje").show();
			 }
		 }
		 bandCorreoConfirmado = false;
    }
	else
	{
		$("#tercermensaje").hide();
	}
}

function habilitaConfirmacionCorreo(){
	var dominio = "";
	var dominioConfirm = "";
	if($("#slcCorreoElectronico").val() == 7){
		dominio = $("#txtOtroDominio").val();
	}else{
		dominio = $("#slcCorreoElectronico").val();
	}

	if($("#slcCorreoElectronicoConfirma").val() == 7){
		dominioConfirm = $("#txtOtroDominioConfirm").val();
	}else{
		dominioConfirm = $("#slcCorreoElectronicoConfirma").val();
	}



	if($("#txtCorreoElectronico").val() != ""){
		var correo = "";
		if($("#slcCorreoElectronico").val() == 7){
			correo = $("#txtCorreoElectronico").val() + "@" + dominio;
		}else{
			correo = $("#txtCorreoElectronico").val() + "@" + $("#slcCorreoElectronico option:selected").text();
		}

		if($("#slcCorreoElectronico").val() == -1){
			correo = $("#txtCorreoElectronico").val() + "@" + "pruebamail.com";
		}
	
		$("#primermensaje").hide();
		$("#segundomensaje").hide();
		$("#tercermensaje").hide();
		validaCorreo(correo);

		if(bandCorreoValido == true){
			$("#confirm1").css("visibility","visible");
			$("#confirm2").css("visibility","visible");
			if($("#txtCorreoElectronicoConfirma").val() != ""){
				validarConfirmacionCorreo();
			}
		}else{
			$("#confirm1").css("visibility","hidden");
			$("#confirm2").css("visibility","hidden");
			$('#tdSwitch1').css("visibility","hidden");
			$('#tdSwitch2').css("visibility","hidden");
			if(bandCorreoValido == true){
				//mensajeSnackbar("Correo electrónico no coincide, favor de revisar los campos");
				$("#tercermensaje").show();
			}
			limpiaCamposConfirmacion();
			bandCorreoConfirmado = false;
		}
	}
	else
	{
		$("#primermensaje").hide();
		$("#segundomensaje").hide();
		$("#tercermensaje").hide();
	}

	if($("#slcCorreoElectronico").val() == -1 || $("#txtCorreoElectronico").val() == ""){
		$('#tdSwitch1').css("visibility","hidden");
		$('#tdSwitch2').css("visibility","hidden");
		bandCorreoConfirmado = false;
		$("#confirm1").css("visibility","hidden");
		$("#confirm2").css("visibility","hidden");
		limpiaCamposConfirmacion();
		bandCorreoValido = false;
	}

	if($("#txtCorreoElectronicoConfirma").val() != ""){
		if($("#txtCorreoElectronicoConfirma").val() != $("#txtCorreoElectronico").val() || dominio != dominioConfirm){
			$('#tdSwitch1').css("visibility","hidden");
			 $('#tdSwitch2').css("visibility","hidden");
			 if(bandCorreoValido == true){
				//mensajeSnackbar("Correo electrónico no coincide, favor de revisar los campos");
				$("#tercermensaje").show();
			 }

			 bandCorreoConfirmado = false;
		}
	}
	else
	{
		//$("#primermensaje").hide();
		//$("#segundomensaje").hide();
		//$("#tercermensaje").hide();
	}
}

function limpiaCamposConfirmacion(){
	$("#txtCorreoElectronicoConfirma").val("");
	$("#slcCorreoElectronicoConfirma").val("-1");
}

//FOLIO 1514 - Validaciones de correo
function validaCorreo(correo){
	//alert("entro");
    var validChars = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
	var latinValidaChars = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
	var notStart = ".'-_@";

    if(validChars.test(correo) && latinValidaChars.test(correo) && notStart.indexOf(correo[0]) == -1 && validaCombinaciones(correo)){
        var splitEmail = correo.split('@');
        var nombreCorreo = splitEmail[0];
        var dominio = splitEmail[1];
        var dominiosNoValidos = "itelcelfacebbokmovistarecorreoe-correoecorreose-correosmailmicorreomimail"
        var dominioSinPunto = dominio.split('.')[0];
        dominio = dominio.toLowerCase();
        var mensaje = ""

        String.prototype.count=function(c) { 
            var result = 0, i = 0;
            for(i;i<this.length;i++)if(this[i]==c)result++;
            return result;
          };

        if(dominiosNoValidos.indexOf(dominioSinPunto) == -1){
            switch(dominio){
                case "hotmail.com":
                        if(nombreCorreo.length < 65 && nombreCorreo.count('.') < 2 && isNaN(nombreCorreo[0])){
                            mensaje = "Correo Valido";
                            bandCorreoValido = true;
                        }else{
							mensaje = "Correo no cumple con validaciones de Dominio seleccionado.";
							bandCorreoValido = false;
                        }
                    break;
                case "gmail.com":
                        if(nombreCorreo.length < 31 && nombreCorreo.count('.') < 2){
                            mensaje = "Correo valido";
                            bandCorreoValido = true;
                        }else{
							mensaje = "Correo no cumple con validaciones de Dominio seleccionado.";
							bandCorreoValido = false;
                        }
                    break;
                case "yahoo.com":
                        if(nombreCorreo.length < 33 && nombreCorreo.count('.') < 2){
                            mensaje = "Correo valido";
                            bandCorreoValido = true;
                        }else{
							mensaje = "Correo no cumple con validaciones de Dominio seleccionado.";
							bandCorreoValido = false;
                        }
                    break;
				 case "yahoo.com.mx": 
                        if(nombreCorreo.length < 33 && nombreCorreo.count('.') < 2){
                            mensaje = "Correo valido";
                            bandCorreoValido = true;
                        }else{
							mensaje = "Correo no cumple con validaciones de Dominio seleccionado.";
							bandCorreoValido = false;
                        }
                 break;
                case "outlook.com":
                case "outlook.es":
                        if(nombreCorreo.length < 65 && nombreCorreo.count('.') < 2){
                            mensaje = "Correo valido";
                            bandCorreoValido = true;
                        }else{
							mensaje = "Correo no cumple con validaciones de Dominio seleccionado.";
							bandCorreoValido = false;
                        }
                    break;
                default:
                        if(nombreCorreo.length < 65 && correo.indexOf("__") == -1 && correo.indexOf("--") == -1){
                            mensaje = "Correo valido";
                            bandCorreoValido = true;
                        }else{
							mensaje = "Correo no cumple con validaciones de Dominio seleccionado.";
							bandCorreoValido = false;
                        }
                    break;
            }
        }else{
			//mensaje = "Correo ingresado no cumple con validaciones.";
			bandCorreoValido = false;
        }
        
        if(bandCorreoValido == false){
			$("#segundomensaje").show();
			//alert("primer");
			//mensajeSnackbar(mensaje);
		}
    }else{
		if($("#slcCorreoElectronico").val() == 7 && $("#txtOtroDominio").val() != undefined && $("#txtOtroDominio").val() != ""){
			//mensajeSnackbar("Correo ingresado no cumple con validaciones.");
			$("#segundomensaje").show();
			//alert("segundo");
		}
		if($("#slcCorreoElectronico").val() != 7){
			//mensajeSnackbar("Correo ingresado no cumple con validaciones.");
			$("#primermensaje").show();
			//alert("tercer");
		}
		bandCorreoValido = false;
    } 
}

function validaCombinaciones(correo){
	var arrCombinaciones = ["__","--","_-","-_","._",".-","-.","_."];
	var valido = true;
	for(var i = 0; i<arrCombinaciones.length;i++){
		if(correo.indexOf(arrCombinaciones[i]) != -1){
			valido = false;
			break;
		}
	}

	return valido;
}

function mensajeAdvertencia2(sMensaje) {

	$("#divMensaje").html("");
	$("#divMensaje").append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>" + sMensaje + "</p>");
	$("#divMensaje").dialog({
		resizable: false,
		height: 250,
		width: 500,
		modal: true,

		open: function (event, ui) { $(".ui-dialog-titlebar-close", ui.dialog).hide(); $(".ui-widget-overlay", ui.front).css('height', '1200px'); },
		title: "ADVERTENCIA",
		buttons: {
			"Aceptar": function () {
				$(this).dialog("close");
			},
		}
	});
}

function mensajeSnackbar(mensaje) {
	// Get the snackbar DIV
	var x = document.getElementById("snackbar");
	x.innerHTML = mensaje;
	// Add the "show" class to DIV
	x.className = "show";
  
	// After 3 seconds, remove the show class from DIV
	setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
  }