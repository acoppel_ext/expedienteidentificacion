var arrBeneficiarios = {};
var callback = function(){};

 //arreglo con el codigo ascii de las teclas espacio, backspace, tab, etc.
var arrCaracteresPermitidos = [0,46, 127, 8, 9, 37, 38, 32, 40, 180, 209, 241, 193, 201, 
                               205, 211, 218, 225, 233, 237, 243, 250];
function limpiarControles(){
  $("#txtPaternoBeneficiario").val("");
  $("#txtMaternoBeneficiario").val("");
  $("#txtNombreBeneficiario").val("");
  $("#txtCurpBeneficiario").val("");
  $("#slctParentescoBeneficiario").val(-1);
  $("#txtPorcentajeBeneficiario").val("");  
}

function iniBeneficiarios(funcion){	    
    callback = funcion;
	  var grid = null;
    var parameters = null;
    $("#txtPaternoBeneficiario,#txtMaternoBeneficiario,#txtNombreBeneficiario,#txtCurpBeneficiario").bind("change",aMayusculas);
    $("#txtPaternoBeneficiario,#txtMaternoBeneficiario,#txtNombreBeneficiario").keypress(function (e) {          
    if(!(arrCaracteresPermitidos.indexOf(e.which) > -1) )
      {
      if(!((e.which >= 65 && e.which<= 90 || e.which >= 97 && e.which <= 122) || e.which == 32)) 
      { 
        e.preventDefault();
      }
    }
    });
    $("#txtCurpBeneficiario").keypress(function (e) {      
        if(!(e.which >= 65 && e.which<= 90 || e.which >= 97 && e.which <= 122 || e.which >= 48 && e.which <= 57 
           || e.which == 0 || e.which == 8)) 
        {
          e.preventDefault();
        }
    });
    $('#divBeneficiarios').hide(0);
    $("#divBeneficiarios").dialog
    ({
        open: function(event, ui) { 
                                   $(".ui-dialog-titlebar-close", ui.dialog).hide(); 
                                   $(".ui-widget-overlay", ui.front).css('height', '1200px');
                                 },
        autoOpen: false,
        resizable: false,
        width: '80%',
        height: 'auto',
        modal: true,
        closeOnEscape: false, //Deshabilitar el boton escape para eviar que se cierre el dialogo        
        position:  {my: "center",at: "center",of: $("body"),within: $("body")}//Posicionar el dialog en el centro
    });
    
	  $("#btnAgregar").button();	
    $("#btnModificar").button();
    $("#btnEliminar").button();   
    $("#btnGuardarModificar").button(); 
    $("#btnGuardarModificar").hide();
    $("#btnRegresar").button();
    $("#btnCancelarBeneficiarios").button();
    llenarSelectParentesco();
    cargarGrid();
    grid = $("#listaBeneficiarios");
    $("#btnAgregar").click(function(ev){
      if (validarGuardarBeneficiarios()) 
      {
          parameters = { rowID: (grid.jqGrid('getGridParam', 'records')+1), 
                 initdata: {paterno: $("#txtPaternoBeneficiario").val(),
                            materno: $("#txtMaternoBeneficiario").val(),
                            nombres: $("#txtNombreBeneficiario").val(),
                            curp: $("#txtCurpBeneficiario").val(),
                            parentesco: $('#slctParentescoBeneficiario option:selected').text(),
                            idparentesco: $('#slctParentescoBeneficiario').val(),
                            porcentaje: parseInt($("#txtPorcentajeBeneficiario").val())
                           },
                 position: "last",
                 useDefValues: true,
                 useFormatter: true,
                 addRowParams: { extraparam: {} }
               };       
        if (validarPorcentajeBeneficiario()){
            limpiarControles();
            if (grid.jqGrid('getGridParam', 'records') == 5) 
            {
              mensajeAdvertencia("El m&aacute;ximo n&uacute;mero de beneficiarios es cinco.");
            }
            else
            {
              grid.jqGrid('addRow', parameters);
            }
        } 
        else{            
            mensajeAdvertencia("El porcentaje repartido entre los beneficiarios no debe exceder el 100%.");
        }   
      }

    
      
    });
    $("#btnModificar").click(function(ev){
        var selRowId = grid.jqGrid ('getGridParam', 'selrow');
      	if (selRowId > 0 && grid.jqGrid('getGridParam', 'records') > 0) 
      	{
            $( "#btnAgregar" ).prop( "disabled", true );
            $( "#btnEliminar" ).prop( "disabled", true );
        		$("#txtPaternoBeneficiario").val(grid.jqGrid ('getCell', selRowId, 'paterno'));
            $("#txtMaternoBeneficiario").val(grid.jqGrid ('getCell', selRowId, 'materno'));
            $("#txtNombreBeneficiario").val(grid.jqGrid ('getCell', selRowId, 'nombres'));
            $("#txtCurpBeneficiario").val(grid.jqGrid ('getCell', selRowId, 'curp'));
            $("#slctParentescoBeneficiario").val(grid.jqGrid ('getCell', selRowId, 'idparentesco'));
        		$("#txtPorcentajeBeneficiario").val(grid.jqGrid ('getCell', selRowId, 'porcentaje'));
            $("#btnModificar").hide();
            $("#btnGuardarModificar").show();
      	}
      	else
      	{
      		  mensajeAdvertencia("Seleccione el beneficiario que desea modificar.");
      	}

    });
    $("#btnGuardarModificar").click(function(ev) {      
        var selRowId = grid.jqGrid ('getGridParam', 'selrow');      
        if (validarGuardarBeneficiarios()){
          if (validarPorcentajeBeneficiario(selRowId)) 
          {
              $( "#btnAgregar" ).prop( "disabled", false );
              $( "#btnEliminar" ).prop( "disabled", false );
              $("#btnGuardarModificar").hide();
              $("#btnModificar").show();
              grid.jqGrid('setRowData',selRowId,{paterno: $("#txtPaternoBeneficiario").val(),
                                                 materno: $("#txtMaternoBeneficiario").val(),
                                                 nombres: $("#txtNombreBeneficiario").val(),
                                                 curp: $("#txtCurpBeneficiario").val(),                                           
                                                 parentesco: $('#slctParentescoBeneficiario option:selected').text(),
                                                 idparentesco: $('#slctParentescoBeneficiario').val(),
                                                 porcentaje: parseInt($("#txtPorcentajeBeneficiario").val())
                                               }
                         );
              $("#txtPaternoBeneficiario").val("");
              $("#txtMaternoBeneficiario").val("");
              $("#txtNombreBeneficiario").val("");
              $("#txtCurpBeneficiario").val("");
              $("#slctParentescoBeneficiario").val(-1);
              $("#txtPorcentajeBeneficiario").val("");            
          }
          else
          {
              mensajeAdvertencia("El porcentaje repartido entre los beneficiarios no debe exceder el 100%.");
          }
       }       
    });
    $("#btnEliminar").click(function(ev){
    	var selRowId = grid.jqGrid ('getGridParam', 'selrow');
    	if (selRowId > 0) 
    	{
    		grid.jqGrid('delRowData',selRowId);
    	}
    	else
    	{
    		mensajeAdvertencia("Seleccione el beneficiario que desea eliminar.");
    	}
    });
    $("#txtPorcentajeBeneficiario").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message        
               return false;
    }
   });
   $("#btnRegresar").click(function(ev) {
     regresarBeneficiarios();
   });
   $('#divBeneficiarios').dialog("open");
   $("#btnCancelarBeneficiarios").click(function(ev){
      $('#divBeneficiarios').dialog("close");
      $('#divBeneficiarios').dialog("destroy");   
   });
}

function validarCurp(curp)
{       
  var esValida = 0;
  //AAAA######AAAAAA[#,A]#    
  if(curp.match(/^([A-Z])([A,E,I,O,U,X])([A-Z]{2})([0-9]{2})([0-1])([0-9])([0-3])([0-9])([M,H])([A-Z]{2})([B,C,D,F,G,H,J,K,L,M,N,Ñ,P,Q,R,S,T,V,W,X,Y,Z]{3})([0-9,A-Z])([0-9])$/i))
  {
   esValida = 1;
  }  
  return esValida;
}

function validarGuardarBeneficiarios()
{
  var sPaterno = $("#txtPaternoBeneficiario").val();
  var sMaterno = $("#txtMaternoBeneficiario").val();
  var sNombre = $("#txtNombreBeneficiario").val();
  var sCurp = $("#txtCurpBeneficiario").val();  
  var iParentescto = $("#slctParentescoBeneficiario").val();
  var sParentesco = $('#slctParentescoBeneficiario option:selected').text();  
  
  var sParentesco = "test";
  var dPorcentaje = $("#txtPorcentajeBeneficiario").val();
  var bBandera = false;
  if(sPaterno.length > 0) 
  {
      if (sNombre.length > 0)
      {
          if (sCurp.length == 18 && validarCurp(sCurp)) 
          {
              if (iParentescto >= 0) 
              {
                 if (dPorcentaje > 0 && dPorcentaje <= 100) 
                 {
                   bBandera = true;
                 }
                 else
                 {
                   mensajeAdvertencia("Indique un porcentaje adecuado para el beneficiario.");
                 }
              }
              else
              {
                mensajeAdvertencia("Seleccione por favor un parentesco.");
              }
          } 
          else
          {
            mensajeAdvertencia("Escriba por favor el CURP del beneficiario de forma correcta y con en su totalidad (18 caracteres).");
          }
      }
      else
      {
        mensajeAdvertencia("Escriba por favor el(los) nombre(s) del beneficiario.");
      }
  }
  else
  {
    mensajeAdvertencia("Escriba por favor el apellido paterno del beneficiario.");
  }

  return bBandera;

}

function validarPorcentajeBeneficiario(iRowId)
{
    var iPorcentaje = 0;
    var bBandera = false;
    var grid = null;
    var i = 0;
    var dPorcentaje = 0;
    var cCelda = "";
    var rows = "";
    grid = $("#listaBeneficiarios");
    if (grid.jqGrid('getGridParam', 'records') > 0) 
    {
        rows = grid.jqGrid('getDataIDs');        
        for(i = 0; i < rows.length; i++)
        {
          if (typeof iRowId == "undefined") 
          {
             cCelda = grid.getCell(rows[i], 'porcentaje');
          }            
          else
          {
             cCelda = iRowId == rows[i] ? 0 : grid.getCell(rows[i], 'porcentaje');
          }
          if(typeof  cCelda != "undefined"){
            dPorcentaje = parseInt(cCelda) + dPorcentaje;            
          }
        }            
      dPorcentaje = parseInt(dPorcentaje) + parseInt($("#txtPorcentajeBeneficiario").val());
      if (dPorcentaje <= 100) 
      {
        bBandera = true;
      }
    }
    else
    {
       bBandera = true;
    }    
    return bBandera;
}

function mensajeAdvertencia(sMensaje)
{
	$( "#divMensaje" ).html("");	
	$( "#divMensaje" ).append("<p><span class='ui-icon ui-icon-alert' style='float:left; margin:0 7px 20px 0;'></span>" + sMensaje + "</p>");	
	$( "#divMensaje" ).dialog({
      resizable: false,
      height:250,
      width: 500,
      modal: true,
      title: "ADVERTENCIA",
      buttons: {
        "Aceptar": function() {
          $( this ).dialog( "close" );
        },        
      }
    });  
}

function mensajeExito(sMensaje)
{
	var newHtml = "<div class='exito'>";
	newHtml += "<p>";
	newHtml += sMensaje;
	newHtml += "</p></div>";	
	$('#divMensaje').html(newHtml);
	$( "#divMensaje" ).dialog({
      resizable: false,
      height:230,
      width: 500,
      modal: true,
      title: "ÉXITO",      
      buttons: {
        "Aceptar": function() {
          $( this ).dialog( "close" );
        },        
      }
    });  
}

function cargarGrid(){
	$('#gridBeneficiarios').html('<div align="center">');
		$('#gridBeneficiarios').append('<table id="listaBeneficiarios" style="width:100%">');
		$('#gridBeneficiarios').append('</table>');
		$('#gridBeneficiarios').append('<div id="paginadorBeneficiarios">');
		$('#gridBeneficiarios').append('</div>');
	$('#gridBeneficiarios').append('</div>');

	$("#listaBeneficiarios").jqGrid({
		url: "",				
		datatype: "json",
		async: true,
		colNames:[
					'id',				
					'Apellido Paterno',
					'Apellido Materno',
					'Nombre(s)',
					'CURP',
					'Parentesco o Relaci&oacute;n',
          'idparentesco',
					'Porcentaje'
				 ],
		colModel:[
					       {name:'id', 		 hidden:true,  index:'id',         width:100, align:"center", editoptions:{readonly:true}},																				
                 {name:'paterno', 	 hidden:false, index:'paterno',    width:150,  align:"center", editoptions:{readonly:true}},
                 {name:'materno',   	 hidden:false, index:'materno',    width:150, align:"center", editoptions:{readonly:true}},
                 {name:'nombres',     hidden:false, index:'nombres',    width:150, align:"center", editoptions:{readonly:true}},
                 {name:'curp', 	     hidden:false, index:'curp',       width:150, align:"center", editoptions:{readonly:true}},
                 {name:'parentesco',  hidden:false, index:'parentesco', width:150, align:"center", editoptions:{readonly:true}},
                 {name:'idparentesco',  hidden:true, index:'idparentesco', width:150, align:"center", editoptions:{readonly:true}},
                 {name:'porcentaje',  hidden:false, index:'porcentaje', width:105, align:"center", formatter:'integer', 
                 formatoptions:{ thousandsSeparator: " ", defaultValue: '0'}, editoptions:{readonly:true}}
				],
		beforeSend: function()
		{
			bloquearUI("");			
		},
		loadError: function(xhr, status, error)
		{
			desbloquearUI();			
		},
		loadComplete: function(respuesta)
		{	
			desbloquearUI();	      
		},				
		caption: 'Datos Beneficiario',		
		pager: "paginadorBeneficiarios",
	    pgbuttons: false, 
		pgtext: false,
		pginput:false,    
		shrinkToFit: false,
		sortname: 'id',
		viewrecords: true,
		multiselect: false,
		forceFit:false,
		sortorder: "desc",           
		height:125,
		width:885		
	});



}

function llenarSelectParentesco(){
  var sPhp = null;
  sPhp = "php/beneficiarios.php";
  $.ajax({
    async: false,
    cache: true,
    url: sPhp,
    type: 'POST',    
    dataType: 'JSON',
    data: {iOpcion: 1},
    error: function (xhr, ajaxOptions, thrownError) 
    { 
        mensajeAdvertencia("Ocurri&oacute; un error al cargar el cat&aacute;logo de parentesco, por favor, reporte a Mesa de Ayuda.");
        console.log(thrownError);
    },
    success: function(respuesta)
    {
        try
        {
            if (respuesta.codigoRespuesta == 1) 
            {

                $("#slctParentescoBeneficiario").append("<option value=-1></option>");
                for (var i = 0; i < respuesta.parentescos.length; i++) 
                {
                    $("#slctParentescoBeneficiario").append("<option value="+respuesta.parentescos[i].idparentesco+">"
                                                  +respuesta.parentescos[i].descripcion+"</option>");
                }

            } 
            else
            {
                mensajeAdvertencia(respuesta.descripcion);
            }            
            
        }
        catch(err)
        {
            mensajeAdvertencia("Ocurri&oacute; un error al cargar la respuesta del cat&aacute;logo de parentesco, por favor, reporte a Mesa de Ayuda.");
            console.log(err);
        }
    }
  });  
  
}

function validarPorcentajeCompleto(){
  var bBandera = false;
  var grid = null;
  var rows = "";
  var cCelda = "";
  var dPorcentaje = 0;
  grid = $("#listaBeneficiarios");
  rows = grid.jqGrid('getDataIDs');        
  for (var i = 0; i < rows.length; i++) {
    cCelda = grid.getCell(rows[i], 'porcentaje');
    if(typeof  cCelda != "undefined"){
       dPorcentaje = parseInt(cCelda) + dPorcentaje;            
    }
  }
  if (dPorcentaje == 100) 
  {
     bBandera = true;
  }

  return bBandera;
}

function regresarBeneficiarios(){
  var grid = $("#listaBeneficiarios");  
  var apellidoPaterno = "";
  var apellidoMaterno = "";
  var nombre = "";
  var curp = "";
  var parentesco = 0;
  var porcentaje = 0;
  var rows = "";
  var jsonObj = [];  
  if (grid.jqGrid('getGridParam', 'records') > 0) 
  {
      rows = grid.jqGrid('getDataIDs');        
      for(i = 0; i < rows.length; i++)
      {

        arrBeneficiarios[i] = grid.jqGrid('getRowData',rows[i]); 

      } 
      if (validarPorcentajeCompleto()) 
      {  
        $("#divBeneficiarios").dialog("destroy");      
        if(typeof callback == 'function')
        {            
            callback();
        }        
      }
      else
      {
        mensajeAdvertencia("Necesita repartir por completo (100%) el porcentaje entre los beneficiarios.");
      }
  }  
  else
  {
      mensajeAdvertencia("No ha dado de alta ning&uacute;n beneficiario.");
  }

}

function aMayusculas(e)
{
  var sCadena = $( e.currentTarget ).val();

    sCadena = sCadena.toUpperCase();

    $( e.currentTarget ).val(sCadena);
}