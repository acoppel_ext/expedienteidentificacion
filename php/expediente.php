<?php
	header("ISO-8859-1");
	include_once("Clases/global.php");
	include_once("Clases/CLog.php");
	
	$modificacion = array("fechasolicitud"=>'',"folio"=>'',"nss"=>'',"curp"=>'',"rfc"=>'',"nombre"=>'',"apellidopaterno"=>'',
	"apellidomaterno"=>'',"promotor"=>'',"nombrefuncionario"=>'',"fechanacimiento"=>'',"genero"=>'',"cvenaturalgenero"=>'',
	"descgenero"=>'',"nacionalidad"=>'',"cvenaturalnacionalidad"=>'',"descnacionalidad"=>'',"entidaddenacimiento"=>'',
	"cvenaturalentidaddenacimiento"=>'',"descentidaddenacimiento"=>'',"ocupacion"=>'',"cvenaturalocupacion"=>'',"descocupacion"=>'',
	"actividadgironegocio"=>'',"cvenaturalactividadgironegocio"=>'',"descactividadgironegocio"=>'',"nivelestudio"=>'',
	"cvenaturalnivelestudio"=>'',"descnivelestudio"=>'',"telefono01"=>'',"tipotelefono01">'',"cvenaturaltipotelefono01"=>'',
	"desctipotelefono01"=>'',"telefono02"=>'',"tipotelefono02"=>'',"cvenaturaltipotelefono02"=>'',"desctipoteleono02"=>'',
	"correoelectronico"=>'', "codigoRespuesta" =>0,"descripcion" =>'');
					
	$domicilios = array("folio" =>'',"tipodomicilio" =>'',"codigopostal" =>'',"colonia" =>'',"calle" =>'',"numeroexterior" =>'',
	"numerointerior" =>'',"delegacionmunicipio" =>'',"ciudad" =>'',"pais" =>'',"entidadfederativa" =>'',"tipodomicilio1" =>'',
	"codigopostal1" =>'',"colonia1" =>'',"calle1" =>'',"numeroexterior1" =>'',"numerointerior1" =>'',"delegacionmunicipio1" =>'',
	"ciudad1" =>'',"pais1" =>'',"entidadfederativa1" =>'',"desctipodomicilio" =>'',"desccolonia" =>'',"descdelegacionmunicipio" =>'',
	"descciudad" =>'',"descpais" =>'',"descentidadfederativa" =>'',"cvenaturalpais" =>'',"cvedomicilio" =>'',"desctipodomicilio1" =>'',
	"desccolonia1" =>'',"descdelegacionmunicipio1" =>'',"descciudad1" =>'',"dscpais1" =>'',"descentidadfederativa1" =>'',
	"cvenaturalpais1" =>'',"cvedomicilio1" =>'',"codigoRespuesta"=>0,"descripcion"=>'');
	
	$domiciliosAux = array("desccolonia" =>'',"descdelegacionmunicipio" =>'',"descentidadfederativa" =>'',"desccolonia1" =>'',
	"descdelegacionmunicipio1" =>'',"descentidadfederativa1" =>'');
	
	$beneficiarios = array("folio"=>'',"curp"=>'',"nombre"=>'',"apellidopaterno"=>'',"apellidomaterno"=>'',"parentesco"=>'',
	"cvenatural"=>'',"descparentesco"=>'',"porcentaje"=>'',"curp1"=>'',"nombre1"=>'',"apellidopaterno1"=>'',"apellidomaterno1"=>'',
	"parentesco1"=>'',"cvenatural1"=>'',"descparentesco1"=>'',"porcentaje1"=>'',"curp2"=>'',"nombre2"=>'',"apellidopaterno2"=>'',
	"apellidomaterno2"=>'',	"parentesco2"=>'',"cvenatural2	"=>'',"descparentesco2"=>'',"porcentaje2"=>'',"curp3"=>'',"nombre3"=>'',
	"apellidopaterno3"=>'',"apellidomaterno3"=>'',"parentesco3"=>'',"cvenatural3"=>'',"descparentesco3"=>'',"porcentaje3"=>'',
	"curp4"=>'',"nombre4"=>'',"apellidopaterno4"=>'',"apellidomaterno4"=>'',"parentesco4"=>'',"cvenatural4"=>'',
	"descparentesco4"=>'',"porcentaje4"=>'',"codigoRespuesta"=>0,"descripcion"=>'');
	
	$referencias = array("folio"=>'',"curp"=>'',"nombre"=>'',"apellidopaterno"=>'',"apellidomaterno"=>'',"telefono"=>'',
	"tipotelefono"=>'',"desctipotelefono"=>'',"parentesco"=>'',"cveparentesco"=>'',"descparentesco"=>'',"curp1"=>'',"nombre1"=>'',
	"apellidopaterno1"=>'',"apellidomaterno1"=>'',"telefono1"=>'',"tipotelefono1"=>'',"desctipotelefono1"=>'',"parentesco1"=>'',
	"cveparentesco1"=>'',"descparentesco1"=>'',"codigoRespuesta"=>0,"descripcion"=>'',"fechanacimiento"=>'',"fechanacimiento1"=>'',
	"genero"=>0,"genero1"=>0,"entidadnacimiento"=>0,"entidadnacimiento1"=>0);
	
	$arrDatosAfiliado = array();
	$iBandera = 0;

	$cFolio = isset($_POST["cFolio"]) ? $_POST["cFolio"] : 0;
	
	if ($cFolio > 0)
	{	
		$cFolio = $cFolio.'-S';
		
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);		
		if ($cnxOdbc) 
		{	
			$cSql = "SELECT fechasolicitud,folio,nss,curp,rfc,nombre,apellidopaterno,apellidomaterno,promotor,nombrefuncionario,
			fechanacimiento,genero,cvenaturalgenero,descgenero,nacionalidad,cvenaturalnacionalidad,descnacionalidad,
			entidaddenacimiento,cvenaturalentidaddenacimiento,descentidaddenacimiento,ocupacion,cvenaturalocupacion,descocupacion,
			actividadgironegocio,cvenaturalactividadgironegocio,descactividadgironegocio,nivelestudio,cvenaturalnivelestudio,
			descnivelestudio,telefono01,tipotelefono01,cvenaturaltipotelefono01,desctipotelefono01,telefono02,tipotelefono02,
			cvenaturaltipotelefono02,desctipotelefono02,correoelectronico FROM fnconsultaeiserviciosmodificacion('$cFolio');";			
			CLog::escribirLog($cSql);
			$resultSet = $cnxOdbc->query($cSql);			
			//Valida el resultado de la consulta
			if ($resultSet) 
			{
				foreach ($resultSet as $resultado) 
				{
					$pos = strpos($resultado["nombrefuncionario"], '�');
					if ( $pos < 0 )
						$pos = strpos($resultado["nombrefuncionario"], '�');
						

					if ( $pos >= 0 )
					{
						$modificacion["nombrefuncionario"] = utf8_encode( $resultado["nombrefuncionario"] );
					}
					else
					{
						$modificacion["nombrefuncionario"] = $resultado["nombrefuncionario"] ;
					}
						
				
					$modificacion["fechasolicitud"] = $resultado["fechasolicitud"];
					$modificacion["folio"] = $resultado["folio"];
					$modificacion["nss"] = $resultado["nss"];
					$modificacion["curp"] = $resultado["curp"];
					$modificacion["rfc"] = $resultado["rfc"];
					$modificacion["nombre"] = utf8_encode($resultado["nombre"]);
					$modificacion["apellidopaterno"] = utf8_encode($resultado["apellidopaterno"]);
					$modificacion["apellidomaterno"] =  utf8_encode($resultado["apellidomaterno"]);
					$modificacion["promotor"] =  utf8_encode($resultado["promotor"]);
					
					$modificacion["fechanacimiento"] = $resultado["fechanacimiento"];
					$modificacion["genero"] = $resultado["genero"];
					$modificacion["cvenaturalgenero"] = $resultado["cvenaturalgenero"];
					$modificacion["descgenero"] = $resultado["descgenero"];
					$modificacion["nacionalidad"] = $resultado["nacionalidad"];
					$modificacion["cvenaturalnacionalidad"] = $resultado["cvenaturalnacionalidad"];
					$modificacion["descnacionalidad"] = $resultado["descnacionalidad"];
					$modificacion["entidaddenacimiento"] = $resultado["entidaddenacimiento"];
					$modificacion["cvenaturalentidaddenacimiento"] = $resultado["cvenaturalentidaddenacimiento"];
					$modificacion["descentidaddenacimiento"] = $resultado["descentidaddenacimiento"];
					$modificacion["ocupacion"] = $resultado["ocupacion"];
					$modificacion["cvenaturalocupacion"] = $resultado["cvenaturalocupacion"];
					$modificacion["descocupacion"] = $resultado["descocupacion"];
					$modificacion["actividadgironegocio"] = $resultado["actividadgironegocio"];
					$modificacion["cvenaturalactividadgironegocio"] = $resultado["cvenaturalactividadgironegocio"];
					$modificacion["descactividadgironegocio"] = $resultado["descactividadgironegocio"];
					$modificacion["nivelestudio"] = $resultado["nivelestudio"];
					$modificacion["cvenaturalnivelestudio"] = $resultado["cvenaturalnivelestudio"];
					$modificacion["descnivelestudio"] = $resultado["descnivelestudio"];
					$modificacion["telefono01"] = $resultado["telefono01"];
					$modificacion["tipotelefono01"] = $resultado["tipotelefono01"];
					$modificacion["cvenaturaltipotelefono01"] = $resultado["cvenaturaltipotelefono01"];
					$modificacion["desctipotelefono01"] = $resultado["desctipotelefono01"];
					$modificacion["telefono02"] = $resultado["telefono02"];
					$modificacion["tipotelefono02"] = $resultado["tipotelefono02"];
					$modificacion["cvenaturaltipotelefono02"] = $resultado["cvenaturaltipotelefono02"];
					$modificacion["desctipotelefono02"] = $resultado["desctipotelefono02"];
					$modificacion["correoelectronico"] = $resultado["correoelectronico"];
				}
				$modificacion["codigoRespuesta"] = OK__;
				$modificacion["descripcion"]     = "EXITO";
			}	
			else
			{
				$modificacion["codigoRespuesta"] = ERR__;
				$modificacion["descripcion"]     = "Ocurrio un problema al consultar los datos de modificados del trabajador.";
			}
		} 
		else 
		{
	
			$modificacion["codigoRespuesta"] = ERR__;
			$modificacion["descripcion"]     = "Ocurrio un problema en la conexion a la base de datos[MODIFICACION].";

			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		$arrDatosAfiliado["ModTrabajador"] [] = array_map('trim',$modificacion);
	
		//----------------------------------------------------------------------------------------------------------------
	
		$cSql = null;
		
		if ($cnxOdbc) 
		{
			$cSql = "SELECT folio,curp,nombre,apellidopaterno,apellidomaterno,telefono,tipotelefono,desctipotelefono,parentesco,
			cveparentesco,descparentesco,curp1,nombre1,apellidopaterno1,apellidomaterno1,telefono1,tipotelefono1,desctipotelefono1,
			parentesco1,cveparentesco1,descparentesco1,fechanacimiento1,fechanacimiento,genero,genero1,entidadnacimiento,
			entidadnacimiento1 FROM fnconsultaeiserviciosreferencia('$cFolio');";
			CLog::escribirLog($cSql);
			$resultSet = $cnxOdbc->query($cSql);			
			//Valida el resultado de la consulta
			if ($resultSet) 
			{
				foreach ($resultSet as $resultado) 
				{
					$iBandera = 1;
					$referencias["folio"] 			= $resultado["folio"];
					$referencias["curp"] 			= $resultado["curp"];
					$referencias["nombre"] 			=  utf8_encode($resultado["nombre"]);
					$referencias["apellidopaterno"] =  utf8_encode($resultado["apellidopaterno"]);
					$referencias["apellidomaterno"] =  utf8_encode($resultado["apellidomaterno"]);
					$referencias["telefono"] 		= $resultado["telefono"];
					$referencias["tipotelefono"] 	= $resultado["tipotelefono"];
					$referencias["desctipotelefono"] = $resultado["desctipotelefono"];
					$referencias["parentesco"] 		= $resultado["parentesco"];
					$referencias["cveparentesco"] 	= $resultado["cveparentesco"];
					$referencias["descparentesco"] 	= $resultado["descparentesco"];
					$referencias["curp1"] 			 = $resultado["curp1"];
					$referencias["nombre1"] 		 =  utf8_encode($resultado["nombre1"]);
					$referencias["apellidopaterno1"] =  utf8_encode($resultado["apellidopaterno1"]);
					$referencias["apellidomaterno1"] =  utf8_encode($resultado["apellidomaterno1"]);
					$referencias["telefono1"] 		 = $resultado["telefono1"];
					$referencias["tipotelefono1"] 	 = $resultado["tipotelefono1"];
					$referencias["desctipotelefono1"] = $resultado["desctipotelefono1"];
					$referencias["parentesco1"] 	 = $resultado["parentesco1"];
					$referencias["cveparentesco1"] 	 = $resultado["cveparentesco1"];
					$referencias["descparentesco1"]  = $resultado["descparentesco1"];
					$referencias["fechanacimiento1"]  = $resultado["fechanacimiento1"];
					$referencias["fechanacimiento"]  = $resultado["fechanacimiento"];
					$referencias["genero"]  		= $resultado["genero"];
					$referencias["genero1"]  		= $resultado["genero1"];
					$referencias["entidadnacimiento"] = $resultado["entidadnacimiento"];
					$referencias["entidadnacimiento1"] = $resultado["entidadnacimiento1"];
				}
				if ($iBandera == 0)
				{
					$arrDatosAfiliado["referencias"] [] = array_map('trim',$referencias);
				}
				$iBandera = 0;
				$referencias["codigoRespuesta"] = OK__;
				$referencias["descripcion"]     = "EXITO";
			}	
			else
			{
				$referencias["codigoRespuesta"] = ERR__;
				$referencias["descripcion"]     = "Ocurrio un problema al consultar las referencias del trabajador.";
			}
		} 
		else 
		{
			$referencias["codigoRespuesta"] = ERR__;
			$referencias["descripcion"]     = "Ocurrio un problema en la conexion a la base de datos [REFERENCIA].";
			
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		
		$arrDatosAfiliado["referencias"] [] = array_map('trim',$referencias);
		
		//--------------------------------------------------------------------------------------------------------------------------------------

		$cSql = null;		
		
		if ($cnxOdbc) 
		{
			$cSql = "SELECT folio,curp,nombre,apellidopaterno,apellidomaterno,parentesco,cvenatural,descparentesco,porcentaje,curp1,
			nombre1,apellidopaterno1,apellidomaterno1,parentesco1,cvenatural1,descparentesco1,porcentaje1,curp2,nombre2,
			apellidopaterno2,apellidomaterno2,parentesco2,cvenatural2,descparentesco2,porcentaje2,curp3,nombre3,apellidopaterno3,
			apellidomaterno3,parentesco3,cvenatural3,descparentesco3,porcentaje3,curp4,nombre4,apellidopaterno4,apellidomaterno4,
			parentesco4,cvenatural4,descparentesco4,porcentaje4 FROM fnconsultaeiserviciosbeneficiario('$cFolio');";
			CLog::escribirLog($cSql);
			$resultSet = $cnxOdbc->query($cSql);			
			//Valida el resultado de la consulta
			if ($resultSet) 
			{
				foreach ($resultSet as $resultado) 
				{
					$iBandera = 1;
					$beneficiarios["folio"] 			= $resultado["folio"];
					$beneficiarios["curp"] 				= $resultado["curp"];
					$beneficiarios["nombre"] 			=  utf8_encode($resultado["nombre"]);
					$beneficiarios["apellidopaterno"] 	=  utf8_encode($resultado["apellidopaterno"]);
					$beneficiarios["apellidomaterno"] 	=  utf8_encode($resultado["apellidomaterno"]);
					$beneficiarios["parentesco"] 		= $resultado["parentesco"];
					$beneficiarios["cvenatural"] 		= $resultado["cvenatural"];
					$beneficiarios["descparentesco"] 	= $resultado["descparentesco"];
					$beneficiarios["porcentaje"] 		= $resultado["porcentaje"];
					$beneficiarios["curp1"] 			= $resultado["curp1"];
					$beneficiarios["nombre1"] 			=  utf8_encode($resultado["nombre1"]);
					$beneficiarios["apellidopaterno1"] 	=  utf8_encode($resultado["apellidopaterno1"]);
					$beneficiarios["apellidomaterno1"] 	=  utf8_encode($resultado["apellidomaterno1"]);
					$beneficiarios["parentesco1"] 		= $resultado["parentesco1"];
					$beneficiarios["cvenatural1"] 		= $resultado["cvenatural1"];
					$beneficiarios["descparentesco1"] 	= $resultado["descparentesco1"];
					$beneficiarios["porcentaje1"] 		= $resultado["porcentaje1"];
					$beneficiarios["curp2"] 			= $resultado["curp2"];
					$beneficiarios["nombre2"] 			=  utf8_encode($resultado["nombre2"]);
					$beneficiarios["apellidopaterno2"] 	=  utf8_encode($resultado["apellidopaterno2"]);
					$beneficiarios["apellidomaterno2"] 	=  utf8_encode($resultado["apellidomaterno2"]);
					$beneficiarios["parentesco2"] 		= $resultado["parentesco2"];
					$beneficiarios["cvenatural2"] 		= $resultado["cvenatural2"];
					$beneficiarios["descparentesco2"] 	= $resultado["descparentesco2"];
					$beneficiarios["porcentaje2"] 		= $resultado["porcentaje2"];
					$beneficiarios["curp3"] 			= $resultado["curp3"];
					$beneficiarios["nombre3"] 			=  utf8_encode($resultado["nombre3"]);
					$beneficiarios["apellidopaterno3"] 	=  utf8_encode($resultado["apellidopaterno3"]);
					$beneficiarios["apellidomaterno3"] 	=  utf8_encode($resultado["apellidomaterno3"]);
					$beneficiarios["parentesco3"] 		= $resultado["parentesco3"];
					$beneficiarios["cvenatural3"] 		= $resultado["cvenatural3"];
					$beneficiarios["descparentesco3"] 	= $resultado["descparentesco3"];
					$beneficiarios["porcentaje3"] 		= $resultado["porcentaje3"];
					$beneficiarios["curp4"] 			= $resultado["curp4"];
					$beneficiarios["nombre4"] 			=  utf8_encode($resultado["nombre4"]);
					$beneficiarios["apellidopaterno4"] 	=  utf8_encode($resultado["apellidopaterno4"]);
					$beneficiarios["apellidomaterno4"] 	=  utf8_encode($resultado["apellidomaterno4"]);
					$beneficiarios["parentesco4"] 		= $resultado["parentesco4"];
					$beneficiarios["cvenatural4"] 		= $resultado["cvenatural4"];
					$beneficiarios["descparentesco4"] 	= $resultado["descparentesco4"];
					$beneficiarios["porcentaje4"] 		= $resultado["porcentaje4"];
				}
				if ($iBandera == 0)
				{
					$arrDatosAfiliado["beneficiarios"] [] = array_map('trim',$beneficiarios);
				}
				$iBandera = 0;
				$beneficiarios["codigoRespuesta"] = OK__;
				$beneficiarios["descripcion"]     = "EXITO";
			}	
			else
			{
				$beneficiarios["codigoRespuesta"] = ERR__;
				$beneficiarios["descripcion"]     = "Ocurrio un problema al consultar los beneficiarios del trabajador.";
			}
		} 
		else 
		{
			$beneficiarios["codigoRespuesta"] = ERR__;
			$beneficiarios["descripcion"]     = "Ocurrio un problema en la conexion a la base de datos [BENEFICIARIOS].";

			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		$arrDatosAfiliado["beneficiarios"] [] = array_map('trim',$beneficiarios);
		//---------------------------------------------------------------------------------------------------------------------------------

		$cSql = null;
		$sTexto = '';
		
		if ($cnxOdbc) 
		{
			$cSql = "SELECT folio,tipodomicilio,codigopostal,colonia,calle,numeroexterior,numerointerior,delegacionmunicipio,ciudad,pais,
			entidadfederativa,desctipodomicilio,desccolonia,descdelegacionmunicipio,descciudad,descpais,descentidadfederativa,
			cvenaturalpais,cvedomicilio,tipodomicilio1,codigopostal1,colonia1,calle1,numeroexterior1,numerointerior1,delegacionmunicipio1,
			ciudad1,pais1,entidadfederativa1,desctipodomicilio1,desccolonia1,descdelegacionmunicipio1,descciudad1,descpais1,
			descentidadfederativa1,cvenaturalpais1,cvedomicilio1 FROM fnconsultaeiserviciosdomicilio('$cFolio');";
			CLog::escribirLog($cSql);
			$resultSet = $cnxOdbc->query($cSql);

			//Valida el resultado de la consulta
			if ($resultSet) 
			{
				foreach ($resultSet as $resultado) 
				{					
	
					$iBandera = 1;
					$domicilios["folio"]					= $resultado["folio"];
					$domicilios["tipodomicilio"]			= $resultado["tipodomicilio"];
					$domicilios["codigopostal"]				= $resultado["codigopostal"];
					$domicilios["colonia"]					= $resultado["colonia"];
					$domicilios["calle"]					= utf8_encode($resultado["calle"]);
					$domicilios["numeroexterior"]			= utf8_encode($resultado["numeroexterior"]);
					$domicilios["numerointerior"]			= utf8_encode($resultado["numerointerior"]);
					$domicilios["delegacionmunicipio"]		= $resultado["delegacionmunicipio"]  ;
					$domicilios["ciudad"]					= $resultado["ciudad"];
					$domicilios["pais"]						= $resultado["pais"];
					$domicilios["entidadfederativa"]		= $resultado["entidadfederativa"];
					$domicilios["entidadfederativa1"]		= $resultado["entidadfederativa1"];
					$domicilios["desctipodomicilio"]		= $resultado["desctipodomicilio"];
					$domicilios["desccolonia"]			    = utf8_encode($resultado["desccolonia"]);
					$domicilios["descdelegacionmunicipio"]	= utf8_encode($resultado["descdelegacionmunicipio"]);
					$domicilios["descciudad"] 				= utf8_encode($resultado["descciudad"]);
					$domicilios["descpais"] 				= $resultado["descpais"];
					$domicilios["descentidadfederativa"]    = $resultado["descentidadfederativa"];
					
					//$sTexto	= busquedaenie($resultado["desccolonia"]);//
					//$domiciliosAux["desccolonia"] = $sTexto;
					//$sTexto	= busquedaenie($resultado["descdelegacionmunicipio"]);//
					//$domiciliosAux["descdelegacionmunicipio"]	= $sTexto;
					//$sTexto = busquedaenie($resultado["descentidadfederativa"]);//
					//$domiciliosAux["descentidadfederativa"] = $sTexto;
					
					$domicilios["cvenaturalpais"]			= $resultado["cvenaturalpais"];
					$domicilios["cvedomicilio"]				= $resultado["cvedomicilio"];
					$domicilios["tipodomicilio1"]			= $resultado["tipodomicilio1"];
					$domicilios["codigopostal1"]			= $resultado["codigopostal1"];
					$domicilios["colonia1"]					= $resultado["colonia1"];
					$domicilios["calle1"]					= utf8_encode($resultado["calle1"]);
					$domicilios["numeroexterior1"]			= utf8_encode($resultado["numeroexterior1"]);
					$domicilios["numerointerior1"]			= utf8_encode($resultado["numerointerior1"]);
					$domicilios["delegacionmunicipio1"]		= $resultado["delegacionmunicipio1"];
					$domicilios["ciudad1"]					= $resultado["ciudad1"];
					$domicilios["pais1"]					= $resultado["pais1"];
					$domicilios["desctipodomicilio1"]		= $resultado["desctipodomicilio1"];
					$domicilios["desccolonia1"] 			= utf8_encode($resultado["desccolonia1"]);
					$domicilios["descdelegacionmunicipio1"]	= utf8_encode($resultado["descdelegacionmunicipio1"]);
					$domicilios["descciudad1"]	 			= utf8_encode($resultado["descciudad1"]);
					$domicilios["descpais1"]	 			= $resultado["descpais1"];
					$domicilios["descentidadfederativa1"]   = $resultado["descentidadfederativa1"];
					
					//$sTexto	= busquedaenie($resultado["desccolonia1"]);//
					//$domiciliosAux["desccolonia1"] = $sTexto;
				    //$sTexto	= busquedaenie($resultado["descdelegacionmunicipio1"]);//
					//$domiciliosAux["descdelegacionmunicipio1"]	= $sTexto;
				    //$sTexto = busquedaenie($resultado["descentidadfederativa1"]);//
					//$domiciliosAux["descentidadfederativa1"] = $sTexto;
					
					$domicilios["cvenaturalpais1"]			= $resultado["cvenaturalpais1"];
					$domicilios["cvedomicilio1"]			= $resultado["cvedomicilio1"];
				}
				if ($iBandera == 0)
				{
					$arrDatosAfiliado["domicilios"] [] = array_map('trim',$domicilios);
					$arrDatosAfiliado["domiciliosAux"] [] = array_map('trim',$domiciliosAux);
				}
				$iBandera = 0;
				$domicilios["codigoRespuesta"] = OK__;
				$domicilios["descripcion"]     = "EXITO";
			}	
			else
			{
				$domicilios["codigoRespuesta"] = ERR__;
				$domicilios["descripcion"]     = "Ocurrio un error al consultar el domicilio del trabajador.";
			}
		} 
		else 
		{
			$domicilios["codigoRespuesta"] = ERR__;
			$domicilios["descripcion"]     = "Ocurrio un error en la conxion a la base de datos [DOMICILIO].";
			
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		$arrDatosAfiliado["domicilios"] [] = array_map('trim',$domicilios);
		$arrDatosAfiliado["domiciliosAux"] [] = array_map('trim',$domiciliosAux);
		
		$cnxOdbc = null;
	}
	/*Metodo para cambiar la letra � cuando se obtiene de la base de datos*/
	function busquedaenie($cadena)
	{
		$sCadenaAux = '';
		
		$pos = strpos($cadena, '�');
		if ( $pos < 0 )
			$pos = strpos($cadena, '�');
			
		if ( $pos >= 0 )
		{
			$sCadenaAux = utf8_encode( $cadena );
		}
		else
		{
			$sCadenaAux = $cadena ;
		}
		return $sCadenaAux;
	}
	echo json_encode($arrDatosAfiliado);
?>