<?php
	//Inlude necesarios
	include_once("Clases/CExpedienteIdentificacion.php");
	include_once ('Clases/CMetodoGeneral.php');
	include_once("Clases/CBeneficiarios.php");
	include_once("Clases/CCatalogo.php");

	$objGn = new CMetodoGeneral();
	$sIpRemoto = '';
	$sIpRemoto = $objGn->getRealIP();
	$arrRespuesta = array();
	
	//Se obtienen los parametros que se enviaron por POST
	$iOpcion = isset($_POST["iOpcion"]) ? $_POST["iOpcion"] : 0;
	$objTrabajador = isset($_POST["objTrabajador"]) ? $_POST["objTrabajador"] : null;
	$cFolio = isset($_POST["cFolio"]) ? $_POST["cFolio"] : 0;	
	$cIdEstado = isset($_POST["cIdEstado"]) ? $_POST["cIdEstado"] : '';
	$iNumeroEmpleado = isset($_POST['iNumeroEmpleado']) ? $_POST['iNumeroEmpleado'] : 0;
	$sNss	= isset($_POST['sNss']) ? $_POST['sNss'] : '';
	$cCurp	= isset($_POST['cCurp']) ? $_POST['cCurp'] : '';
	$sFechaNacimiento = isset($_POST['sFechaNacimiento']) ? $_POST['sFechaNacimiento'] : '1900-01-01';
	$iTipoServicio  = isset($_POST['iTipoServicio']) ? $_POST['iTipoServicio'] : -1;
	$iRespuesta = isset($_POST['iRespuesta']) ? $_POST['iRespuesta'] : -1;
	
	//---------------------------------------------------------------------------------------------
	//Variables necesarias para ejecutar WS 
	//---------------------------------------------------------------------------------------------
	//RENAPO Por CURP.
	//$cCurp = isset($_POST['curp']) ? $_POST['curp']: '';
	
	//RENAPO Por Datos.
	$cApellidopaterno = isset($_POST['apellidoPaterno']) ? $_POST['apellidoPaterno']: '';  
	$cApellidomaterno = isset($_POST['apellidoMaterno']) ? $_POST['apellidoMaterno']: ''; 
	$cNombres = isset($_POST['nombre']) ? $_POST['nombre']: ''; 
	$cSexo = isset($_POST['sexo']) ? $_POST['sexo']: ''; 
	$cFechaNacimiento  =isset($_POST['fechanacimiento']) ? $_POST['fechanacimiento']:'';
	$iSlcEntidadNacimiento  =isset($_POST['entidadNacimiento']) ? $_POST['entidadNacimiento']:'';
	$iEmpleado = isset($_POST['empleado']) ? $_POST['empleado']:'';
	//Servidor para el servicio de RENAPO.
	$idServidor = 5;
	//Claves de servicios.
	//$idServicioC = 9906; //curp
	$idServicioD = 9907; //datos
	$iFolioAfore = isset($_POST['folioservicioafore']) ? $_POST['folioservicioafore']: '';
	$iFolioSolicitud = isset($_POST['foliosolicitud']) ? $_POST['foliosolicitud']: '';
	$iProceso = isset($_POST['idproceso']) ? $_POST['idproceso']: '';
	$idsubproceso= isset($_POST['idsubproceso']) ? $_POST['idsubproceso']: '';
	
	$cTelefono1	= isset($_POST['Telefono1']) ? $_POST['Telefono1'] : '';
	$cTelefono2	= isset($_POST['Telefono2']) ? $_POST['Telefono2'] : '';
	
	//Obtener URL Generación de formato de CURP consultado por RENAPO.
	$iOpcionFormatoCurp = isset($_POST['iOpcionFormatoCurp']) ? $_POST['iOpcionFormatoCurp']: '';	

	//Datos del solicitante
	$objSolicitante = isset($_POST["objSolicitante"]) ? $_POST["objSolicitante"] : null;
	
	$cTipoServicio = isset($_POST['tipoServicio']) ? $_POST['tipoServicio']: '';

	//Folio 1514 Datos con los estatus de los switch
	$cCurp= isset($_POST['cCurp']) ? $_POST['cCurp']: '';
	$iNss= isset($_POST['iNss']) ? $_POST['iNss']: '';
	$iTiposolicitud= isset($_POST['iTiposolicitud']) ? $_POST['iTiposolicitud']: 0;
	$cCorreo= isset($_POST['cCorreo']) ? $_POST['cCorreo']: '';
	$iBandnotificaciones= isset($_POST['iBandnotificaciones']) ? $_POST['iBandnotificaciones']: 0;
	$iBandedocta= isset($_POST['iBandedocta']) ? $_POST['iBandedocta']: 0;

	switch ($iOpcion) 
	{
		case 1:						
			echo json_encode(CExpedienteIdentificacion::guardarExpedienteIdentificacion($objTrabajador));			
		break;
		case 2:
			echo json_encode(CExpedienteIdentificacion::generarPDF($cFolio."-S"));
			break;
		case 3:
			echo json_encode(CCatalogo::consultarCatalogoParentesco());
		break;	
		case 4:
			echo json_encode(CCatalogo::consultarCatalogoTelefono());
			break;
		case 5:
			echo json_encode(CCatalogo::consultarCatalogoCorreoElectronico());
			break;
		case 6:
			echo json_encode(CCatalogo::consultarCatalogoOcupacion());
			break;
		case 7:
			echo json_encode(CCatalogo::consultarCatalogoActividadGiroNegocio());
			break;
		case 8:
			echo json_encode(CCatalogo::consultarCatalogoEntidadNacimiento());
			break;
		case 9:
			echo json_encode(CExpedienteIdentificacion::obtenerSiglasEntidadNacimiento($cIdEstado));
			break;
		case 10:
			echo json_encode(CExpedienteIdentificacion::obtenerCurpReferencia());
			break;
		case 11:
			echo json_encode(CExpedienteIdentificacion::obtenerCurpReferencia());
			break;
		case 12:
			echo json_encode(CCatalogo::consultarCatalogoIdentificacionesOficiales($sFechaNacimiento));
			break;
		case 13:
			echo json_encode(CCatalogo::consultarCatalogoGenero());
			break;
		case 14:
			echo json_encode(CCatalogo::consultarCatalogoNivelEstudio());
			break;
		case 15:
			echo json_encode(CExpedienteIdentificacion::verificarPromotor($iNumeroEmpleado));
			break;
		case 16:
			echo json_encode(CCatalogo::consultarCatalogoNacionalidad());
			break;
		case 17:
			echo json_encode(CCatalogo::consultarCatalogoComprobanteDomicilio());
			break;
		case 18:
			echo json_encode(CExpedienteIdentificacion::obtenerInformacionTrabajadorAfiliacion($cFolio, $iTipoServicio));
			break;
		case 19:
			echo json_encode(CExpedienteIdentificacion::obtenerInformacionTrabajador($cFolio));
			break;
		case 20:
			echo json_encode(CExpedienteIdentificacion::obtenerExpedienteDeIdentificacion($cFolio));
			break;
		case 21:			
			echo json_encode(CExpedienteIdentificacion::obtenerFechaMaximaComprobanteDomicilio());
			break;
		case 22:
			echo json_encode(CExpedienteIdentificacion::actualizarEstatusExpediente($cFolio,$iTipoServicio,$iRespuesta));
			break;
		case 23:
			echo json_encode(CExpedienteIdentificacion::obtenerFechaMaximaMayoriaEdad());
			break;
		case 24:
			echo json_encode(CExpedienteIdentificacion::vaidarMayorDeEdad());
			break;
		case 25:
			echo json_encode(CExpedienteIdentificacion::validarExpedientePublicado($cFolio,$iTipoServicio));
			break;
		case 26:
			echo json_encode(CExpedienteIdentificacion::PermisosCurpsReferecias($iTipoServicio));
			break;
		case 27:
			echo json_encode(CExpedienteIdentificacion::CurpsGenericasReferecias($iTipoReferencia));
			break;
		case 28:
			echo json_encode(CExpedienteIdentificacion::extraerDatosBancoppel($cFolio));
			break;
		case 29:
			echo json_encode(CExpedienteIdentificacion::extraerDatosRenapo($cFolio));
			break;
		case 30:
			echo json_encode(CExpedienteIdentificacion:: ejecutaServicioRenapoDatos($cApellidopaterno, $cApellidomaterno, $cNombres, $cSexo, $cFechaNacimiento,$iSlcEntidadNacimiento, $iEmpleado, $idServidor, $idServicioD));
			break;
		case 31:
			echo json_encode(CExpedienteIdentificacion:: guardarSolicitudRenapo($iFolioAfore, $iFolioSolicitud, $iProceso, $iEmpleado, $idsubproceso, $cCurp, $cApellidopaterno, $cApellidomaterno, $cNombres, $cSexo, $cFechaNacimiento,$iSlcEntidadNacimiento));
			break;
		case 32:
			$arrRespuesta = CExpedienteIdentificacion::consultaRespuestaRenapo($iFolioAfore);
			echo json_encode($arrRespuesta);
			break;
		case 33:
			echo json_encode(CExpedienteIdentificacion::obtenerURLFormatoCurp($iOpcionFormatoCurp, $iFolioAfore, $sIpRemoto));
			break;
		case 34:
			echo json_encode(CExpedienteIdentificacion::validarTelefonoListaNegra($cTelefono1,$cTelefono2,$cFolio,$iEmpleado));//392.1
			break;
		case 35://Folio 316.2
			echo json_encode(CExpedienteIdentificacion::obtenerExcepcionEnrolamiento($cFolio));
		break;
		case 36://Folio 752.1
			echo json_encode(CExpedienteIdentificacion::obtenerTipoConstancia($cFolio));
		break;
		case 37://Folio 752.1
			echo json_encode(CExpedienteIdentificacion::obtenerExcepcionSolicitante($cFolio));
		break;
		case 38://Folio 893.1
			echo json_encode(CExpedienteIdentificacion::obtenerTipoSolicitante($cFolio));
			break;
		case 39://Folio 893.1
			echo json_encode(CCatalogo::consultarCatalogoParentescoEi($cFolio));
			break;
		case 40://Folio 893.1
			echo json_encode(CExpedienteIdentificacion::obtenerDatosSolicitante($cFolio));
			break;
		case 41:////Folio 893.1
			echo json_encode(CExpedienteIdentificacion::guardarDatosSolicitante($cFolio, $objSolicitante));
			break;
		case 42://Folio 605.1
			echo json_encode(CExpedienteIdentificacion:: obtenerVerificacionei($cCurp, $iNumeroEmpleado, $cTipoServicio));
			break;
		case 43:
			echo json_encode(CExpedienteIdentificacion:: obtenerInformacionServicioEi($cCurp));
			break;
		case 44:
			echo json_encode(CExpedienteIdentificacion:: obtenerbanderabancoppel($cFolio));
			break;
		//Folio 1514
		case 45:
			$arrRespuesta = CExpedienteIdentificacion::guardarDatosSwitch($cCurp, $iNss, $iTiposolicitud, $cCorreo, $iBandnotificaciones, $iBandedocta);
			echo json_encode($arrRespuesta);
			break;
		default:
			break;
	}
?>