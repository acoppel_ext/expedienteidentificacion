<?php 
	
	include_once("JSON.php");
	include_once("Clases/global.php");
	include_once("Clases/CLog.php");
	include_once("Clases/fpdf.php");
	include_once("../../fpdi/fpdi.php");
	include_once("Clases/CMetodoExpedienteServicio.php");
	
	
	$json = new Services_JSON();
	$arrResp = array();
	$aArregloResp = array("codigoRespuesta"=>0,"descripcion"=>'',"rutapdf"=>'',"iRetorno"=>0);
	//$aDomicilio = array("domicilioparticular"=>'',"domiciliolaboral"=>'');
	//global $domicilioparticular = "";
	//global $domiciliolaboral = "";
	$aRespUnionImg = array();
	$arrDatosFormatoSer = array("tipo"=>'SEI0');
	   
	$data = json_decode($_POST['aConsulta'], true);
	$aDatosGen = json_decode($_POST['aDatosGen'], true);
	$iBanderaDatos=  isset($_POST['iBanderaDatos']) ? $_POST['iBanderaDatos']: 0;
	
	$iFolioEi =  isset($_POST['cFolio']) ? $_POST['cFolio']: '';
	$iOpcion =  isset($_POST['opcion']) ? $_POST['opcion']: 0;
	
	switch($iOpcion) 
{
	case 1:
		$arrResp = generarPDF($iFolioEi, $data, $aDatosGen);
	break;
}
echo $json->encode($arrResp);
	
	function generarPDF($folio,$datos,$aDatosGen)
	{
		$iPidActual = getmypid();
		clog::escribirLog("Folio -->".$folio);
		$iValorRetornoMod = $datos["ModTrabajador"][0]["codigoRespuesta"];
		$iValorRetornoRef = $datos["referencias"][0]["codigoRespuesta"];
		$iValorRetornoBen = $datos["beneficiarios"][0]["codigoRespuesta"];
		$iValorRetornoDom = $datos["domicilios"][0]["codigoRespuesta"];
		
		//Objetos de salida que contienen la informacion y en caso de error su descripcion		
		$datosClass = new stdClass();
		$pdf = new FPDF();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$iAltoRen = 6;
		$iAnchoLogo = 45;
		$iCol = 9;		
		$iColDatos = 50;
		$iColDatos2 = 135;
		$iRen = 8;
		$iBorde = 0;
		$sTexto = "";
		$fechaNacimiento = '';
		$iLongitud = 0;
		$sPrimerCadena = "";
		$sSegundaCadena="";
		$iPosicionY=0;
		$bBandera =0;
		
		/*Bloque de codigo para los datos principales del trabajador*/
		$sNombre = ''; $ApellidoPaterno = ''; $sApellidoMaterno = ''; $sCurp = ''; $sNss = ''; $nombretrabajador = ''; $sCaracterNss = '';
		
		$sNombre = $aDatosGen["trabajador"]["nombres"];
		$ApellidoPaterno = $aDatosGen["trabajador"]["apellidopaterno"];
		$sApellidoMaterno = $aDatosGen["trabajador"]["apellidomaterno"];
		$sCurp = $aDatosGen["trabajador"]["curp"];
		$sNss = $aDatosGen["trabajador"]["nss"];
		$sCaracterNss = substr($sNss,0,1);
		if($sCaracterNss == 'i' || $sCaracterNss == 'I')
		{	$sNss = '';}
		
		$nombretrabajador = $aDatosGen["trabajador"]["nombres"].' '.$aDatosGen["trabajador"]["apellidopaterno"].' '.$aDatosGen["trabajador"]["apellidomaterno"];
		
		clog::escribirLog("codigoRespuestaMod-->".$iValorRetornoMod."\n codigoRespuestaRef-->".$iValorRetornoRef."\n codigoRespuestaBen-->".$iValorRetornoBen."\n codigoRespuestaDom-->".$iValorRetornoDom);

		//valida que haya obtenido toda la informacion
		if($iValorRetornoMod == 1 && $iValorRetornoRef == 1 && $iValorRetornoBen == 1 && $iValorRetornoDom == 1)
		{			
			try
			{

				//Indicamos el lenguaje español para los datos tipo fecha
				date_default_timezone_set('America/Mazatlan');
				setlocale(LC_TIME, 'spanish');				
				$datosClass->ModTrabajador[0]["fechanacimiento"] = $datos["ModTrabajador"][0]["fechanacimiento"];
				//Cambiamos el formato de la fecha de nacimiento a como se requiere para la impresion
				if($datosClass->ModTrabajador[0]["fechanacimiento"] == "1900-01-01")
				{
					$datos["ModTrabajador"][0]["fechanacimiento"] = "";
				}
				else
				{
					$datos["ModTrabajador"][0]["fechanacimiento"] = strftime("%d de %B de %Y", strtotime($datos["ModTrabajador"][0]["fechanacimiento"]));
				}

				//Cambiamos el formato de la fecha de solicitud a como se requiere para la impresion
				$datos["ModTrabajador"][0]["fechasolicitud"] = ucwords(strtolower(strftime("%d / %b / %Y", strtotime($datos["ModTrabajador"][0]["fechasolicitud"]))));

				$aArregloResp["codigoRespuesta"] = ERR__;
				//Agrega el rectangulo de la hoja
				$pdf->Rect(7,7,195,265, 'D');
				$pdf->Rect(7,7,195,15, 'D');
				//Agrega el logo al PDF			
				$pdf->Image('../../imagenes/logo_aforecoppel.png', $iCol, $iRen, $iAnchoLogo);
				
				//Agrega el titulo del documento			
				$pdf->SetFont('Arial','B',9);
				$pdf->SetXY($iCol + 48, $iRen);
				$sTexto = utf8_decode("Solicitud de Actualización de Expediente de Identificación");
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, 'C');
				$pdf->SetXY($iCol + 74, $iRen + 4);
				$sTexto = utf8_decode("y Modificación de Datos");
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, 'C');
				
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Agrega el Folios de servicio y la fecha de solicitud
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol + 141, $iRen);
				//Imprime la etiqueta
				$sTexto = "Folio de Servicio:";
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor del folio
				$pdf->SetXY($iCol + 166, $iRen);				
				$sTexto = $datos["ModTrabajador"][0]["folio"];
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime la etiqueta
				$pdf->SetXY($iCol + 141, $iRen + 4);
				$sTexto = "Fecha de Solicitud: ";
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor de la fecha de solicitud
				$pdf->SetXY($iCol + 166, $iRen + 4);
				$sTexto = $datos["ModTrabajador"][0]["fechasolicitud"];
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion de los datos del trabajador
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$iRen += 14;
					$pdf->SetFont('Arial','B',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = "Datos del Trabajador";
					$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');				
					//Imprime la etiqueta del Apellido paterno
					$iRen += $iAltoRen;
					$pdf->SetFont('Arial','',8);				
					$pdf->SetXY($iCol, $iRen);
					$sTexto = "Apellido Paterno: ";
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Imprime el valor del apellido paterno
					$pdf->SetXY($iColDatos, $iRen);
					$sTexto = obtenerCadenaFormateada($ApellidoPaterno);
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Imprime la etiqueta del CURP
					$pdf->SetXY($iCol + 100, $iRen);
					$sTexto = "CURP: ";
					$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Imprime el valor del CURP
					$pdf->SetXY($iColDatos2, $iRen);
					$sTexto = strtoupper($sCurp);
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Imprime la etiqueta del Apellido Materno
					$iRen += $iAltoRen;
					$pdf->SetXY($iCol, $iRen);
					$sTexto = "Apellido Materno: ";
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
					//imprime el valor del apellido materno
					$pdf->SetXY($iColDatos, $iRen);
					$sTexto = obtenerCadenaFormateada($sApellidoMaterno);
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Imprime la etiqueta del nss
					$pdf->SetXY($iCol + 100, $iRen);
					$sTexto = "NSS: ";
					$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
					//imprime el valor del nss
					$pdf->SetXY($iColDatos2, $iRen);
					$sTexto = $sNss;
					$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Imprime la etiqueta del los Nombres
					$iRen += $iAltoRen;
					$pdf->SetXY($iCol, $iRen);
					$sTexto = "Nombre (s): ";
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');				
					//imprime el valor de los nombres
					$pdf->SetXY($iColDatos, $iRen);
					$sTexto = obtenerCadenaFormateada($sNombre);
					$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>	
				//Imprime la seccion modificacion de datos
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto =  utf8_decode("A. Modificación de Datos");
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//imprime la etiqueta Apellido paterno
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Paterno: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del apellido paterno
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["apellidopaterno"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Sexo
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "Sexo: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor de sexo
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["descgenero"]);
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Apellido Materno
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Materno: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de apellido materno
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["apellidomaterno"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Nacionalidad
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "Nacionalidad: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de nacionalidad
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["descnacionalidad"]);
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Nombres
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Nombre (s): ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime el valor de los nombres
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["nombre"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime la etiqueta del CURP
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "CURP: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del CURP
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = $datos["ModTrabajador"][0]["curp"];
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de la Fecha Nacimiento
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Fecha de Nacimiento: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime el valor de la fecha de nacimiento
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = $datos["ModTrabajador"][0]["fechanacimiento"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime la etiqueta del RFC
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "RFC: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime el valor del RFC
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = $datos["ModTrabajador"][0]["rfc"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime la etiqueta Entidad de Nacimiento
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Entidad de Nacimiento: ";
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');	
				//Imprime el valor de la entidad de nacimiento
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["descentidaddenacimiento"]);
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion domicilios
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$domicilioparticular = "";
				$domiciliolaboral = "";
				if( $datos["domicilios"][0]["tipodomicilio"] == 1)
				{
				
					//Valida si la calle viene diferente de vacio
					if( $datos["domicilios"][0]["calle"] != "" )
					{
						$domiciliolaboral = " ".$datos["domicilios"][0]["calle"]. " ";
					}
					//Valida si el numero exterior viene diferente de vacio
					if( $datos["domicilios"][0]["numeroexterior"] != "" )
					{
						$domiciliolaboral =	$domiciliolaboral.
																	"Ext: ".$datos["domicilios"][0]["numeroexterior"];
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior"] != "" )
						{
							$domiciliolaboral = $domiciliolaboral.", ".
																"Int ".$datos["domicilios"][0]["numerointerior"];
						}																			
					}
					else
					{
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior"] != "" )
						{
							$domiciliolaboral =$domiciliolaboral.
																"Int ". $datos["domicilios"][0]["numerointerior"];
						}	
					}
					//valida si la colonia viene diferente de vacio
					if(  $datos["domicilios"][0]["desccolonia"] != "" )
					{
						$domiciliolaboral =$domiciliolaboral.
															"\nCol. ". $datos["domicilios"][0]["desccolonia"];
					}
					//Valida si el codigo postal viene diferente de vacio
					if(  $datos["domicilios"][0]["codigopostal"] != "" )
					{
						$domiciliolaboral =$domiciliolaboral.
															"\nC.P. ". $datos["domicilios"][0]["codigopostal"];
					}
					//Valida si el municipio viene diferente de vacio
					if(  $datos["domicilios"][0]["descdelegacionmunicipio"] != "" )
					{
						$domiciliolaboral = $domiciliolaboral.
															"\n". $datos["domicilios"][0]["descdelegacionmunicipio"];
						//Valida si el estado de diferente de vacio
						if(  $datos["domicilios"][0]["descentidadfederativa"] != "" )
						{
							$domiciliolaboral =$domiciliolaboral.", ".
																$datos["domicilios"][0]["descentidadfederativa"];
						}
					}
					else
					{
								//Valida si el estado de diferente de vacio
						if(  $datos["domicilios"][0]["descentidadfederativa"] != "" )
						{
							$domiciliolaboral = $domiciliolaboral.
																"\n". $datos["domicilios"][0]["descentidadfederativa"];
						}
					}
				}
				if( $datos["domicilios"][0]["tipodomicilio"] == 2)
				{
					if( $datos["domicilios"][0]["calle"] != "" )
					{
						$domicilioparticular = " ".$datos["domicilios"][0]["calle"]. " ";
					}
					//Valida si el numero exterior viene diferente de vacio
					if($datos["domicilios"][0]["numeroexterior"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"Ext: ".$datos["domicilios"][0]["numeroexterior"];
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.", ".
																	"Int ".$datos["domicilios"][0]["numerointerior"];
						}																			
					}
					else
					{
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.
																	"Int ".$datos["domicilios"][0]["numerointerior"];
						}	
					}
					//valida si la colonia viene diferente de vacio
					if( $datos["domicilios"][0]["desccolonia"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"\nCol. ".$datos["domicilios"][0]["desccolonia"];
					}
					//Valida si el codigo postal viene diferente de vacio
					if( $datos["domicilios"][0]["codigopostal"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"\nC.P. ".$datos["domicilios"][0]["codigopostal"];
					}
					//Valida si el municipio viene diferente de vacio
					if( $datos["domicilios"][0]["descdelegacionmunicipio"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"\n".$datos["domicilios"][0]["descdelegacionmunicipio"];
						//Valida si el estado de diferente de vacio
						if( $datos["domicilios"][0]["descentidadfederativa"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.", ".
																		$datos["domicilios"][0]["descentidadfederativa"];
						}
					}
					else
					{
						//Valida si el estado de diferente de vacio
						if( $datos["domicilios"][0]["descentidadfederativa"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.
																		"\n".$datos["domicilios"][0]["descentidadfederativa"];
						}
					}
				}
				if( $datos["domicilios"][0]["tipodomicilio1"] == 2)
				{
					//Valida si la calle viene diferente de vacio
					if( $datos["domicilios"][0]["calle1"] != "" )
					{
						$domicilioparticular = " ".$datos["domicilios"][0]["calle1"]. " ";
					}
					//Valida si el numero exterior viene diferente de vacio
					if($datos["domicilios"][0]["numeroexterior1"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"Ext: ".$datos["domicilios"][0]["numeroexterior1"];
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior1"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.", ".
																	"Int ".$datos["domicilios"][0]["numerointerior1"];
						}																			
					}
					else
					{
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior1"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.
																	"Int ".$datos["domicilios"][0]["numerointerior1"];
						}	
					}
					//valida si la colonia viene diferente de vacio
					if($datos["domicilios"][0]["desccolonia1"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"\nCol. ".$datos["domicilios"][0]["desccolonia1"];
					}
					//Valida si el codigo postal viene diferente de vacio
					if( $datos["domicilios"][0]["codigopostal1"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"\nC.P. ".$datos["domicilios"][0]["codigopostal"];
					}
					//Valida si el municipio viene diferente de vacio
					if( $datos["domicilios"][0]["descdelegacionmunicipio1"] != "" )
					{
						$domicilioparticular =	$domicilioparticular.
																	"\n".$datos["domicilios"][0]["descdelegacionmunicipio1"];
						//Valida si el estado de diferente de vacio
						if( $datos["domicilios"][0]["descentidadfederativa1"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.", ".
																		$datos["domicilios"][0]["descentidadfederativa1"];
						}
					}
					else
					{
						//Valida si el estado de diferente de vacio
						if( $datos["domicilios"][0]["descentidadfederativa1"] != "" )
						{
							$domicilioparticular =	$domicilioparticular.
																		"\n".$datos["domicilios"][0]["descentidadfederativa1"];
						}
					}		
				}
				//----------------------------------------------------------
					if( $datos["domicilios"][0]["tipodomicilio1"] == 1)
				{
				
					//Valida si la calle viene diferente de vacio
					if( $datos["domicilios"][0]["calle1"] != "" )
					{
						$domiciliolaboral = " ".$datos["domicilios"][0]["calle1"]. " ";
					}
					//Valida si el numero exterior viene diferente de vacio
					if( $datos["domicilios"][0]["numeroexterior1"] != "" )
					{
						$domiciliolaboral =	$domiciliolaboral.
																	"Ext: ".$datos["domicilios"][0]["numeroexterior1"];
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior1"] != "" )
						{
							$domiciliolaboral = $domiciliolaboral.", ".
																"Int ".$datos["domicilios"][0]["numerointerior1"];
						}																			
					}
					else
					{
						//valida si el numero interior es diferente de vacio
						if( $datos["domicilios"][0]["numerointerior1"] != "" )
						{
							$domiciliolaboral =$domiciliolaboral.
																"Int ". $datos["domicilios"][0]["numerointerior1"];
						}	
					}
					//valida si la colonia viene diferente de vacio
					if(  $datos["domicilios"][0]["desccolonia1"] != "" )
					{
						$domiciliolaboral =$domiciliolaboral.
															"\nCol. ". $datos["domicilios"][0]["desccolonia1"];
					}
					//Valida si el codigo postal viene diferente de vacio
					if(  $datos["domicilios"][0]["codigopostal1"] != "" )
					{
						$domiciliolaboral =$domiciliolaboral.
															"\nC.P. ". $datos["domicilios"][0]["codigopostal1"];
					}
					//Valida si el municipio viene diferente de vacio
					if(  $datos["domicilios"][0]["descdelegacionmunicipio1"] != "" )
					{
						$domiciliolaboral = $domiciliolaboral.
															"\n". $datos["domicilios"][0]["descdelegacionmunicipio1"];
						//Valida si el estado de diferente de vacio
						if(  $datos["domicilios"][0]["descentidadfederativa1"] != "" )
						{
							$domiciliolaboral =$domiciliolaboral.", ".
																$datos["domicilios"][0]["descentidadfederativa1"];
						}
					}
					else
					{
								//Valida si el estado de diferente de vacio
						if(  $datos["domicilios"][0]["descentidadfederativa1"] != "" )
						{
							$domiciliolaboral = $domiciliolaboral.
																"\n". $datos["domicilios"][0]["descentidadfederativa1"];
						}
					}
				}
				//----------------------------------------------------------
				//Imprime la etiqueta domicilios
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "B. Domicilio (s)";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');				
				//Imprime el valor de la etiqueta Domicilio particular
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Particular: ";
				$pdf->Cell( 16, $iAltoRen - 2, $sTexto, $iBorde, 0, '');
				//Imprime el valor del domicilio particular
				$pdf->SetXY($iColDatos, $iRen);	
				//Se comento para no aplicar el utf8_decode cuando se formatea la cadena
				$sTexto = obtenerCadenaFormateada($domicilioparticular);
				$pdf->MultiCell( 174, $iAltoRen - 2, $sTexto, $iBorde, 'J');
				//Imprime el valor de la etiqueta Domicilio particular
				$iRen += $iAltoRen * 3;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Laboral: ";
				$pdf->Cell( 16, $iAltoRen - 2, $sTexto, $iBorde, 0, '');
				//Imprime el valor del domicilio laboral
				$pdf->SetXY($iColDatos, $iRen);
				//Se comento para no aplicar el utf8_decode cuando se formatea la cadena
				$sTexto = obtenerCadenaFormateada($domiciliolaboral);
				$pdf->MultiCell( 174, $iAltoRen - 2, $sTexto, $iBorde, 'J');

				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion datos de contacto
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				clog::escribirLog("DATOS DE CONTACTO");
				$iRen += $iAltoRen * 3;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "C. Datos de Contacto";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta Telefono 1
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = utf8_decode("Teléfono 1: ");
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del telefono 1
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = $datos["ModTrabajador"][0]["telefono01"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta Correo electronico
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = utf8_decode("Correo electrónico: ");
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//--------------------------------------------------------------------
				$iLongitud = strlen($datos["ModTrabajador"][0]["correoelectronico"]);
				$pdf->SetXY($iColDatos2, $iRen);
				if($iLongitud >= 25)
				{
					$bBandera = 1;
					$sPrimerCadena = substr($datos["ModTrabajador"][0]["correoelectronico"],1,25);
					$sTexto = $sPrimerCadena;
					$iPosicionY = $iRen + 3;
				}
				else
				{
					$sTexto = $datos["ModTrabajador"][0]["correoelectronico"];
				}
				$pdf->Cell(90, $iAltoRen, $sTexto, $iBorde, 0, '');
				
				$pdf->SetXY($iColDatos2, $iPosicionY);
				if($bBandera == 1)
				{
					$sSegundaCadena = substr($datos["ModTrabajador"][0]["correoelectronico"],26,$iLongitud-25);
				}
				$pdf->Cell(90, $iAltoRen, $sSegundaCadena, $iBorde, 0, '');

				
				//Imprime la etiqueta de Telefono2
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = utf8_decode("Teléfono 2: ");
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del telefono 2
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = $datos["ModTrabajador"][0]["telefono02"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');


				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion actividad economica, nivel de estudios ocupacion
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				clog::escribirLog("ACTIVIDAD,ESTUDIO,OCUPACION");
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = utf8_decode("D. Actividad económica, nivel de estudios, ocupación.");
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Actividad Económica:
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = utf8_decode("Actividad Económica: ");
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de actividad economica
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["descactividadgironegocio"]);
				$pdf->Cell( 150, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Nivel estudio
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Nivel de Estudios: ";
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de nivel de estudios
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["descnivelestudio"]);
				$pdf->Cell( 150, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Ocupacion
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = utf8_decode("Ocupación: ");
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de ocupacion
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = obtenerCadenaFormateada($datos["ModTrabajador"][0]["descocupacion"]);
				$pdf->MultiCell( 150, $iAltoRen - 2, $sTexto, $iBorde, 'J');
				
				
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion de beneficiarios
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				clog::escribirLog("BENEFICIARIOS");
				$iRen += $iAltoRen * 2;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "E. Beneficiarios Sustitutos";
				$pdf->Cell( 50, $iAltoRen, $sTexto, $iBorde, 0, '');				
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Paterno: ";
				$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 33, $iRen);
				$sTexto = "Apellido Materno: ";
				$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 66, $iRen);
				$sTexto = "Nombre (s): ";
				$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 105, $iRen);
				$sTexto = "Parentesco: ";
				$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 130, $iRen);
				$sTexto = "CURP: ";
				$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 165, $iRen);
				$sTexto = "Porcentaje: ";
				$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');

				//Itera el total de beneficiarios que se tienen
				
			//	for( $i = 0; $i < 5; $i++)
				//{
					/*BENEFICIARIO 1*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidopaterno"])?$datos["beneficiarios"][0]["apellidopaterno"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 33, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidomaterno"])?$datos["beneficiarios"][0]["apellidomaterno"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 66, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["nombre"])?$datos["beneficiarios"][0]["nombre"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 105, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["descparentesco"])?$datos["beneficiarios"][0]["descparentesco"]:'');
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 130, $iRen);
					$sTexto = isset($datos["beneficiarios"][0]["curp"])?$datos["beneficiarios"][0]["curp"]:'';					
					$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 165, $iRen);
					if($datos["beneficiarios"][0]["porcentaje"] != '0' && $datos["beneficiarios"][0]["porcentaje"] != 0)
						{$sTexto = isset($datos["beneficiarios"][0]["porcentaje"])?$datos["beneficiarios"][0]["porcentaje"]."%":'';}
					$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');
					
					/*BENEFICIARIO 2*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidopaterno1"])?$datos["beneficiarios"][0]["apellidopaterno1"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 33, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidomaterno1"])?$datos["beneficiarios"][0]["apellidomaterno1"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 66, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["nombre1"])?$datos["beneficiarios"][0]["nombre1"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 105, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["descparentesco1"])?$datos["beneficiarios"][0]["descparentesco1"]:'');
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 130, $iRen);
					$sTexto = isset($datos["beneficiarios"][0]["curp"])?$datos["beneficiarios"][0]["curp1"]:'';					
					$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 165, $iRen);
					if($datos["beneficiarios"][0]["porcentaje1"] != '0' && $datos["beneficiarios"][0]["porcentaje1"] != 0)
						{$sTexto = isset($datos["beneficiarios"][0]["porcentaje1"])?$datos["beneficiarios"][0]["porcentaje1"]."%":'';}
					$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');
					
					/*BENEFICIARIO 3*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidopaterno2"])?$datos["beneficiarios"][0]["apellidopaterno2"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 33, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidomaterno2"])?$datos["beneficiarios"][0]["apellidomaterno2"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 66, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["nombre2"])?$datos["beneficiarios"][0]["nombre2"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 105, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["descparentesco2"])?$datos["beneficiarios"][0]["descparentesco2"]:'');
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 130, $iRen);
					$sTexto = isset($datos["beneficiarios"][0]["curp2"])?$datos["beneficiarios"][0]["curp2"]:'';					
					$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 165, $iRen);
					if($datos["beneficiarios"][0]["porcentaje2"] != '0' && $datos["beneficiarios"][0]["porcentaje2"] != 0)
						{$sTexto = isset($datos["beneficiarios"][0]["porcentaje2"])?$datos["beneficiarios"][0]["porcentaje2"]."%":'';}
					$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');
					
					/*BENEFICIARIO 4*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidopaterno3"])?$datos["beneficiarios"][0]["apellidopaterno3"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 33, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidomaterno3"])?$datos["beneficiarios"][0]["apellidomaterno3"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 66, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["nombre3"])?$datos["beneficiarios"][0]["nombre3"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 105, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["descparentesco3"])?$datos["beneficiarios"][0]["descparentesco3"]:'');
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 130, $iRen);
					$sTexto = isset($datos["beneficiarios"][0]["curp3"])?$datos["beneficiarios"][0]["curp3"]:'';					
					$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 165, $iRen);
					if($datos["beneficiarios"][0]["porcentaje3"] != '0' && $datos["beneficiarios"][0]["porcentaje3"] != 0)
						{$sTexto = isset($datos["beneficiarios"][0]["porcentaje3"])?$datos["beneficiarios"][0]["porcentaje3"]."%":'';}
					$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');
					
					/*BENEFICIARIO 5*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidopaterno4"])?$datos["beneficiarios"][0]["apellidopaterno4"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 33, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["apellidomaterno4"])?$datos["beneficiarios"][0]["apellidomaterno4"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 66, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["nombre4"])?$datos["beneficiarios"][0]["nombre4"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 105, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["beneficiarios"][0]["descparentesco4"])?$datos["beneficiarios"][0]["descparentesco4"]:'');
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 130, $iRen);
					$sTexto = isset($datos["beneficiarios"][0]["curp4"])?$datos["beneficiarios"][0]["curp4"]:'';					
					$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 165, $iRen);
					if($datos["beneficiarios"][0]["porcentaje4"] != '0' && $datos["beneficiarios"][0]["porcentaje4"] != 0)
						{$sTexto = isset($datos["beneficiarios"][0]["porcentaje4"])?$datos["beneficiarios"][0]["porcentaje4"]."%":'';}
					$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');
				//}

				//Imprime la seccion de referencias
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "F. Referencias Personales";
				$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');				
				//Imprime la etiqueta de apellido paterno
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol + 18, $iRen);
				$sTexto = "Apellido Paterno: ";
				$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de apellido materno
				$pdf->SetXY($iCol + 51, $iRen);
				$sTexto = "Apellido Materno: ";
				$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de nombres
				$pdf->SetXY($iCol + 84, $iRen);
				$sTexto = "Nombre (s): ";
				$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de CURP
				$pdf->SetXY($iCol + 118, $iRen);
				$sTexto = "CURP: ";
				$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de telefono
				$pdf->SetXY($iCol + 153, $iRen);
				$sTexto = utf8_decode("Teléfono: ");
				$pdf->Cell( 18, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de parentesco
				$pdf->SetXY($iCol + 171, $iRen);
				$sTexto = "Parentesco: ";
				$pdf->Cell( 25, $iAltoRen, $sTexto, $iBorde, 0, '');
				
				//Agrega las referencias
				//for($i = 0; $i < 2; $i++)
				//{
				/*BENEFICIARIO 1*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = "Referencia ". 1 .":";
					$pdf->Cell( 18, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Apellido paterno
					$pdf->SetXY($iCol + 18, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["apellidopaterno"])?$datos["referencias"][0]["apellidopaterno"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Apellido materno
					$pdf->SetXY($iCol + 51, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["apellidomaterno"])?$datos["referencias"][0]["apellidomaterno"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Nombres
					$pdf->SetXY($iCol + 84, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["nombre"])?$datos["referencias"][0]["nombre"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//CURP
					$pdf->SetXY($iCol + 118, $iRen);
					$sTexto = isset($datos["referencias"][0]["curp"])?$datos["referencias"][0]["curp"]:'';
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Telefono
					$pdf->SetXY($iCol + 153, $iRen);
					$sTexto = isset($datos["referencias"][0]["telefono"])?$datos["referencias"][0]["telefono"]:'';
					$pdf->Cell( 18, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Parentesco
					$pdf->SetXY($iCol + 171, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["descparentesco"])?$datos["referencias"][0]["descparentesco"]:'');
					$pdf->Cell( 25, $iAltoRen, $sTexto, $iBorde, 0, '');
					
					/*BENEFICIARIO 2*/
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = "Referencia ". 2 .":";
					$pdf->Cell( 18, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Apellido paterno
					$pdf->SetXY($iCol + 18, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["apellidopaterno1"])?$datos["referencias"][0]["apellidopaterno1"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Apellido materno
					$pdf->SetXY($iCol + 51, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["apellidomaterno1"])?$datos["referencias"][0]["apellidomaterno1"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Nombres
					$pdf->SetXY($iCol + 84, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["nombre1"])?$datos["referencias"][0]["nombre1"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//CURP
					$pdf->SetXY($iCol + 118, $iRen);
					$sTexto = isset($datos["referencias"][0]["curp1"])?$datos["referencias"][0]["curp1"]:'';
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');					
					//Telefono
					$pdf->SetXY($iCol + 153, $iRen);
					$sTexto = isset($datos["referencias"][0]["telefono1"])?$datos["referencias"][0]["telefono1"]:'';
					$pdf->Cell( 18, $iAltoRen, $sTexto, $iBorde, 0, '');
					//Parentesco
					$pdf->SetXY($iCol + 171, $iRen);
					$sTexto = obtenerCadenaFormateada(isset($datos["referencias"][0]["descparentesco1"])?$datos["referencias"][0]["descparentesco1"]:'');
					$pdf->Cell( 25, $iAltoRen, $sTexto, $iBorde, 0, '');
				//}

				//Imprime la seccion de la firma del promotor y trabajador
				$iRen += $iAltoRen * 5;
				$pdf->Line( $iCol + 10, $iRen, $iCol + 85, $iRen);
				$pdf->Line( $iCol + 105, $iRen, $iCol + 180, $iRen);				
				//Imprime el nombre del funcionario o promotor
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				//Se comento para no aplicar el utf8_decode cuando se formatea la cadena
				//$sTexto = obtenerCadenaFormateadaFuncionario($datos["ModTrabajador"][0]["nombrefuncionario"]);
				
				$sTexto = $datos["ModTrabajador"][0]["nombrefuncionario"];
				
				//Itera la cadena para cambiar la letra Ñ
				for($i = 1; $i < strlen($sTexto); $i++ )
				{
					if( $sTexto[$i] == 'Ñ' ||  $sTexto[$i] == 'ñ')
						$sTexto[$i] = utf8_encode( 'Ñ' );
					
				}
				
				
				$pdf->Cell( 95, $iAltoRen, utf8_decode($sTexto), $iBorde, 0, 'C');
				//Imprime el nombre del trabajador
				$pdf->SetXY($iCol + 95, $iRen);
				$sTexto = obtenerCadenaFormateada($nombretrabajador);
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');

				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen + 4);
				$sTexto = "Firma del Funcionario";
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');
				$pdf->SetXY($iCol + 95, $iRen + 4);
				$sTexto = "Firma del Trabajador";
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');
					

				//Imprime el pie de pagina
				$iRen += 18;
				$pdf->SetFont('Arial','',6.5);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = utf8_decode("AFORE COPPEL S.A. DE C.V. AVENIDA INSURGENTES ".
						"SUR No. 553 Piso 6. 603 Col. ESCANDON Delegación ".
						"MIGUEL HIDALGO D.F. C.P. 11800");
				$pdf->Cell( 190, $iAltoRen, $sTexto, 0, 0, 'C');

				$CMetodosExpedienteServicio = new CMetodosExpedienteServicio();
				
				//$sNombreDoc = $datos["ModTrabajador"][0]["folio"]."_".$iPidActual."_SolicitudExpIden".trim($sCurp).".pdf";
				$sNombreDoc = $datos["ModTrabajador"][0]["folio"]."_".$iPidActual."_SolicitudExpIden".trim($datos["ModTrabajador"][0]["curp"]).".pdf";
				//$sNombreDoc2 = $datos["ModTrabajador"][0]["folio"]."_".$iPidActual."_SolicitudExpIden".trim($sCurp)."_TEMP.pdf";
				$sNombreDoc2 = $datos["ModTrabajador"][0]["folio"]."_".$iPidActual."_SolicitudExpIden".trim($datos["ModTrabajador"][0]["curp"])."_TEMP.pdf";
				$sRutaPdf = "/sysx/progs/web/salida/formatosservicios/".$sNombreDoc;
				//$sRutaPdf = "/sysx/progs/web/salida/formatosservicios/".$sNombreDoc;
				//$sRutaPdfTemp = "../../salida/formatosservicios/".$sNombreDoc2;
				$sRutaPdfTemp = "/sysx/progs/web/salida/formatosservicios/".$sNombreDoc2;
				
				//Agregamos la ruta de las firmas
				$sRutaFirmaTrab = "/sysx/progs/web/entrada/firmas/SEI0_".$folio."-S_FTRAB.JPG";
				$sRutaFirmaProm = "/sysx/progs/web/entrada/firmas/SEI0_".$folio."-S_FPROM.JPG";
				
				$sRutaImagenFinal = $sNss."_". $datos["ModTrabajador"][0]["folio"].".JPG";

				$aArregloResp["iRetorno"] = 26;
				//Graba el archivo en el servidor
				$pdf->Output($sRutaPdf);
				//Se hace una copia del documento creado
				copy($sRutaPdf, $sRutaPdfTemp);
				
				chmod($sRutaPdf, 0777);
				$aArregloResp["iRetorno"] = 27;
				clog::escribirLog("Antes De Las Imagenes De Las Firmas Con El PDF");
				
				//Se crea nuevamente un objeto de la clase FPDI
				$pdf = new FPDI();
				//$pageCount = $pdf->setSourceFile('../../salida/formatosservicios/'.$sNombreDoc);
				$pageCount = $pdf->setSourceFile('/sysx/progs/web/salida/formatosservicios/'.$sNombreDoc);
				$tplIdx = $pdf->importPage(1);
				$pdf->addPage();
				$pdf->useTemplate($tplIdx, null, null, 0, 0, true);
				
				$pdf->Image($sRutaFirmaTrab, 126, 227, 50, 20, 'JPG');
				$pdf->Image($sRutaFirmaProm, 30, 227, 50, 20, 'JPG');
				CLog::escribirLog("Se agregan las firmas al PDF");
				
				//SE GUARDA PDF Y SE MUESTRA EN EL NAVEGADOR
				$pdf->Output($sRutaPdf, 'F');
				clog::escribirLog("Se guarda y se muestra PDF(Formato de EI Servicio) en el navegador");
					
				//}
				$aArregloResp["iRetorno"] = 28;
				unset($aRespUnionImg);
				//SE ACTUALIZA EN LA TABLA STMPPUBLICARIMAGEN
				$aRespUnionImg = $CMetodosExpedienteServicio->actualizarpublicarimagenservicioei($folio, 1);
				$aArregloResp["iRetorno"] = 29;
				$aArregloResp["codigoRespuesta"] = OK__;
				$aArregloResp["rutapdf"] = '../../salida/formatosservicios/'.$sNombreDoc;
				
			}
			catch(Exception $e)
			{

				//Enviamos el error al usuario
				$aArregloResp["codigoRespuesta"] = ERR__;
				$aArregloResp["descripcion"] 	 = "Ocurri&oacute; un error al realizar la impresi&oacute;n, por favor, reporte a Mesa de Ayuda.";

				//Enviamos el error al log
				CLog::escribirLog('[' . __FILE__ . '] FPDF Error : '. $e->getMessage());
				
			}
		}
		else
		{
			$aArregloResp["codigoRespuesta"] = ERR__;
			$aArregloResp["descripcion"] 	 = "Las consults no se realizaron correctamente";
		}
		
		return $aArregloResp;	
	}

	function obtenerCadenaFormateada($cCadena)
	{
		$cCadenaAuxiliar = "";
		$cLetraAnterior = "";

		//decodifica la cadena a utf8 y cambia las letras a minuscualas y la primer
		//letra de cada parala a mayuscula
		$cCadenaAuxiliar = ucwords(strtolower(utf8_decode($cCadena)));
		
		//Itera la cadena para cambiar la letras que tienen acento
		for($i = 1; $i < strlen($cCadenaAuxiliar); $i++ )
		{

			//Valida si la letra anterior no es un espacio
			if( $cCadenaAuxiliar[$i - 1] != ' ' )			
			{
				//En caso de no ser espacio valida si tiene 
				//que reemplazar la letra por minuscula
				switch ($cCadenaAuxiliar[$i]) {
					case 'Á':
						$cCadenaAuxiliar[$i] = 'á';
					break;
					case 'É':
						$cCadenaAuxiliar[$i] = 'é';
					break;
					case 'Í':
						$cCadenaAuxiliar[$i] = 'í';
					break;
					case 'Ó':
						$cCadenaAuxiliar[$i] = 'ó';
					break;
					case 'Ú':
						$cCadenaAuxiliar[$i] = 'ú';
					break;
					case 'Ñ':
						$cCadenaAuxiliar[$i] = 'ñ';
					break;
									
				}
			}
		}

		$cCadena = $cCadenaAuxiliar;
		//retorna la cadena formateada
		return $cCadena;
	}
	
	function obtenerCadenaFormateadaFuncionario($cCadena)
	{
		$cCadenaAuxiliar = "";
		$cLetraAnterior = "";

		//decodifica la cadena a utf8 y cambia las letras a minuscualas y la primer
		//letra de cada parala a mayuscula
		$cCadenaAuxiliar = ucwords(strtolower($cCadena));
		
		//Itera la cadena para cambiar la letras que tienen acento
		for($i = 1; $i < strlen($cCadenaAuxiliar); $i++ )
		{

			//Valida si la letra anterior no es un espacio
			if( $cCadenaAuxiliar[$i - 1] != ' ' )			
			{
				//En caso de no ser espacio valida si tiene 
				//que reemplazar la letra por minuscula
				switch ($cCadenaAuxiliar[$i]) {
					case 'Á':
						$cCadenaAuxiliar[$i] = 'á';
					break;

					case 'É':
						$cCadenaAuxiliar[$i] = 'é';
					break;
					case 'Í':
						$cCadenaAuxiliar[$i] = 'í';
					break;
					case 'Ó':
						$cCadenaAuxiliar[$i] = 'ó';
					break;

					case 'Ú':
						$cCadenaAuxiliar[$i] = 'ú';
					break;

					case 'Ñ':
						$cCadenaAuxiliar[$i] = 'ñ';
					break;
									
				}
			}
		}

		$cCadena = $cCadenaAuxiliar;
		//retorna la cadena formateada
		return $cCadena;
	}
	
	
	function obtenerCadenaFormateadaDomicilio($cCadena)
	{
		$cCadenaAuxiliar = "";
		$cCadenaAuxiliar2 = "";
		
		$cCadenaAuxiliar = ucwords(strtolower(utf8_decode($cCadena)));
		
		//Itera la cadena para cambiar la letras que tienen acento
		for($i = 1; $i < strlen($cCadenaAuxiliar); $i++ )
		{

			//Valida si la letra anterior no es un espacio
			if( $cCadenaAuxiliar[$i - 1] != ' ' )			
			{
				//En caso de no ser espacio valida si tiene 
				//que reemplazar la letra por minuscula
				switch ($cCadenaAuxiliar[$i]) {
					case 'Á':
						$cCadenaAuxiliar[$i] = 'á';
					break;
					case 'É':
						$cCadenaAuxiliar[$i] = 'é';
					break;
					case 'Í':
						$cCadenaAuxiliar[$i] = 'í';
					break;
					case 'Ó':
						$cCadenaAuxiliar[$i] = 'ó';
					break;
					case 'Ú':
						$cCadenaAuxiliar[$i] = 'ú';
					break;
					case 'Ñ':
						$cCadenaAuxiliar[$i] = 'ñ';
					break;
									
				}
			}
		}

		$cCadena = urldecode($cCadenaAuxiliar);
		//retorna la cadena formateada
		return $cCadena;
	}
	
	function obtIpTienda()
	{
		$ipCliente = "";
		
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
			$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR']; 
		else
			$ipCliente = $_SERVER['REMOTE_ADDR'];

		return $ipCliente;
	} 
?>