<?php
include_once("global.php");
include_once("fpdf.php");
include_once("CLog.php");
include_once ('Clases/CMetodoGeneral.php');
include_once ('Clases/CServicioBusTramites.php');
include_once ('Clases/Capirestexpedienteidentificacion.php');

$opcionWs['location'] = leerParametrosWs();
$opcionWs['cache_wsdl'] = WSDL_CACHE_NONE;

//Clase utilizada para la administracion del expediente de identificacion
class CExpedienteIdentificacion
{
	
	//Funcion utilizada para desmarcar la opcion de "Registro de un trabajador fallecido" cuando el solicitante sea "Beneficiario, Curador, Apoderado legal".
	public static function obtenerTipoConstancia($folio){

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestexpedienteidentificacion();

		$datos = new stdClass();

		$arrApi = array("cFolio"=>$folio);

		try
		{
			$resultAPI = $objAPI->consumirApi('obtenerExcepcionSolicitante',$arrApi);
			
			if($resultAPI)
			{
				foreach ($resultAPI['registros'] as $reg) {
					$excepcion = $reg['iexcepcion'];
					$tiposolicitante = $reg['itiposolicitante'];
				}
		
				if ($tiposolicitante == 1) {
					$datos->codigoRespuestaEx = 1;
				}
				else if($tiposolicitante == 2){
					$datos->codigoRespuestaEx = 2;
				}
				else{
					$datos->codigoRespuestaEx = ERR__;
				}
			}
			else
			{
				$datos['estatus'] = ERR__;
				$datos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				throw new Exception("constanciaafiliacion.php\tvalidarTelefonoListaNegra"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}

			$resultAPI2 = $objAPI->consumirApi('fnobtenerconstanciatrabajadorfallecido',$arrApi);

			if($resultAPI2)
			{
				foreach ($resultAPI2['registros'] as $reg) {
					$iTipoConstancia = $reg['fnobtenerconstanciatrabajadorfallecido'];
				}
		
				if ($iTipoConstancia > 0) {
					$datos->codigoRespuesta = OK__;
				}
				else{
					$datos->codigoRespuesta = ERR__;
				}
			}
			else
			{
				$datos['estatus'] = ERR__;
				$datos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				throw new Exception("constanciaafiliacion.php\tvalidarTelefonoListaNegra"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $datos;

	}
	
	//Funcion utilizada para guardar en base de datos la informacin del expediente de identificacin
	public static function guardarExpedienteIdentificacion($expediente)
	{
		CLog::escribirLog( '[' . __FILE__ . '] Inicia guardado del Expediente de Identificacin asociado al folio '.$expediente["cFolio"] );
		//Objetos de salida que contienen la informacion y en caso de error su descripcion
		$datos = new stdClass();
		$datosLlamada = new stdClass();
		$cSql = null;
		
		//Abre conexion a la base de datos
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);			
		
		//valida el estado de la conexion
		if ($cnxOdbc) 
		{
			CLog::escribirLog( '[' . __FILE__ . '] Conexin abierta. ');
			$cFolio = isset($expediente['cFolio']) ? $expediente['cFolio'] : '';
			$dtFechaServicio = isset($expediente['dtFechaServicio']) ? $expediente['dtFechaServicio'] : '1900-01-01';
			$iTipoServicio = isset($expediente['iTipoServicio']) ? $expediente['iTipoServicio'] : -1;
			$iEstatus = isset($expediente["iEstatus"]) ? $expediente["iEstatus"] : 0;
			$cNss = isset($expediente['cNss']) ? $expediente['cNss'] : '';
            $cCurp = isset($expediente['cCurp']) ? $expediente['cCurp'] : '';
            $cRfc = isset($expediente['cRfc']) ? $expediente['cRfc'] : '';
            $cApellidoPaterno = isset($expediente['cApellidoPaterno']) ? $expediente['cApellidoPaterno'] : '';
            $cApellidoMaterno = isset($expediente['cApellidoMaterno']) ? $expediente['cApellidoMaterno'] : '';
            $cNombres = isset($expediente['cNombres']) ? $expediente['cNombres'] : '';
            $dtFechaNacimiento = isset($expediente['dtFechaNacimiento']) ? $expediente['dtFechaNacimiento'] : '1900-01-01';
            $iGenero = isset($expediente['cGenero']) ? $expediente['cGenero'] : 0;
            $iNivelEstudio = isset($expediente['iNivelEstudio']) ? $expediente['iNivelEstudio'] : 0;
            $iOcupacion = isset($expediente['iOcupacion']) ? $expediente['iOcupacion'] : 0;
            $iActividadGiroNegocio = isset($expediente['iActividadGiroNegocio']) ? $expediente['iActividadGiroNegocio'] : 0;
            $cTelefono1 = isset($expediente['cTelefono1']) ? $expediente['cTelefono1'] : '';
            $cTelefono2 = isset($expediente['cTelefono2']) ? $expediente['cTelefono2'] : '';
            $islcTel1 = (isset($expediente['islcTel1']) == true) ? $expediente['islcTel1'] : 0;            
            $islcTel2 = (isset($expediente['islcTel2']) == true) ? $expediente['islcTel2'] : 0;
            $cCorreo = isset($expediente['cCorreo']) ? $expediente['cCorreo'] : '';
            $islcCorreo = isset($expediente['islcCorreo']) ? $expediente['islcCorreo'] : 0;
          	$iEntidadNacimiento = isset($expediente['iEntidadNacimiento']) ? $expediente['iEntidadNacimiento'] : 0;
          	$iNacionalidad = isset($expediente['cNacionalidad']) ? $expediente['cNacionalidad'] : 0;
          	$dtFechaVigenciaIdOficial = isset($expediente['dtFechaVigenciaIdOficial']) ?
          	                            $expediente['dtFechaVigenciaIdOficial'] : '1900-01-01';          	                            
			$dtFechaComprobanteDomicilio = isset($expediente['dtFechaComprobanteDomicilio']) ?
          	                               $expediente['dtFechaComprobanteDomicilio'] : '1900-01-01';
          	$iExisteEi = isset($expediente['iExisteEi']) ? $expediente['iExisteEi'] : -1;
          	$iPromotor = isset($expediente['iPromotor']) ? $expediente['iPromotor'] : 0;
          	$iEsFinado = isset($expediente['chBoxTrabajadorfallecido']) ? $expediente['chBoxTrabajadorfallecido'] : -1;
          	$iSlcComprobanteDomicilio = isset($expediente['iSlcComprobanteDomicilio']) ? $expediente['iSlcComprobanteDomicilio'] : -1;
            $iSlcIndetificacionOficial = isset($expediente['iSlcIndetificacionOficial']) ? $expediente['iSlcIndetificacionOficial'] : -1;
             
             if ($iTipoServicio != 26 && $iTipoServicio != 27 && $iTipoServicio != 33) {
             	$cFolio = $cFolio."-S";
             	if ($iExisteEi == 0) {
             		$datosLlamada = self::guadarLlamadaCat($cFolio,$iTipoServicio);
             	}
             }
			
			//Se decodifican los nombres y apellidos del trabajador para que se guarden  en BD.
			$cApellidoPaterno = utf8_decode($cApellidoPaterno);
            $cApellidoMaterno = utf8_decode($cApellidoMaterno);
            $cNombres = utf8_decode($cNombres);

			//prepara y ejecuta la consulta en la base de datos
            //Transaccionamos para asegurar que se guarde o toda la informacin o nada			
			
			$cSql = "SELECT 1 FROM fnguardareitrabajador ('$cFolio',$iEstatus::smallint,'$cNss','$cCurp','$cRfc','$cNombres',".
					"'$cApellidoPaterno','$cApellidoMaterno','$dtFechaNacimiento',$iGenero::smallint,$iOcupacion::smallint,".
					"$iActividadGiroNegocio::smallint,$iNivelEstudio::smallint,'$cTelefono1',$islcTel1::smallint,'$cTelefono2',".
					"$islcTel2::smallint,'$cCorreo',$iNacionalidad::smallint,$iEntidadNacimiento::smallint,'$dtFechaVigenciaIdOficial'::date,".
					"'$dtFechaComprobanteDomicilio'::date,$iPromotor, $iEsFinado::smallint,$iTipoServicio, '$dtFechaServicio',$iSlcIndetificacionOficial::smallint,$iSlcComprobanteDomicilio::smallint); ";
			
			$domicilio = $expediente['domicilio'];
			
			$cCodigoPostal = isset($expediente['domicilio']['cCodigoPostal']) ? $expediente['domicilio']['cCodigoPostal'] : '';
			$iTipoDomicilio = 2;
			$cPais = isset($expediente['domicilio']['cPais']) ? $expediente['domicilio']['cPais'] : '';
			$iEntidadFederativa = isset($expediente['domicilio']['iEntidadFederativa']) ? $expediente['domicilio']['iEntidadFederativa'] : 0;
			$iMunicipio = isset($expediente['domicilio']['iMunicipio']) ? $expediente['domicilio']['iMunicipio'] : 0;
			$iColonia = isset($expediente['domicilio']['iColonia']) ? $expediente['domicilio']['iColonia'] : 0;
			$cCalle = isset($expediente['domicilio']['cCalle']) ? $expediente['domicilio']['cCalle'] : '';
			$cNumExterior = isset($expediente['domicilio']['cNumExterior']) ? $expediente['domicilio']['cNumExterior'] : '';
			$cNumInterior = isset($expediente['domicilio']['cNumInterior']) ? $expediente['domicilio']['cNumInterior'] : '';
			$iCiudad = isset($expediente['domicilio']['iCiudad']) ? $expediente['domicilio']['iCiudad'] : 0;

			$cSql .= "SELECT 1 FROM fnguardareidomicilio('$cFolio',$iTipoDomicilio::smallint,'$cCodigoPostal','$cPais',".
					 "$iEntidadFederativa,$iMunicipio,$iColonia,".
					 "'$cCalle','$cNumInterior','$cNumExterior',$iCiudad,'$cNss','$cCurp'); ";
						
			//$domicilioLaboral = $expediente["domicilioLaboral"];

			$cCodigoPostalLaboral = isset($expediente['domicilioLaboral']['cCodigoPostalLaboral']) ? $expediente['domicilioLaboral']['cCodigoPostalLaboral'] : '';
			$iTipoDomicilio = 1;
			$cPaisLaboral = isset($expediente['domicilioLaboral']['cPaisLaboral']) ? $expediente['domicilioLaboral']['cPaisLaboral'] : '';
			$iEntidadFederativaLaboral = isset($expediente['domicilioLaboral']['iEntidadFederativaLaboral']) ? $expediente['domicilioLaboral']['iEntidadFederativaLaboral'] : 0;
			$iMunicipioLaboral = isset($expediente['domicilioLaboral']['iMunicipioLaboral']) ? $expediente['domicilioLaboral']['iMunicipioLaboral'] : 0;
			$iColoniaLaboral = isset($expediente['domicilioLaboral']['iColoniaLaboral']) ? $expediente['domicilioLaboral']['iColoniaLaboral'] : 0;
			$cCalleLaboral = isset($expediente['domicilioLaboral']['cCalleLaboral']) ? $expediente['domicilioLaboral']['cCalleLaboral'] : '';
			$cNumExteriorLaboral = isset($expediente['domicilioLaboral']['cNumExteriorLaboral']) ? $expediente['domicilioLaboral']['cNumExteriorLaboral'] : '';
			$cNumInteriorLaboral = isset($expediente['domicilioLaboral']['cNumInteriorLaboral']) ? $expediente['domicilioLaboral']['cNumInteriorLaboral'] : '';
			$iCiudadLaboral = isset($expediente['domicilioLaboral']['iCiudadLaboral']) ? $expediente['domicilioLaboral']['iCiudadLaboral'] : 0;

			if( $iColoniaLaboral > 0 && $cCodigoPostalLaboral != '' && $iTipoDomicilio == 1)
			{
				$cSql .= "SELECT 1 FROM fnguardareidomicilio('$cFolio',$iTipoDomicilio::smallint,'$cCodigoPostalLaboral','$cPais',".
									"$iEntidadFederativaLaboral::smallint,$iMunicipioLaboral,$iColoniaLaboral,'$cCalleLaboral',".
									"'$cNumInteriorLaboral','$cNumExteriorLaboral',$iCiudadLaboral,'$cNss','$cCurp'); ";			
			}
			

			if (isset($expediente['beneficiarios']) && $expediente['beneficiarios'] != "") {
				if (count($expediente['beneficiarios']) > 0) {
					for ($i=0; $i < count($expediente['beneficiarios']); $i++) { 
						$cPaternoBeneficiario = isset($expediente['beneficiarios'][$i]['paterno']) ? $expediente['beneficiarios'][$i]['paterno'] : '';
		                $cMaternoBeneficiario = isset($expediente['beneficiarios'][$i]['materno']) ? $expediente['beneficiarios'][$i]['materno'] : '';
		                $cNombresBeneficiario = isset($expediente['beneficiarios'][$i]['nombres']) ? $expediente['beneficiarios'][$i]['nombres'] : '';
		                $cCurpBeneficiario = isset($expediente['beneficiarios'][$i]['curp']) ? $expediente['beneficiarios'][$i]['curp'] : '';
		                $iIdparentescoBeneficiario = isset($expediente['beneficiarios'][$i]['idparentesco']) ? $expediente['beneficiarios'][$i]['idparentesco'] : 0;
		                $iPorcentajeBeneficiario = isset($expediente['beneficiarios'][$i]['porcentaje']) ? $expediente['beneficiarios'][$i]['porcentaje'] : 0;
						
						$cPaternoBeneficiario = utf8_decode($cPaternoBeneficiario);
						$cMaternoBeneficiario = utf8_decode($cMaternoBeneficiario);
						$cNombresBeneficiario = utf8_decode($cNombresBeneficiario);
						
		                $cSql .= " SELECT 1 FROM fnguardareibeneficiario('$cFolio','$cCurpBeneficiario','$cNombresBeneficiario',".
		                         "'$cPaternoBeneficiario','$cMaternoBeneficiario',$iIdparentescoBeneficiario::smallint,$iPorcentajeBeneficiario::smallint,'$cNss','$cCurp'); ";
					}
				}
    
			}
        
			$cSql = str_replace("'","''",$cSql);
			CLog::escribirLog($cSql);
            $cSql = " SELECT 1 FROM fnguardarexpedienteidentificacion('$cSql','$cFolio'); ";
            CLog::escribirLog($cSql);
			$resultSet = $cnxOdbc->query($cSql);
			//Valida el resultado de la consulta
			if ($resultSet) 
			{
				CLog::escribirLog( '[' . __FILE__ . '] Expediente guardado con xito. ');
				//indica que todo salio bien
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
			}	
			else
			{
				//Regresa el error al usuario
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al guardar la informaci&oacute;n del expediente ".
									"de identificaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
				//Envia la consulta al log
				CLog::escribirLog($cSql);
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} 
		else 
		{
			//Regresa el error al usuario
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, ".
								"por favor, reporte a Mesa de Ayuda.";
			//Enviamos el error al log
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		$cnxOdbc = null;
		//retorna el objeto con la informacion generada
		return $datos;
	}

	public static function validarExpedientePublicado($cfolio,$iTipoServicio)
	{
		$datos = new stdClass();
		$objApi = new Capirestexpedienteidentificacion();

		if ($iTipoServicio != 26 && $iTipoServicio != 27 && $iTipoServicio != 33)
		{
					$cfolio = $cfolio."-S";
		}

		$arrData = array ('cfolio' => $cfolio);

		CLog::escribirLog("validarExpedientePublicado() FOLIO: ".$cfolio);

		$resultApi = $objApi->consumirApi('validarExpedientePublicado',$arrData);

		if ($resultApi['estatus'] == 1)
		{
			CLog::escribirLog("validarExpedientePublicado() ENTRO RESULSET");
			//Copia la informacion al objeto de salida
			foreach ($resultApi['registros'] as $resultado)
			{
				$datos->estatusExp = $resultado['estatus'];
				CLog::escribirLog("validarExpedientePublicado() RESULTADO: ".$datos->estatusExp);
			}
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			//Regresa el error al usuario
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el estauts del ".
								"expediente, por favor reporte a Mesa de Ayuda.";
			CLog::escribirLog("validarExpedientePublicado() ERROR RESULTSET");
		}

		return $datos;
	}

	//Funcion utilizada para obtener la informacin de los datos del trabajador en el expediente de identificacion
	public static function consultarEIDatosTrabajador($folio)
	{
		//Objetos de salida que contienen la informacion y en caso de error su descripcion
		$datos = new stdClass();
		$modificaciondatosTrabajador = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrData = array ('folio' => $folio);

		$resultApi = $objApi->consumirApi('consultarEIDatosTrabajador',$arrData);


		//Valida el resultado de la consulta
		if ($resultApi['estatus'] == 1)
		{
			//Copia la informacion al objeto de salida
			foreach ($resultApi['registros'] as $resultado)
			{
				$modificaciondatosTrabajador[] = array_map('trim',$resultado);
			}

			//Valida que exista informacion con el folio indicado
			if( count($modificaciondatosTrabajador) > 0)
			{
				$datos->modificaciondatosTrabajador = $modificaciondatosTrabajador;
				//indica que todo salio bien
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
			}
			else
			{
				//Regresa el error al usuario
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "No se encontr&oacute; informaci&oacute;n para el folio proporcionado ";
			}
		}
		else
		{
			//Regresa el error al usuario
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la informaci&oacute;n de ".
								"los datos del trabajador, por favor, reporte a Mesa de Ayuda.";
			//Envia la consulta al log
			CLog::escribirLog($cSql);
			//Enviamos el error al log
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		return $datos;
	}

	//Funcion utilizada para obtener la informacin de los beneficiarios del trabajador en el expediente de identificacion
	public static function consultarEIBeneficiarios($folio)
	{
		//Objetos de salida que contienen la informacion y en caso de error su descripcion
		$datos = new stdClass();
		$beneficiarios = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrData = array ('folio' => $folio);

		$resultApi = $objApi->consumirApi('consultarEIBeneficiarios',$arrData);

		if ($resultApi['estatus'] == 1)
		{
			//Copia la informacion al objeto de salida
			foreach ($resultApi['registros'] as $resultado)
			{
				$beneficiarios[] = array_map('trim',$resultado);
			}
			$datos->beneficiarios = $beneficiarios;
			//indica que todo salio bien
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			//Regresa el error al usuario
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la informaci&oacute;n de ".
								"los beneficiarios del trabajador, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcion utilizada para obtener la informacin de los domicilios del trabajador en el expediente de identificacion
	public static function consultarEIDomicilios($folio)
	{
		//Objetos de salida que contienen la informacion y en caso de error su descripcion
		$datos = new stdClass();
		$domicilios = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrData = array ('folio' => $folio);

		$resultApi = $objApi->consumirApi('consultarEIDomicilios',$arrData);

		if ($resultApi['estatus'] == 1)
		{
			//Copia la informacion al objeto de salida
			foreach ($resultApi['registros'] as $resultado)
			{
				$domicilios[] = array_map('trim',$resultado);
			}
			clog::escribirLog("exito");
			$datos->domicilios = $domicilios;
			//indica que todo salio bien
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			//Regresa el error al usuario
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la informaci&oacute;n de los ".
								"domicilios del trabajador, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcion utilizada para consultar la informacion del expediente de identificacion asociado a un folio
	public static function consultarInformacionEI($folio)
	{
		//Objetos de salida que contienen la informacion y en caso de error su descripcion
		$datos = new stdClass();
		$datos->codigoRespuesta = null;
		$datos->descripcion = null;
		$datos->datosTrabajador = null;
		$datos->modificaciondatosTrabajador = null;
		$datos->beneficiarios = null;
		$datos->domicilios = null;
		//graba en el log el avance del proceso
		CLog::escribirLog( '[' . __FILE__ . '] Inicia consulta de informacion para impresion del EI');
		//Obtiene los datos del trabajador
		$resultado = CExpedienteIdentificacion::obtenerInformacionTrabajador(str_replace("-S","",$folio));
		if( $resultado["codigoRespuesta"] == OK__ )
		{
			CLog::escribirLog( '[' . __FILE__ . '] Obtiene los datos del trabajador');
			$datos->datosTrabajador = $resultado["trabajador"];
			//Obtiene los datos de modificacion de datos del trabajador
			$resultado = CExpedienteIdentificacion::consultarEIDatosTrabajador($folio);
			if( $resultado->codigoRespuesta == OK__ )
			{
				//Graba en el log el avance del proceso
				CLog::escribirLog( '[' . __FILE__ . '] Obtiene la informacion de modificacion de datos del trabajador');
				$datos->modificaciondatosTrabajador = $resultado->modificaciondatosTrabajador;
					//Obtiene los datos de los beneficiarios del trabajador
					$resultado = CExpedienteIdentificacion::consultarEIBeneficiarios($folio);
					if( $resultado->codigoRespuesta == OK__ )
					{
						//Graba en el log el avance del proceso
						CLog::escribirLog( '[' . __FILE__ . '] Obtiene los beneficiarios del trabajador');
						$datos->beneficiarios = $resultado->beneficiarios;
						//Obtiene los datos de los domicilios del trabajador
						$resultado = CExpedienteIdentificacion::consultarEIDomicilios($folio);
						if( $resultado->codigoRespuesta == OK__ )
						{
							//Graba en el log el avance del proceso
							CLog::escribirLog( '[' . __FILE__ . '] Obtiene los domicilios del trabajador');
							$datos->domicilios = $resultado->domicilios;
						}

					}
			}
		}
		//Asigna el codigo final y la descripcion de la respuesta del proceso
		$datos->codigoRespuesta = $resultado->codigoRespuesta;
		$datos->descripcion = $resultado->descripcion;
		//graba en el log el avance del proceso
		if($datos->codigoRespuesta == 1)
		{
			CLog::escribirLog( '[' . __FILE__ . '] Termina consulta de informacion para impresion del EI');
		}

		//retorna el objeto con la informacion generada
		return $datos;
	}

	//Funcion utilizada para generar el documento PDF del expediente de identificacion
	public static function generarPDF($folio)
	{
		//Objetos de salida que contienen la informacion y en caso de error su descripcion
		$datos = new stdClass();
		$pdf = new FPDF();
		$pdf->AliasNbPages();
		$pdf->AddPage();
		$iAltoRen = 6;
		$iAnchoLogo = 45;
		$iCol = 9;
		$iColDatos = 50;
		$iColDatos2 = 135;
		$iRen = 8;
		$iBorde = 0;
		$sTexto = "";

		clog::escribirLog("function generarPDF($folio)"); // 392.1

		//Obtenemos toda la informacin del expediente de identificacion
		$datos = CExpedienteIdentificacion::consultarInformacionEI($folio);

		clog::escribirLog("codigoRespuesta ".$datos->codigoRespuesta);

		//valida que haya obtenido toda la informacion
		if( $datos->codigoRespuesta == OK__ )
		{
			try
			{

				//Indicamos el lenguaje espaol para los datos tipo fecha
				date_default_timezone_set('America/Mazatlan');
				setlocale(LC_TIME, 'spanish');

				//Cambiamos el formato de la fecha de nacimiento a como se requiere para la impresion
				if( $datos->modificaciondatosTrabajador[0]["fechanacimiento"] == "1900-01-01")
				{
					$datos->modificaciondatosTrabajador[0]["fechanacimiento"] = "";
				}
				else
				{
					$datos->modificaciondatosTrabajador[0]["fechanacimiento"] = strftime("%d de %B de %Y", strtotime($datos->modificaciondatosTrabajador[0]["fechanacimiento"]));
				}

				//Cambiamos el formato de la fecha de solicitud a como se requiere para la impresion
				$datos->modificaciondatosTrabajador[0]["fechasolicitud"] = ucwords(strtolower(strftime("%d / %b / %Y", strtotime($datos->modificaciondatosTrabajador[0]["fechasolicitud"]))));


				$datos->codigoRespuesta = ERR__;
				//Agrega el rectangulo de la hoja
				$pdf->Rect(7,7,195,265, 'D');
				$pdf->Rect(7,7,195,15, 'D');
				//Agrega el logo al PDF
				$pdf->Image('../imagenes/logo_aforecoppel.png', $iCol, $iRen, $iAnchoLogo);

				//Agrega el titulo del documento
				$pdf->SetFont('Arial','B',9);
				$pdf->SetXY($iCol + 48, $iRen);
				$sTexto = "Solicitud de Actualizacin de Expediente de Identificacin";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, 'C');
				$pdf->SetXY($iCol + 74, $iRen + 4);
				$sTexto = "y Modificacin de Datos";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, 'C');

				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Agrega el Folios de servicio y la fecha de solicitud
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol + 141, $iRen);
				//Imprime la etiqueta
				$sTexto = "Folio de Servicio:";
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor del folio
				$pdf->SetXY($iCol + 166, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["folio"];
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime la etiqueta
				$pdf->SetXY($iCol + 141, $iRen + 4);
				$sTexto = "Fecha de Solicitud: ";
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor de la fecha de solicitud
				$pdf->SetXY($iCol + 166, $iRen + 4);
				$sTexto = $datos->modificaciondatosTrabajador[0]["fechasolicitud"];
				$pdf->Cell( 48, $iAltoRen, $sTexto, $iBorde, 0, '');
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion de los datos del trabajador
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$iRen += 14;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Datos del Trabajador";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del Apellido paterno
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Paterno: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del apellido paterno
				$pdf->SetXY($iColDatos, $iRen);
				//$sTexto = ucwords(strtolower($datos->datosTrabajador[0]["CAPELLIDOPATERNO"]));
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->datosTrabajador["cApellidoPaterno"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del CURP
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "CURP: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del CURP
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = strtoupper($datos->datosTrabajador["cCurp"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del Apellido Materno
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Materno: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor del apellido materno
				$pdf->SetXY($iColDatos, $iRen);
				//$sTexto = ucwords(strtolower($datos->datosTrabajador[0]["CAPELLIDOMATERNO"]));
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->datosTrabajador["cApellidoMaterno"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del nss
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "NSS: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor del nss
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = $datos->datosTrabajador["cNss"];
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del los Nombres
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Nombre (s): ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor de los nombres
				$pdf->SetXY($iColDatos, $iRen);
				//$sTexto = ucwords(strtolower($datos->datosTrabajador[0]["CNOMBRE"]));
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->datosTrabajador["cNombre"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');

				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion modificacion de datos
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				CLog::escribirLog("paso por aqui");
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "A. Modificacin de Datos";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime la etiqueta Apellido paterno
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Paterno: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del apellido paterno
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["apellidopaterno"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Sexo
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "Sexo: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//imprime el valor de sexo
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["descgenero"]);
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Apellido Materno
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Materno: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de apellido materno
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["apellidomaterno"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Nacionalidad
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "Nacionalidad: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de nacionalidad
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["descnacionalidad"]);
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Nombres
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Nombre (s): ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de los nombres
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["nombre"]);
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del CURP
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "CURP: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del CURP
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["curp"];
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de la Fecha Nacimiento
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Fecha de Nacimiento: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de la fecha de nacimiento
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["fechanacimiento"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta del RFC
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "RFC: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del RFC
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["rfc"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta Entidad de Nacimiento
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Entidad de Nacimiento: ";
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de la entidad de nacimiento
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["descentidaddenacimiento"]);
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion domicilios
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$datos->domicilios[0]["domicilioparticular"]="";
				$datos->domicilios[0]["domiciliolaboral"]="";
				//Identifica el domicilio particular y laboral
				for($i = 0; $i < 2; $i++ )
				{
					if( isset($datos->domicilios[$i]["tipodomicilio"]) )
					{
						//Valida si es domicilio particular
						if( $datos->domicilios[$i]["tipodomicilio"]	== "2" )
						{
							//Valida si la calle viene diferente de vacio
							if( $datos->domicilios[$i]["calle"] != "" )
							{
								$datos->domicilios[0]["domicilioparticular"] = " ".$datos->domicilios[$i]["calle"]. " ";
							}
							//Valida si el numero exterior viene diferente de vacio
							if( $datos->domicilios[$i]["numeroexterior"] != "" )
							{
								$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].
																			"Ext: ".$datos->domicilios[$i]["numeroexterior"];
								//valida si el numero interior es diferente de vacio
								if( $datos->domicilios[$i]["numerointerior"] != "" )
								{
									$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].", ".
																			"Int ".$datos->domicilios[$i]["numerointerior"];
								}
							}
							else
							{
								//valida si el numero interior es diferente de vacio
								if( $datos->domicilios[$i]["numerointerior"] != "" )
								{
									$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].
																			"Int ".$datos->domicilios[$i]["numerointerior"];
								}
							}
							//valida si la colonia viene diferente de vacio
							if( $datos->domicilios[$i]["desccolonia"] != "" )
							{
								$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].
																			"\nCol. ".$datos->domicilios[$i]["desccolonia"];
							}
							//Valida si el codigo postal viene diferente de vacio
							if( $datos->domicilios[$i]["codigopostal"] != "" )
							{
								$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].
																			"\nC.P. ".$datos->domicilios[$i]["codigopostal"];
							}
							//Valida si el municipio viene diferente de vacio
							if( $datos->domicilios[$i]["descdelegacionmunicipio"] != "" )
							{
								$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].
																			"\n".$datos->domicilios[$i]["descdelegacionmunicipio"];
								//Valida si el estado de diferente de vacio
								if( $datos->domicilios[$i]["descentidadfederativa"] != "" )
								{
									$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].", ".
																				$datos->domicilios[$i]["descentidadfederativa"];
								}
							}
							else
							{
								//Valida si el estado de diferente de vacio
								if( $datos->domicilios[$i]["descentidadfederativa"] != "" )
								{
									$datos->domicilios[0]["domicilioparticular"] =	$datos->domicilios[0]["domicilioparticular"].
																				"\n".$datos->domicilios[$i]["descentidadfederativa"];
								}
							}

						}
						//valida si es domicilio laboral
						else if ( $datos->domicilios[$i]["tipodomicilio"] == "1" )
						{
							//Valida si la calle viene diferente de vacio
							if( $datos->domicilios[$i]["calle"] != "" )
							{
								$datos->domicilios[0]["domiciliolaboral"] = " ".$datos->domicilios[$i]["calle"]. " ";
							}
							//Valida si el numero exterior viene diferente de vacio
							if( $datos->domicilios[$i]["numeroexterior"] != "" )
							{
								$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].
																			"Ext: ".$datos->domicilios[$i]["numeroexterior"];
								//valida si el numero interior es diferente de vacio
								if( $datos->domicilios[$i]["numerointerior"] != "" )
								{
									$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].", ".
																			"Int ".$datos->domicilios[$i]["numerointerior"];
								}
							}
							else
							{
								//valida si el numero interior es diferente de vacio
								if( $datos->domicilios[$i]["numerointerior"] != "" )
								{
									$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].
																			"Int ".$datos->domicilios[$i]["numerointerior"];
								}
							}
							//valida si la colonia viene diferente de vacio
							if( $datos->domicilios[$i]["desccolonia"] != "" )
							{
								$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].
																			"\nCol. ".$datos->domicilios[$i]["desccolonia"];
							}
							//Valida si el codigo postal viene diferente de vacio
							if( $datos->domicilios[$i]["codigopostal"] != "" )
							{
								$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].
																			"\nC.P. ".$datos->domicilios[$i]["codigopostal"];
							}
							//Valida si el municipio viene diferente de vacio
							if( $datos->domicilios[$i]["descdelegacionmunicipio"] != "" )
							{
								$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].
																			"\n".$datos->domicilios[$i]["descdelegacionmunicipio"];
								//Valida si el estado de diferente de vacio
								if( $datos->domicilios[$i]["descentidadfederativa"] != "" )
								{
									$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].", ".
																				$datos->domicilios[$i]["descentidadfederativa"];
								}
							}
							else
							{
								//Valida si el estado de diferente de vacio
								if( $datos->domicilios[$i]["descentidadfederativa"] != "" )
								{
									$datos->domicilios[0]["domiciliolaboral"] =	$datos->domicilios[0]["domiciliolaboral"].
																				"\n".$datos->domicilios[$i]["descentidadfederativa"];
								}
							}
						}
					}

				}
				//Imprime la etiqueta domicilios
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "B. Domicilio (s)";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de la etiqueta Domicilio particular
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Particular: ";
				$pdf->Cell( 16, $iAltoRen - 2, $sTexto, $iBorde, 0, '');
				//Imprime el valor del domicilio particular
				$pdf->SetXY($iColDatos, $iRen);
				//Se comento para no aplicar el utf8_decode cuando se formatea la cadena
				//$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->domicilios[0]["domicilioparticular"]);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateadaDomicilio($datos->domicilios[0]["domicilioparticular"]);
				$pdf->MultiCell( 174, $iAltoRen - 2, $sTexto, $iBorde, 'J');
				//Imprime el valor de la etiqueta Domicilio particular
				$iRen += $iAltoRen * 3;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Laboral: ";
				$pdf->Cell( 16, $iAltoRen - 2, $sTexto, $iBorde, 0, '');
				//Imprime el valor del domicilio laboral
				$pdf->SetXY($iColDatos, $iRen);
				//Se comento para no aplicar el utf8_decode cuando se formatea la cadena
				//$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->domicilios[0]["domiciliolaboral"]);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateadaDomicilio($datos->domicilios[0]["domiciliolaboral"]);
				$pdf->MultiCell( 174, $iAltoRen - 2, $sTexto, $iBorde, 'J');

				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion datos de contacto
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				clog::escribirLog("por aqui tambien");
				$iRen += $iAltoRen * 3;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "C. Datos de Contacto";
				$pdf->Cell( 40, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta Telefono 1
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Telfono 1: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del telefono 1
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["telefono01"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta Correo electronico
				$pdf->SetXY($iCol + 100, $iRen);
				$sTexto = "Correo electrnico: ";
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del correo electronico
				$pdf->SetXY($iColDatos2, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["correoelectronico"];
				$pdf->Cell( 90, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Telefono2
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Telfono 2: ";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor del telefono 2
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = $datos->modificaciondatosTrabajador[0]["telefono02"];
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');


				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion actividad economica, nivel de estudios ocupacion
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "D. Actividad econmica, nivel de estudios, ocupacin.";
				$pdf->Cell( 100, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Actividad Econmica:
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Actividad Econmica: ";
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de actividad economica
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["descactividadgironegocio"]);
				$pdf->Cell( 150, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Nivel estudio
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Nivel de Estudios: ";
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de nivel de estudios
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["descnivelestudio"]);
				$pdf->Cell( 150, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime la etiqueta de Ocupacion
				$iRen += $iAltoRen;
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Ocupacin: ";
				$pdf->Cell( 190, $iAltoRen, $sTexto, $iBorde, 0, '');
				//Imprime el valor de ocupacion
				$pdf->SetXY($iColDatos, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["descocupacion"]);
				$pdf->MultiCell( 150, $iAltoRen - 2, $sTexto, $iBorde, 'J');


				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				//Imprime la seccion de beneficiarios
				//<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
				$iRen += $iAltoRen * 2;
				$pdf->SetFont('Arial','B',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "E. Beneficiarios Sustitutos";
				$pdf->Cell( 50, $iAltoRen, $sTexto, $iBorde, 0, '');
				$iRen += $iAltoRen;
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "Apellido Paterno: ";
				$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 33, $iRen);
				$sTexto = "Apellido Materno: ";
				$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 66, $iRen);
				$sTexto = "Nombre (s): ";
				$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 105, $iRen);
				$sTexto = "Parentesco: ";
				$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 130, $iRen);
				$sTexto = "CURP: ";
				$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
				$pdf->SetXY($iCol + 165, $iRen);
				$sTexto = "Porcentaje: ";
				$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');

				//Itera el total de beneficiarios que se tienen
				for( $i = 0; $i < 5; $i++)
				{
					$iRen += $iAltoRen - 2;
					$pdf->SetFont('Arial','',8);
					$pdf->SetXY($iCol, $iRen);
					$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada(isset($datos->beneficiarios[$i]["apellidopaterno"])?$datos->beneficiarios[$i]["apellidopaterno"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 33, $iRen);
					$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada(isset($datos->beneficiarios[$i]["apellidomaterno"])?$datos->beneficiarios[$i]["apellidomaterno"]:'');
					$pdf->Cell( 33, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 66, $iRen);
					$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada(isset($datos->beneficiarios[$i]["nombre"])?$datos->beneficiarios[$i]["nombre"]:'');
					$pdf->Cell( 34, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 105, $iRen);
					$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada(isset($datos->beneficiarios[$i]["descparentesco"])?$datos->beneficiarios[$i]["descparentesco"]:'');
					$pdf->Cell( 30, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 130, $iRen);
					$sTexto = isset($datos->beneficiarios[$i]["curp"])?$datos->beneficiarios[$i]["curp"]:'';
					$pdf->Cell( 35, $iAltoRen, $sTexto, $iBorde, 0, '');
					$pdf->SetXY($iCol + 165, $iRen);
					$sTexto = isset($datos->beneficiarios[$i]["porcentaje"])?$datos->beneficiarios[$i]["porcentaje"]."%":'';
					$pdf->Cell( 15, $iAltoRen, $sTexto, $iBorde, 0, '');
				}
				//Imprime la seccion de la firma del promotor y trabajador
				$iRen += $iAltoRen * 5;
				$pdf->Line( $iCol + 10, $iRen, $iCol + 85, $iRen);
				$pdf->Line( $iCol + 105, $iRen, $iCol + 180, $iRen);
				//Imprime el nombre del funcionario o promotor
				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen);

				//Se comento para no aplicar el utf8_decode cuando se formatea la cadena
				//$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada($datos->modificaciondatosTrabajador[0]["nombrefuncionario"]);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateadaFuncionario($datos->modificaciondatosTrabajador[0]["nombrefuncionario"]);
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');
				//Imprime el nombre del trabajador
				$pdf->SetXY($iCol + 95, $iRen);
				$sTexto = CExpedienteIdentificacion::obtenerCadenaFormateada(
						$datos->datosTrabajador["cNombre"].' '.
						$datos->datosTrabajador["cApellidoPaterno"].' '.
						$datos->datosTrabajador["cApellidoMaterno"]);
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');

				$pdf->SetFont('Arial','',8);
				$pdf->SetXY($iCol, $iRen + 4);
				$sTexto = "Firma del Funcionario";
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');
				$pdf->SetXY($iCol + 95, $iRen + 4);
				$sTexto = "Firma del Trabajador";
				$pdf->Cell( 95, $iAltoRen, $sTexto, $iBorde, 0, 'C');


				//Imprime el pie de pagina
				$iRen += 18;
				$pdf->SetFont('Arial','',6.5);
				$pdf->SetXY($iCol, $iRen);
				$sTexto = "AFORE COPPEL S.A. DE C.V. AVENIDA INSURGENTES ".
						"SUR No. 553 Piso 6. 603 Col. ESCANDON Delegacin ".
						"MIGUEL HIDALGO D.F. C.P. 11800";
				$pdf->Cell( 190, $iAltoRen, $sTexto, 0, 0, 'C');

				$salida = "../PDFs/$folio.pdf";
				clog::escribirLog("casi termina");
				//Si existe el archivo lo elimina
				if(file_exists($salida))
				{
					unlink($salida);
				}

				//Graba el archivo en el servidor
				$pdf->Output($salida);

				$datos->codigoRespuesta = OK__;
				//regresamos la ruta donde se genero el archivo PDF
				$datos->rutapdf = "PDFs/$folio.pdf";;

			}
			catch(Exception $e)
			{

				//Enviamos el error al usuario
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al realizar la impresi&oacute;n, por favor, reporte a Mesa de Ayuda.";

				//Enviamos el error al log
				CLog::escribirLog('[' . __FILE__ . '] FPDF Error : '. $e->getMessage());

			}
		}
		clog::escribirLog("touche");
		//retorna el objeto con la informacion generada
		return $datos;
	}

	public static function obtenerCadenaFormateada($cCadena)
	{
		$cCadenaAuxiliar = "";
		$cLetraAnterior = "";

		//decodifica la cadena a utf8 y cambia las letras a minuscualas y la primer
		//letra de cada parala a mayuscula
		$cCadenaAuxiliar = ucwords(strtolower(utf8_decode($cCadena)));

		//Itera la cadena para cambiar la letras que tienen acento
		for($i = 1; $i < strlen($cCadenaAuxiliar); $i++ )
		{

			//Valida si la letra anterior no es un espacio
			if( $cCadenaAuxiliar[$i - 1] != ' ' )
			{
				//En caso de no ser espacio valida si tiene
				//que reemplazar la letra por minuscula
				switch ($cCadenaAuxiliar[$i]) {
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

				}
			}
		}

		$cCadena = $cCadenaAuxiliar;
		//retorna la cadena formateada
		return $cCadena;
	}

	public static function obtenerCadenaFormateadaFuncionario($cCadena)
	{
		$cCadenaAuxiliar = "";
		$cLetraAnterior = "";

		//decodifica la cadena a utf8 y cambia las letras a minuscualas y la primer
		//letra de cada parala a mayuscula
		$cCadenaAuxiliar = ucwords(strtolower($cCadena));

		//Itera la cadena para cambiar la letras que tienen acento
		for($i = 1; $i < strlen($cCadenaAuxiliar); $i++ )
		{

			//Valida si la letra anterior no es un espacio
			if( $cCadenaAuxiliar[$i - 1] != ' ' )
			{
				//En caso de no ser espacio valida si tiene
				//que reemplazar la letra por minuscula
				switch ($cCadenaAuxiliar[$i]) {
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

				}
			}
		}

		$cCadena = $cCadenaAuxiliar;
		//retorna la cadena formateada
		return $cCadena;
	}

	public static function obtenerCadenaFormateadaDomicilio($cCadena)
	{
		$cCadenaAuxiliar = "";
		$cLetraAnterior = "";

		//decodifica la cadena a utf8 y cambia las letras a minuscualas y la primer
		//letra de cada parala a mayuscula
		$cCadenaAuxiliar = ucwords(strtolower($cCadena));

		//Itera la cadena para cambiar la letras que tienen acento
		for($i = 1; $i < strlen($cCadenaAuxiliar); $i++ )
		{

			//Valida si la letra anterior no es un espacio
			if( $cCadenaAuxiliar[$i - 1] != ' ' )
			{
				//En caso de no ser espacio valida si tiene
				//que reemplazar la letra por minuscula
				switch ($cCadenaAuxiliar[$i]) {
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;
					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

					case '':
						$cCadenaAuxiliar[$i] = '';
					break;

				}
			}
		}

		$cCadena = $cCadenaAuxiliar;
		//retorna la cadena formateada
		return $cCadena;
	}

	/**
	* devuelve la ip de la PC cliente
	* @return string $ipCliente
	*/
	private static function obtenerIpCliente()
	{
		$ipCliente = "";

		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"]) )
		{ //En caso de que haya un proxy intermedio la ip del cliente se encuentra aqui
			$ipCliente = $_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ipCliente = $_SERVER['REMOTE_ADDR'];
		}
		return $ipCliente;
	}


	//Funcin para validar que el promotor tiene clave CONSAR
	public static function verificarPromotor($iNumeroEmpleado)
	{
		$datos = new stdClass();
		$objApi = new Capirestexpedienteidentificacion();
		$arrApi = array('iNumeroEmpleado' => $iNumeroEmpleado);

		$resultApi = $objApi->consumirApi('verificarPromotor',$arrApi);

		if ($resultApi['estatus'] == 1)
		{

			foreach ($resultApi['registros'] as $resultado)
			{
				$datos->promotorValido = $resultado['promotorvalido'];
			}
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de actividad o giro del negocio, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcin para obtener informacin del trabajador del maestro
	public static function obtenerInformacionTrabajador($cFolio){

		global $opcionWs;
		$objCompleto = array();
		$resWs = array();
		$arrEstatus = array();
		$arrDatosAfiliado = array();
		$inParam = array("iFolioServicio"=>$cFolio);

		CLog::escribirLog( '[' . __FILE__ . '] Consultando (ws) informacin del folio '.$cFolio);

		try
		{
			$clienteWs = new SoapClient(path_wsdl, $opcionWs);
			$resWs = $clienteWs->__call('consultarAfiliado',array( $inParam ) );
			$objCompleto = get_object_vars($resWs);
			$arrEstatus = get_object_vars($objCompleto['EstadoProc']);
			if( $arrEstatus['Estado'] == OK__ )
			{
				$arrDatosAfiliado['codigoRespuesta'] = OK__;
				$arrDatosAfiliado['descripcion'] = 'EXITO';
				$arrDatosAfiliado['trabajador'] = array_map('trim',get_object_vars($objCompleto['datosAfiliado']));
				var_dump($arrDatosAfiliado['trabajador']);
			}
			else
			{
				$arrDatosAfiliado['codigoRespuesta'] = $arrEstatus['Estado'];
				$arrDatosAfiliado['descripcion'] = utf8_decode($arrEstatus['DescripcionEstado']);
			}
		}
		catch(SoapFault $soapEx)
		{
			$arrDatosAfiliado['codigoRespuesta'] = ERR__;
			$arrDatosAfiliado['descripcion'] = $soapEx->getMessage();
		}
		catch (Exception $Ex)
		{
			$arrDatosAfiliado['codigoRespuesta'] = ERR__;
			$arrDatosAfiliado['descripcion'] = $Ex->getMessage();
		}
		unset($objCompleto);
		unset($resWs);
		unset($arrEstatus);
		unset($inParam);
		
		/*
		if($arrDatosAfiliado['trabajador'] == '')
		{
			consultadatosbancoppel($cFolio);
		}
		*/

		return $arrDatosAfiliado;
	}

	//Funcin para obtener informacin del trabajador almacenada en la afiliacin/traspaso
	public static function obtenerInformacionTrabajadorAfiliacion($cFolio, $iTipoServicio){
		$datos = new stdClass();
		$arrTrabajador = array();
		$arrReferencias = array();
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);		
		$cSql = null;		
		$i = 0;
        $idTelefono = 0;
        $descripcion = "";
		if ($cnxOdbc) 
		{
			$cSql = " SELECT curp, appaterno, apmaterno, nombres, nss, tipotelefono, telefonocelular, telefonocontacto, ".
       	            " comptelefonicacontacto, codigopostal, idestado, estado, idciudad, ciudad, iddelegmunic, delegmunic, ".
                    " idcolonia, colonia, calle, numexterior, numinterior, fechageneraconstancia, genero,fechanacimiento,entidadnacimiento, esextranjero ".
                    " FROM fnconsultarsolconstanciaei($cFolio); ";

			$resultSet = $cnxOdbc->query($cSql);			
			if ($resultSet) 
			{

				foreach ($resultSet as $resultado)
				{													
					$resultado["appaterno"] = utf8_encode($resultado["appaterno"]);
					$resultado["apmaterno"] = utf8_encode($resultado["apmaterno"]);
					$resultado["nombres"] = utf8_encode($resultado["nombres"]);
					$resultado["colonia"] = utf8_encode($resultado["colonia"]);
					$resultado["calle"] = utf8_encode($resultado["calle"]);
					$resultado["codigopostal"] = utf8_encode($resultado["codigopostal"]); 
					$resultado["idestado"] = utf8_encode($resultado["idestado"]); 
					$resultado["estado"] = utf8_encode($resultado["estado"]); 
					$resultado["idciudad"] = utf8_encode($resultado["idciudad"]);
					$resultado["ciudad"] = utf8_encode($resultado["ciudad"]);
					$resultado["iddelegmunic"] = utf8_encode($resultado["iddelegmunic"]);
					$resultado["delegmunic"] = utf8_encode($resultado["delegmunic"]);
					$resultado["idcolonia"] = utf8_encode($resultado["idcolonia"]);	
					$resultado["colonia"] = utf8_encode($resultado["colonia"]);	
					$resultado["calle"] = utf8_encode($resultado["calle"]);	
					$resultado["numexterior"] = utf8_encode($resultado["numexterior"]);	
					$resultado["numinterior"] = utf8_encode($resultado["numinterior"]);	
					$resultado["fechageneraconstancia"] = utf8_encode($resultado["fechageneraconstancia"]);	
					$resultado["genero"] = utf8_encode($resultado["genero"]);	
					$resultado["fechanacimiento"] = utf8_encode($resultado["fechanacimiento"]);	
					$resultado["entidadnacimiento"] = utf8_encode($resultado["entidadnacimiento"]);	
					$resultado["esextranjero"] = utf8_encode($resultado["esextranjero"]);	
					
					
					
					$arrTrabajador[] = array_map('trim',$resultado);					
				}
				$datos->trabajador = $arrTrabajador;
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";		
				CLog::escribirLog( '[' . __FILE__ . '] Informacin del trabajador del folio '.$cFolio.' consultada con xito.');		
			}	
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de telefonos, por favor, reporte a Mesa de Ayuda.";
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}

			$cSql = "SELECT nivelestudio, ocupacion, actividadgironegocio, tipotelefono02, telefono02, tipocorreoelectronico, correoelectronico, codigopostallaboral, idestadolaboral, estadolaboral, idciudadlaboral, 
					ciudadlaboral, iddelegmuniclaboral, delegmuniclaboral, idcolonialaboral, colonialaboral, callelaboral, numexteriorlaboral, numinteriorlaboral FROM fnvalidarregistroprevioeiafiliacion($cFolio);";
			
			CLog::escribirLog( '[' . __FILE__ . '] ['.$cSql.'].');
			
			$resultSet = $cnxOdbc->query($cSql);
			if($resultSet)
			{
				foreach ($resultSet as $resultado)
				{	
					$arrTrabajador[] = array_map('trim',$resultado);					
				}
				$datos->trabajador = $arrTrabajador;
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";		
				CLog::escribirLog( '[' . __FILE__ . '] Informaci?n del trabajador del folio '.$cFolio.' consultada con ?xito.');
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de telefonos, por favor, reporte a Mesa de Ayuda.";
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			
			if ( $iTipoServicio == 27 ) {
				$cSql = "SELECT curp, nombre, appaterno, apmaterno, fecnacimiento, LPAD(entnacimiento::CHAR(2),2,'0') AS entnacimiento, genero, telefono, tipotelefono, parentescorelacion FROM fnvalidarregistroprevioeiafiliacionreferencias($cFolio)";
				
				CLog::escribirLog( '[' . __FILE__ . '] ['.$cSql.'].');
				
				$resultSet = $cnxOdbc->query($cSql);
				
				if($resultSet)
				{
					$numRef = 0;
					
					foreach ($resultSet as $resultado)
					{	
						$arrReferencias[] = array_map('trim',$resultado);					
						CLog::escribirLog("REF-->" . $resultado[curp]);
						if ($resultado[curp] != '')
							$numRef ++;
						
					}
					//$datos->referencia = $arrReferencias;
					$datos->domicilio = $arrReferencias;
					$datos->codigoRespuesta = OK__;
					$datos->descripcion = "";	
					$datos->ireferencia = $numRef;
					CLog::escribirLog( '[' . __FILE__ . '] Informaci?n de referencias del trabajador del folio '.$cFolio.' consultada con ?xito.');
				}
				else
				{
					$datos->codigoRespuesta = ERR__;
					$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de telefonos, por favor, reporte a Mesa de Ayuda.";
					//Enviamos el error al log
					$arrErr = $cnxOdbc->errorInfo();
					CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
				
			}
			else
			{
				$datos->ireferencia = 0;
			}

		} 
		else 
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
			 //Enviamos el error al log
			 $arrErr = $cnxOdbc->errorInfo();
			 CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		$cnxOdbc = null;
		return $datos;	
	}

	//Funcin para obtener el expediente de identificacin certificado
	public static function obtenerExpedienteDeIdentificacion($cFolioParam)
	{
		$datos = new stdClass();
		$datosTrabajador = new stdClass();
		$arrTrabajador = array();
		$arrDomicilio = array();
		$arrReferencia = array();
		$arrBeneficiario = array();
		$cFolio = null;
		$sNss = null;
		$sCurp = null;
		$objApi = new Capirestexpedienteidentificacion();

		$datosTrabajador = CExpedienteIdentificacion::obtenerInformacionTrabajador($cFolioParam);
		if ($datosTrabajador["codigoRespuesta"] == OK__) {

			$sNss = $datosTrabajador["trabajador"]["cNss"];
			$sCurp = $datosTrabajador["trabajador"]["cCurp"];
			$arrAPI = array('sNss' => $sNss,
							'sCurp' => $sCurp,
							'cFolio' => $cFolio,
							'opcion' => 1
						);

			$resultAPI = $objApi->consumirApi('obtenerExpedienteDeIdentificacion',$arrAPI);

			if ($resultAPI['estatus'] == 1) {

				foreach ($resultAPI['registros'] as $resultado)
				{
					$arrTrabajador[] = array_map('trim',$resultado);
				}
				if (count($arrTrabajador) > 0) {
					$datos->trabajador = $arrTrabajador;
					$datos->trabajador[0]['dtFechaServicio'] = $datosTrabajador["trabajador"]["cFechaServicio"];
					$datos->codigoRespuesta = OK__;
					$datos->descripcion = "";
				}
				else
				{
					$datos->codigoRespuesta = ERR__;
					$datos->descripcion = "No hay informaci&oacute;n del Expediente de Identificaci&oacute;n del trabajador.";
				}

			}
			else{

				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar el Expediente de Identificaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
			}

			if($datos->codigoRespuesta == OK__){

				$cFolio = $datos->trabajador[0]["folio"];

				$arrAPI = array('sNss' => $sNss,
							'sCurp' => $sCurp,
							'cFolio' => $cFolio,
							'opcion' => 2
						);

				$resultAPI = $objApi->consumirApi('obtenerExpedienteDeIdentificacion',$arrAPI);

				if ($resultAPI['estatus'] == 1){

					foreach ($resultAPI['registros'] as $resultado)
					{
						$arrDomicilio[] = array_map('trim',$resultado);
					}
					$datos->domicilio = $arrDomicilio;
					$datos->codigoRespuesta = OK__;
					$datos->descripcion = "";
				}
				else {

					$datos->codigoRespuesta = ERR__;
					$datos->descripcion = "Ocurri&oacute; un error al consultar el Expediente de Identificaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
				}

			}

			if ($datos->codigoRespuesta == OK__) {

				$cFolio = $datos->trabajador[0]["folio"];
				$arrAPI = array('sNss' => $sNss,
								'sCurp' => $sCurp,
								'cFolio' => $cFolio,
								'opcion' => 3
							);

				$resultAPI = $objApi->consumirApi('obtenerExpedienteDeIdentificacion',$arrAPI);

				if ($resultAPI['estatus'] == 1) {

					foreach ($resultAPI['registros'] as $resultado)
					{
						$arrReferencia[] = array_map('trim',$resultado);
					}
					$datos->referencias = $arrReferencia;
					$datos->codigoRespuesta = OK__;
					$datos->descripcion = "";
				}
				else {

					$datos->codigoRespuesta = ERR__;
					$datos->descripcion = "Ocurri&oacute; un error al consultar el Expediente de Identificaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
				}
			}

			if ($datos->codigoRespuesta == OK__){

				$cFolio = $datos->trabajador[0]["folio"];

				$arrAPI = array('sNss' => $sNss,
								'sCurp' => $sCurp,
								'cFolio' => $cFolio,
								'opcion' => 4
							);

				$resultAPI = $objApi->consumirApi('obtenerExpedienteDeIdentificacion',$arrAPI);

				if ($resultAPI['estatus'] == 1) {

					foreach ($resultAPI['registros'] as $resultado)
					{
						$arrBeneficiario[] = array_map('trim',$resultado);
					}
					$datos->beneficiarios = $arrBeneficiario;
					$datos->codigoRespuesta = OK__;
					$datos->descripcion = "";
				}
				else {

					$datos->codigoRespuesta = ERR__;
					$datos->descripcion = "Ocurri&oacute; un error al consultar el Expediente de Identificaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
				}
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el Expediente de Identificaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
			CLog::escribirLog( '[' . __FILE__ . '] Error al consultar en informix la informacin del servicio.');
		}

		return $datos;
	}

	//Funcin para obtener la descripcin del servicio que llam al Expediente de Identificacin
	private static function obtenerDescripcionServicio($iTipoServicio)
	{
		$datos = new stdClass();
		$arrServicio = array();
		$objAPI = new Capirestexpedienteidentificacion();

		CLog::escribirLog( '[' . __FILE__ . '] Consultando informacin del servicio '.$iTipoServicio);

		$resultAPI = $objAPI->consumirApi('obtenerDescripcionServicio',$iTipoServicio);

		if ($resultAPI['estatus'] == 1)
		{
			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrServicio[] = array_map('trim',$resultado);
			}

			if (count($arrServicio) > 0)
			{
				$datos->servicio = $arrServicio;
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
				CLog::escribirLog( '[' . __FILE__ . '] Descripcin del servicio consultada con xito.');
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "No se encontr&oacute; informaci&oacute;n del trabajador, para el folio proporcionado.";
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la informaci&oacute;n del trabajador, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcin para guardar en la tabla solllamadasconfirmacioncat para el CAT
	private static function guadarLlamadaCat($cFolio,$iTipoServicio)
	{
		$datos = new stdClass();
		$datosServicio = new stdClass();
		$objAPI = new Capirestexpedienteidentificacion();
		$cDescServicio = null;
		$iResultadoGuardado = 0;

		$datosServicio = self::obtenerDescripcionServicio($iTipoServicio);
		$cDescServicio = $datosServicio->servicio[0]["CDESCRIPCION"];

		if ($datosServicio->codigoRespuesta == OK__)
		{
			$arrApi = array(
				"cFolio"  => $cFolio,
				"cDescServicio"  => $cDescServicio);

			$resultAPI = $objAPI->consumirApi('guadarLlamadaCat',$arrApi);

			if ($resultAPI['estatus'] == 1)
			{
				foreach ($resultAPI['registros'] as $resultado)
				{
					$iResultadoGuardado = $resultado['llamada'];
				}
				$datos->guardoLlamadaCat = $iResultadoGuardado;
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
				CLog::escribirLog( '[' . __FILE__ . '] Llamada del CAT guardada con xito para el folio '.$cFolio);
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al guardar la informaci&oacute;n para el CAT, por favor, reporte a Mesa de Ayuda.";
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al guardar la informaci&oacute;n para el CAT, por favor, reporte a Mesa de Ayuda.";
			//Enviamos el error al log
			CLog::escribirLog( '[' . __FILE__ . '] Error: No se obtuv descripcin del servicio.');
		}

		return $datos;
	}

	//Funcion que retorna una fecha tres meses atras del da actual
	public static function obtenerFechaMaximaComprobanteDomicilio()
	{
		$datos = new stdClass();
		$objAPI = new Capirestexpedienteidentificacion();

		$resultAPI = $objAPI->consumirApi('obtenerFechaMaximaComprobanteDomicilio',$arrDatos);

		if ($resultAPI['estatus'] == 1)
		{
			$datos->fechaMaxima = $resultAPI['fechamaxima'];
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
			CLog::escribirLog( '[' . __FILE__ . '] Fecha mxima del comprobante de domicilio consultada con xito.');
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la fecha m&aacute;xima para el comprobante del domicilio, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcin para revisar la vigencia de la identificacin oficial
	public static function obtenerVigenciaIdentificacionOficial($cNss, $cCurp)
	{
		$datos = new stdClass();
		$objAPI = new Capirestexpedienteidentificacion();
		$arrApi = array(
			"cNss"  => $cNss,
			"cCurp"  => $cCurp);

		$resultAPI = $objAPI->consumirApi('obtenerVigenciaIdentificacionOficial',$arrApi);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$datos->vigencia = $resultado['vigenciaexpirada'];
			}
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
			CLog::escribirLog( '[' . __FILE__ . '] Vigencia de la identificacin oficial del trabajador '.$cNss.' ,'.$cCurp.' consultada con xito.');
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la fecha m&aacute;xima para el comprobante del domicilio, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcin para revisar la vigencia de la fotografa
	public static function obtenerVigenciaFotografia($cNss, $cCurp)
	{
		$datos = new stdClass();
		$objAPI = new Capirestexpedienteidentificacion();

		$arrApi = array(
					"cCurp"  => $cCurp,
					"cNss"  => $cDescServicio);

		$resultAPI = $objAPI->consumirApi('obtenerVigenciaFotografia',$arrApi);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$datos->vigencia = $resultado['vigenciaexpirada'];
			}
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
			CLog::escribirLog( '[' . __FILE__ . '] Vigencia de la fotografa del trabajador '.$cNss.' ,'.$cCurp.' obtenida con xito');
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la fecha m&aacute;xima para el comprobante del domicilio, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	//Funcin para actualizar el estatus del expediente a digitalizado
	public static function actualizarEstatusExpediente($cFolio, $iTipoServicio, $iRespuesta)
	{
		$datos = new stdClass();
		$objAPI = new Capirestexpedienteidentificacion();

		if ($iRespuesta == 1) {

			if ($iTipoServicio != 26 && $iTipoServicio != 27 && $iTipoServicio != 33) {
				$cFolio = $cFolio."-S";
			}
			
			$arrApi = array("cFolio"  => $cFolio);
			
			CLog::escribirLog( '[' . __FILE__ . '] Respuesta del applet: ['.$iRespuesta.'] Folio: ['.$cFolio.'].');
			$resultAPI = $objAPI->consumirApi('actualizarEstatusExpediente',$arrApi);

			if ($resultAPI['estatus'] == 1)
			{
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
				CLog::escribirLog( '[' . __FILE__ . '] Folio ['.$cFolio.'] actualizado con xito.');
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al actualizar el estado del Expediente de Identificaci&oacute;n, por favor, reporte a Mesa de Ayuda.";
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = 'El Expediente de Identificaci&oacute;n no se digitaliz&oacute; correctamente, por favor, reporte a Mesa de Ayuda.';
		}

		return $datos;
	}

	//Funcin para obtener la fecha mxima de mayoria de edad al da de hoy
	public static function obtenerFechaMaximaMayoriaEdad()
	{
		$datos = new stdClass();
		$objAPI = new Capirestexpedienteidentificacion();
		$arrApi = array();

		$resultAPI = $objAPI->consumirApi('obtenerFechaMaximaMayoriaEdad',$arrApi);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$datos->fechaMaxima = $resultado['fechamaxima'];
			}
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
			CLog::escribirLog( '[' . __FILE__ . '] Fecha mxima del comprobante de domicilio consultada con xito.');
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar la fecha m&aacute;xima para el comprobante del domicilio, por favor, reporte a Mesa de Ayuda.";
		}

		return $datos;
	}

	public static function PermisosCurpsReferecias($iTipoServicio)
	{
		$datos = new stdClass();
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);
		$cSql = null;
		if ($cnxOdbc)
		{
			$cSql ="select fnobtenertiposervicioeiactivado ($iTipoServicio) as serviciovalidado";
			$resulSet = $cnxOdbc->query($cSql);
			if($resulSet)
			{
				foreach($resulSet as $resultado)
				{
					$datos->serviciovalido = $resultado['serviciovalidado'];
				}
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar si el servicio esta dentro de la tabla para validar si es necesario las curps de las referencias, por favor, reporte a Mesa de Ayuda.";
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
			 //Enviamos el error al log
			 $arrErr = $cnxOdbc->errorInfo();
			 CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		$cnxOdbc = null;
		return $datos;
	}

	public static function CurpsGenericasReferecias($iTipoReferencia)
	{
		$datos = new stdClass();
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);
		$cSql = null;
		if ($cnxOdbc)
		{
			$cSql =" select fnobtenercurpgenericareferenciasei($iTipoReferencia) as curpref ";
			$resulSet = $cnxOdbc->query($cSql);

			if($resulSet)
			{
				foreach($resulSet as $resultado)
				{
					$datos->curpreferencia = $resultado['curpref'];
				}
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar si el servicio eta dentro de la tabla para validar si es necesario las curps de las referencias, por favor, reporte a Mesa de Ayuda.";
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
			 //Enviamos el error al log
			 $arrErr = $cnxOdbc->errorInfo();
			 CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		$cnxOdbc = null;
		return $datos;
	}

	public static function vaidarMayorDeEdad()
	{
		$datos = new stdClass();
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);
		$cSql = null;
		if ($cnxOdbc)
		{
			$cSql = " SELECT current_date as fechavalida;";
			$resultSet = $cnxOdbc->query($cSql);
			if ($resultSet)
			{

				foreach ($resultSet as $resultado)
				{
					$datos->fechavalida = $resultado['fechavalida'];
				}
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
				CLog::escribirLog( '[' . __FILE__ . '] Fecha mayor de edad de la referencias obtenida con exito.');
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar la fecha mayor de edad valida, por favor, reporte a Mesa de Ayuda.";
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
			 //Enviamos el error al log
			 $arrErr = $cnxOdbc->errorInfo();
			 CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		$cnxOdbc = null;
		return $datos;
	}

	public static function extraerDatosBancoppel($cFolio)
	{
		$datos = new stdClass();
		$arrTrabajador = array();
		$objAPI = new Capirestexpedienteidentificacion();
		$arrApi = array('cFolio' => $cFolio);

		$resultAPI = $objAPI->consumirApi('extraerDatosBancoppel',$arrApi);

		if ($resultAPI)
		{
			foreach ($resultAPI['registros'] as $resultado)
			{
				$datos->rfc = $resultado['rfc'];
				$datos->fechanacimiento = $resultado['fechanacimiento'];
				$datos->entidadnacimiento = $resultado['entidadnacimiento'];
				$datos->genero = $resultado['genero'];
				$datos->nacionalidad = $resultado['nacionalidad'];
				$datos->escolaridad = $resultado['escolaridad'];
				$datos->profesion = $resultado['profesion'];
				$datos->actividad = $resultado['actividad'];
				$datos->telefonofijo = $resultado['telefonofijo'];
				$datos->email = $resultado['email'];
				//datos referencia 1
				$datos->apellidopatref1 = $resultado['apellidopatref1'];
				$datos->apellidomatref1 = $resultado['apellidomatref1'];
				$datos->nombresref1 = $resultado['nombresref1'];


				$arrTrabajador[] = array_map('trim',$resultado);

			}
			$datos->codigoRespuesta = OK__;
			$datos->trabajador = $arrTrabajador;
			$datos->descripcion = "";
			CLog::escribirLog( '[' . __FILE__ . '] Se extrajeron los datos Bancoppel correctamente');
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar los datos del cleinte Bancoppel ";
			//Enviamos el error al log
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		return $datos;
	}

	//Este mtodo verifica si para el folio dado se consulto la CURP del empleado a traves de RENAPO
	//y nos regresa informacin del empleado que se consiguio a traves de esta consulta de CURP.
	public static function extraerDatosRenapo($cFolio)
	{
		$datos = new stdClass();
		$arrTrabajador = array();
		$objAPI = new Capirestexpedienteidentificacion();
		$arrApi = array('cFolio' => $cFolio);

		$resultAPI = $objAPI->consumirApi('extraerDatosRenapo',$arrApi);

		if ($resultAPI)
		{
			foreach ($resultAPI['registros'] as $resultado)
			{
				$datos->fechanacimiento = $resultado['fechanacimiento'];
				$datos->entidadnacimiento = $resultado['entidadnacimiento'];
				$datos->genero = $resultado['genero'];
				$datos->nacionalidad = $resultado['nacionalidad'];

				$arrTrabajador[] = array_map('trim',$resultado);

			}
			$datos->codigoRespuesta = OK__;
			$datos->trabajador = $arrTrabajador;
			$datos->descripcion = "";
			CLog::escribirLog( '[' . __FILE__ . '] Se extrajeron los datos de Renapo correctamente');
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar los datos de RENAPO";
			//Enviamos el error al log
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		return $datos;
	}

	//Este mtodo recibe los datos necesarios para realizar una Consulta de CURP por Datos a RENAPO y
	// consume el WS de RENAPO y regresa el Folio de Servicio que le corresponde a la consulta reazizada.
	public static function ejecutaServicioRenapoDatos ($cApellidopaterno, $cApellidomaterno, $cNombres, $cSexo, $cFechaNacimiento,$iSlcEntidadNacimiento, $iEmpleado, $idServidor, $idServicioD)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();

		//se crea arreglo a retornar
		$arrResp = array();

		//Se declaran varibles en vacio y 0 por si falla la incacion tenga algo por default
		$arrResp->descripcionRespuesta= '';
		$arrResp->folioServicioAfore = 0;
		$arrResp->respondioServicio = 0;

		//Se obtine la fecha del servidor
		date_default_timezone_set('America/Mazatlan');
		setlocale(LC_TIME, 'spanish');

		if($cApellidomaterno == '')
		{
			//Se crea array a mandar
			$array = array(
					'numeroEmpleado'=> $iEmpleado,
					'fechaNacimiento'=> $cFechaNacimiento,
					'nombre'=> $cNombres,
					'primerApellido'=> $cApellidopaterno,
					'sexo'=> $cSexo,
					'entidadNacimiento'=> $iSlcEntidadNacimiento
				 );
		}
		else
		{
			//Se crea array a mandar
			$array = array(
					'numeroEmpleado'=> $iEmpleado,
					'fechaNacimiento'=> $cFechaNacimiento,
					'nombre'=> $cNombres,
					'primerApellido'=> $cApellidopaterno,
					'segundoApellido' => $cApellidomaterno,
					'sexo'=> $cSexo,
					'entidadNacimiento'=>$iSlcEntidadNacimiento
				 );
		}
		//Crear XML a enviar de parametros
		$xml = $objGn->obtenerXML($array);

		$mensaje = "XML:".$xml;
		CLog::escribirLog( '[' . __FILE__ . ']'.$mensaje);

		//Ejecutar el Servicio
		$arrResp = $objGn->consumirServicioEjecutarAplicacion($idServicioD, $idServidor, $xml);

		//pasa la respuesta obtenida del metodo anterior
		$mensaje = $arrResp->descripcionRespuesta;
		$folioServicioAfore = $arrResp->folioServicioAfore;
		$respuestaservicio = $arrResp->respondioServicio;

		$mensajelog = "Mensaje:".$mensaje.'FolioAfore:'.$folioServicioAfore."RespServicioInv:".$respuestaservicio."RespServicioEI:".$respuestaEI;
		CLog::escribirLog( '[' . __FILE__ . ']'.$mensajelog);

		return $arrResp;
	}

	//Este mtodo recibe y guarda los datos con los que se realizo la consulta de CURP a RENAPO.
	public static function guardarSolicitudRenapo($iFolioAfore, $iFolioSolicitud, $iProceso, $iEmpleado, $idsubproceso, $cCurp, $cApellidopaterno, $cApellidomaterno, $cNombres, $cSexo, $cFechaNacimiento,$iSlcEntidadNacimiento)
	{
		//CREA UN OBJETO DE LA CLASE
		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestexpedienteidentificacion();

		//Se obtiene la IP del modulo.
		$cIpModulo = $objGn -> getRealIP();

		//SE APLICA INTRUCCION PARA EL TRATADO DE
		$cNombres = utf8_decode($cNombres);
		$cApellidopaterno = utf8_decode($cApellidopaterno);
		$cApellidomaterno = utf8_decode($cApellidomaterno);

		//SE DECLARA UN ARREGLO
		$arrDatos = array("estatus"=>0,
					  "MensajeDescrip"=>"",
					  "respuesta"=>""
					   );

		//SE DECLARA ARREGLO PARA API REST
		$arrApi = array(
					   'iFolioAfore'=> $iFolioAfore,
					   'iFolioSolicitud'=> $iFolioSolicitud,
					   'iProceso'=> $iProceso,
					   'iEmpleado'=> $iEmpleado,
					   'idsubproceso'=> $idsubproceso,
					   'cCurp'=> $cCurp,
					   'cApellidopaterno'=> $cApellidopaterno,
					   'cApellidomaterno'=> $cApellidomaterno,
					   'cNombres'=> $cNombres,
					   'cSexo'=> $cSexo,
					   'cFechaNacimiento'=> $cFechaNacimiento,
					   'iSlcEntidadNacimiento'=> $iSlcEntidadNacimiento
				   );
		try
		{
			$resultAPI = $objAPI->consumirApi('guardarSolicitudRenapo',$arrApi);

			//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
			if($resultAPI)
			{
				//INDICADOR QUE ASIGNA ESTATUS 1, OSEA QUE ES CORRECTO Y SU DESCRIPCION
				$arrDatos['estatus'] = 1;
				$arrDatos['MensajeDescrip'] = "EXITO";

				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['respuesta']= $reg["valor"];
				}
			}
			else
			{
				// SI EXISTE UN ERROR EN LA CONSULTA MOSTRARA EL SIGUIENTE MENSAJE
				$arrDatos['estatus'] = ERR__;
				$arrDatos['MensajeDescrip'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("tCExpedienteIdentificacion.php\tguardarSolicitudRenapo"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}

		}
		catch (Exception $e)
		{
			//CACHA LA EXCEPCION POR LA QUE FALLO LA EJECUCION DEL QUERY Y LO MANDA COMO PARAMETRO PARA ESCRIBIR LA DESCRIPCION DEL ERROR.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			CLog::escribirLog( '[' . __FILE__ . ']Consulta WS: '.$mensaje);
		}

		return $arrDatos;
	}

	//Este mtodo consulta que la respuesta de RENAPO haya side exitos o en su defecto que Error nos arroja.
	public static function consultaRespuestaRenapo($iFolioAfore)
	{
		$objAPI = new Capirestexpedienteidentificacion();
		//SE DECLARA UN ARREGLO
		$arrDatos = array("estatus"=>0,
					  "MensajeDescrip"=>"",
					  "tipoerror"=>-1,
					  "codigoerrorrenapo"=>-1,
					  "descerror"=>"",
					  "curp"=>"",
					  "apellidopaterno"=>"",
					  "apellidomaterno"=>"",
					  "nombre"=>"",
					  "subproceso"=>0
					   );

		$arrApi = array('iFolioAfore' => $iFolioAfore);

		try
		{
			//EJECUTA LA CONSULTA
			$resultAPI = $objAPI->consumirApi('consultaRespuestaRenapo',$arrApi);
			//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
			if($resultAPI)
			{
				//INDICADOR QUE ASGNA ESTATUS 1, OSEA QUE ES CORRECTO Y SU DESCRIPCION
				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['estatus'] = 1;
					$arrDatos['MensajeDescrip'] = "EXITO";
					$arrDatos['tipoerror'] = $reg["idu_tipoerror"];
					$arrDatos['codigoerrorrenapo'] = $reg["idu_codigoerrorrenapo"];
					$arrDatos['descerror'] = utf8_encode(trim($reg["mensaje"]));
					$arrDatos['curp'] = $reg["ccurp"];
					//$sDesc = strpos(strtoupper($reg['descripcion']), '')===false ? $reg['descripcion'] : utf8_decode($reg['descripcion']);
					$arrDatos['apellidopaterno'] = strpos(strtoupper($reg['capellidopaterno']), '')===false ? $reg['capellidopaterno'] : utf8_decode($reg['capellidopaterno']);
					//$reg["capellidopaterno"];
					$arrDatos['apellidomaterno'] = strpos(strtoupper($reg['capellidomaterno']), '')===false ? $reg['capellidomaterno'] : utf8_decode($reg['capellidomaterno']);
					//$reg["capellidomaterno"];
					$arrDatos['nombre'] = strpos(strtoupper($reg['cnombre']), '')===false ? $reg['cnombre'] : utf8_decode($reg['cnombre']);
					//$reg["cnombre"];
					$arrDatos['subproceso'] = $reg["isubproceso"];
				}

				$mensaje = "\tCExpedienteIdentificacion.php\tconsultaRespuestaRenapo"."\tSe consulto exitosamente codigoerrorrenapo=__________" + $arrDatos['codigoerrorrenapo'] + " " + $arrDatos['curp'] + " " ;
				CLog::escribirLog( '[' . __FILE__ . '] '.$mensaje);
			}
			else
			{
				// SI EXISTE UN ERROR EN LA CONSULTA MOSTRARA EL SIGUIENTE MENSAJE
				$arrDatos['estatus'] = ERR__;
				$arrDatos['MensajeDescrip'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("tCExpedienteIdentificacion.php\tconsultaRespuestaRenapo"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//CACHA LA EXCEPCION POR LA QUE FALLO LA EJECUCION DEL QUERY Y LO MANDA COMO PARAMETRO PARA ESCRIBIR LA DESCRIPCION DEL ERROR.
			$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			CLog::escribirLog( '[' . __FILE__ . ']'.$mensaje);
		}
		return $arrDatos;
	}

	//Este mtodo obtiene la URL para generar el formato de CURP
	public static function obtenerURLFormatoCurp($iOpcionFormatoRenapo, $iFolioAfore, $sIpRemoto)
	{
		CLog::escribirLog("Entra a obtenerURLFormatoCurp");
		//SE DECLARA UN ARREGLO
		$objAPI = new Capirestexpedienteidentificacion();
		$arrApi = array(
					"iOpcionFormatoRenapo"  => $iOpcionFormatoRenapo,
					"iFolioAfore"  => $iFolioAfore,
					"sIpRemoto"  => $sIpRemoto);

		$arrDatos = array("URL" => "");

		try
		{
			$resultAPI = $objAPI->consumirApi('obtenerURLFormatoCurp',$arrApi);

			//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
			if($resultAPI['estatus'] == 1)
			{
				foreach($resultAPI['registros'] as $reg)
				{
					$arrDatos['URL']= trim($reg["url"]);
				}
			}
			else
			{
				// SI EXISTE UN ERROR EN LA CONSULTA MOSTRARA EL SIGUIENTE MENSAJE
				throw new Exception("\tCExpedienteIdentificacion.php\tobtenerURLFormatoCurp"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//CACHA LA EXCEPCION POR LA QUE FALLO LA EJECUCION DEL QUERY Y LO MANDA COMO PARAMETRO PARA ESCRIBIR LA DESCRIPCION DEL ERROR.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			CLog::escribirLog( '[' . __FILE__ . ']'.$mensaje);
		}
		return $arrDatos;
	}

	public function obtenerExcepcionEnrolamiento( $cFolio )
	{
		$objAPI = new Capirestexpedienteidentificacion();
		//CLog::escribirLog("[". __FUNCTION__ ."] Folio Antes: ".$cFolio);
		$iFolio =  str_replace("-S", "", $cFolio);
		//CLog::escribirLog("[". __FUNCTION__ ."] Folio Despues: ".$cFolio);

		$arrDatos = array("mensaje"=>'', "respuesta"=>-1);
		$arrApi = array('folio' => $iFolio);
		try
		{
			$resultAPI = $objAPI->consumirApi('obtenerExcepcionEnrolamiento',$arrApi);

			if($resultAPI['estatus'] == 1)
			{

				foreach ($resultAPI['registros'] as $reg) {
					//CLog::escribirLog("[". __FUNCTION__ ."] Resultado -- >: ".$reg["resultado"]);
					$arrDatos["respuesta"] = $reg["resultado"];
					$arrDatos["mensaje"] = "Exito en la consulta";
				}
			}
		}
		catch (Exception $e) {
			$mensaje= 'Error favor de comunucarse con incidencias afore . ' ;
			$arrDatos["mensaje"] = $mensaje;
		    CLog::escribirLog ( "[" . __FILE__ . "]" . $e);
		}

		return $arrDatos;
	}

	public static function validarTelefonoListaNegra($cTelefono1,$cTelefono2,$cFolio , $iEmpleado)
	{

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestexpedienteidentificacion();
		
		$arrTelefonos = array(
		"cTelefono1"=>$cTelefono1,
		"cTelefono2"=>$cTelefono2,
		"cFolio"=>$cFolio,
		"iEmpleado"=>$iEmpleado
		);

		try
		{
			$resultAPI = $objAPI->consumirApi('validarTelefonoListaNegra',$arrTelefonos);
			
			if($resultAPI)
			{
				foreach ($resultAPI['registros'] as $reg) {
					foreach ($reg as $reg2) {
						$arrDatos[] = $reg2;		
					}
				}
			}
			else
			{
				
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";

				throw new Exception("constanciaafiliacion.php\tvalidarTelefonoListaNegra"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}

	//Funcion que verifica si el solicitante tiene alguna excepcion de manos.
	public function obtenerExcepcionSolicitante($cFolio) {

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestexpedienteidentificacion();

		$arrDatos = array("mensaje"=>'', "excepcion"=>-1);

		$arrApi = array("cFolio"=>$cFolio);

		try
		{
			$resultAPI = $objAPI->consumirApi('obtenerExcepcionSolicitante',$arrApi);
			
			if($resultAPI)
			{
				foreach ($resultAPI['registros'] as $reg) {
					$iExepciones = $reg['iexcepcion'];
					$iTipoSolicitante = $reg['itiposolicitante'];
				}
		
				if($iExepciones > 0){
					$arrDatos["excepcion"] = 1;
				}
				elseif ($iExepciones == 0 && $iTipoSolicitante == 1) {
					$arrDatos["excepcion"] = 0;
				}
				else{
					$arrDatos["excepcion"] = 0;
				}
			}
			else
			{
				$arrDatos['estatus'] = ERR__;
				$arrDatos['descripcion'] = "Se presento un problema al ejecutar la consulta";
				throw new Exception("constanciaafiliacion.php\tvalidarTelefonoListaNegra"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
	//Funcion que obtiene el tipo de solicitante y si el solicitante es promotor activo
	public function obtenerTipoSolicitante($cFolio) {

		$objGn = new CMetodoGeneral();
		$objAPI = new Capirestexpedienteidentificacion();

		$arrDatos = new stdClass();
		$arrDatos->tiposolicitante = 0;
		$arrDatos->promotoractivo = 0;
		$arrDatos->claveconsar = 0;
		$arrDatos->estatus = 0;
		$arrDatos->descripcion = '';


		$arrApi = array("iFolio"=>$cFolio);

		try
		{
			$resultAPI = $objAPI->consumirApi('obtenertiposolicitante',$arrApi);
			
			if($resultAPI)
			{
				foreach ($resultAPI['registros'] as $reg) {
					$arrDatos->tiposolicitante = $reg['tiposolicitante'];
					$arrDatos->promotoractivo = $reg['promotoractivo'];
					$arrDatos->claveconsar = $reg['claveconsar'];
					$arrDatos->estatus = OK__;
					$arrDatos->descripcion = "Tipo solicitante consultado correctamente.";
				}
			}
			else
			{
				$arrDatos->tiposolicitante = 0;
				$arrDatos->estatus = ERR__;
				$arrDatos->descripcion = "Se presento un problema al ejecutar la consulta";
				throw new Exception("\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		    $objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
		}
		return $arrDatos;
	}
		//Funcion que obtiene los datos de solicitante
		public function obtenerDatosSolicitante($cFolio) {

			$objGn = new CMetodoGeneral();
			$objAPI = new Capirestexpedienteidentificacion();
	
			$arrDatos = new stdClass();
			$arrDatos->datossolicitante = null;
			$arrDatos->estatus = 0;
			$arrDatos->descripcion = '';
	
	
			$arrApi = array("iFolio"=>$cFolio);
			try
			{
				$resultAPI = $objAPI->consumirApi('obtenerdatossolitante',$arrApi);
				
				if($resultAPI)
				{
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos->datossolicitante = array_map('trim',$reg);
						$arrDatos->estatus = OK__;
						$arrDatos->descripcion = "Datos del solicitante consultados correctamente.";
					}
				}
				else
				{
					$arrDatos->estatus = ERR__;
					$arrDatos->descripcion = "Se presento un problema al ejecutar la consulta";
					throw new Exception("\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
			}
			catch (Exception $e)
			{
				//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			return $arrDatos;
		}
		//Funcion que guarda los datos de solicitante
		public function guardarDatosSolicitante($cFolio, $objSolicitante) {

			$objGn = new CMetodoGeneral();
			$objAPI = new Capirestexpedienteidentificacion();
	
			$arrDatos = new stdClass();
			$arrDatos->estatus = 0;
			$arrDatos->descripcion = '';
	
	
			$arrApi = array("iFolio"=>$cFolio,
				"sTipoSolicitante"=> $objSolicitante['sTipoSolicitante'],
				"sApellidoPatSol"=> $objSolicitante['sApellidoPatSol'],
				"sApellidoMatSol"=> $objSolicitante['sApellidoMatSol'],
				"sNombresSol"=> $objSolicitante['sNombresSol'],
				"sCurpSol"=> $objSolicitante['sCurpSol'],
				"sFechaNacSol"=> $objSolicitante['sFechaNacSol'],
				"iEntidadSol"=> $objSolicitante['iEntidadSol'],
				"iNacionalidadSol"=> $objSolicitante['iNacionalidadSol'],
				"iGeneroSol"=> $objSolicitante['iGeneroSol'],
				"sRFCSol"=> $objSolicitante['sRFCSol'],
				"iNumeroHoja"=> $objSolicitante['iNumeroHoja'],
				"iNumeroNotario"=> $objSolicitante['iNumeroNotario'],
				"sNombreNotario"=> $objSolicitante['sNombreNotario'],
				"iNumeroTestimonio"=> $objSolicitante['iNumeroTestimonio'],
				"sFechaTestimonio"=> $objSolicitante['sFechaTestimonio'],
				"sNombreJuzgado"=> $objSolicitante['sNombreJuzgado'],
				"iNumeroJuicio"=> $objSolicitante['iNumeroJuicio'],
				"iPromotorActivo"=> $objSolicitante['iPromotorActivo'],
				"iPromotorInterno"=> $objSolicitante['iPromotorInterno'],
				"iClaveConsar"=> $objSolicitante['iClaveConsar'],
				"iParentesco"=> $objSolicitante['iParentesco']);
			try
			{
				$resultAPI = $objAPI->consumirApi('guardardatossolicitante',$arrApi);
				
				if($resultAPI)
				{
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos->datossolicitante = $reg;
						$arrDatos->estatus = OK__;
						$arrDatos->descripcion = "Datos del solicitante guardados correctamente.";
					}
				}
				else
				{
					$arrDatos->tiposolicitante = 0;
					$arrDatos->estatus = ERR__;
					$arrDatos->descripcion = "Se presento un problema al ejecutar la consulta";
					throw new Exception("\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
			}
			catch (Exception $e)
			{
				//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			return $arrDatos;
		}
		
		//Funcion que consulta el estatus del servicio 0407
		public function obtenerVerificacionei($cCurpTrabajador, $iNumpEmpleado, $cTipoServicio){
			$objGn = new CMetodoGeneral();
			$objAPI = new Capirestexpedienteidentificacion();
			
			$arrDatos = new stdClass();
			$arrDatos->estatus = 0;
			$arrDatos->descripcion = '';
			
			$arrApi = array("cCurpTrabajador"=>$cCurpTrabajador,
							"iNumpEmpleado"=>$iNumpEmpleado,
							"cTipoServicio"=>$cTipoServicio);
			try
			{
				$resultAPI = $objAPI->consumirApi('obtenerVerificacionei',$arrApi);
				
				if($resultAPI)
				{
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos->estatusverificacion = $reg['verificacionservicio'];
						$arrDatos->estatus = OK__;
						$arrDatos->descripcion = "Estatus consultado correctamente.";
					}
				}
				else
				{
					$arrDatos->tiposolicitante = 0;
					$arrDatos->estatus = ERR__;
					$arrDatos->descripcion = "Se presento un problema al ejecutar la consulta";
					throw new Exception("\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
			}
			catch (Exception $e)
			{
				//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			return $arrDatos;
		}
		
		public function obtenerInformacionServicioEi($sCurp){
			$objGn = new CMetodoGeneral();
			$objAPI = new Capirestexpedienteidentificacion();
			
			$arrDatos = new stdClass();
			$arrDatos->estatus = 0;
			$arrDatos->descripcion = '';
			$arrDatos->datostrabajador = null;
			
			$arrApi = array("curpTrabajador"=>$sCurp);
			try
			{
				$resultAPI = $objAPI->consumirApi('obtenerInformacionServicioEi',$arrApi);
				
				
				
				if($resultAPI)
				{
					foreach ($resultAPI['registros'] as $reg) {
						$arrDatos->datostrabajador = $reg;
						$arrDatos->estatus = OK__;
						$arrDatos->descripcion = "Datos consultados correctamente";
					}
				}
				else
				{
					$arrDatos->datostrabajador = null;
					$arrDatos->estatus = ERR__;
					$arrDatos->descripcion = "Se presento un problema al ejecutar la consulta";
					throw new Exception("\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}
			}
			catch (Exception $e)
			{
				//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
				$objGn->grabarLogx( '[' . __FILE__ . ']' . $mensaje);
			}
			return $arrDatos;
		}
		
		public function obtenerbanderabancoppel($cFolio)
		{
			$datos = new stdClass();
			$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);
			$cSql = null;
			if ($cnxOdbc)
			{
			$cSql = "select fnllenadobitacoraafiliacionbancoppel from fnllenadobitacoraafiliacionbancoppel".
			"('$cFolio','1900-01-01','',0,0,'0','0','','','','','','1900-01-01',0,'',0,'',0,'',0,0,0,'0','','',".
			"'','','','',0,0,0,0,'','','','','',0,0,0,0,'','','','','',0,0,0,0,'','','','','','','','','',3)";
				
				$resulSet = $cnxOdbc->query($cSql);

				if($resulSet)
				{
					foreach($resulSet as $resultado)
					{
						$datos->fnllenadobitacoraafiliacionbancoppel = $resultado['fnllenadobitacoraafiliacionbancoppel'];
					}
				}
				else
				{
					$datos->codigoRespuesta = ERR__;
					$datos->descripcion = "Ocurri&oacute; un error al consultar si el servicio eta dentro de la tabla para validar si es necesario las curps de las referencias, por favor, reporte a Mesa de Ayuda.";
					//Enviamos el error al log
					$arrErr = $cnxOdbc->errorInfo();
					CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
				}
			}
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
				 //Enviamos el error al log
				 $arrErr = $cnxOdbc->errorInfo();
				 CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
			$cnxOdbc = null;
			return $datos;
		}

		//Folio 1514 - Funcion para mandar guardar los datos junto con estatus de os switch
		public static function guardarDatosSwitch($cCurp, $iNss, $iTiposolicitud, $cCorreo, $iBandnotificaciones, $iBandedocta)
		{
			//CREA UN OBJETO DE LA CLASE
			$objGn = new CMetodoGeneral();
			$objAPI = new Capirestexpedienteidentificacion();

			//SE DECLARA UN ARREGLO
			$arrDatos = array("estatus"=>0,
						"MensajeDescrip"=>"",
						"respuesta"=>""
						);

			//SE DECLARA ARREGLO PARA API REST
			$arrApi = array(
						'cCurp'=> $cCurp,
						'iNss'=> $iNss,
						'iTiposolicitud'=> $iTiposolicitud,
						'cCorreo'=> $cCorreo,
						'iBandnotificaciones'=> $iBandnotificaciones,
						'iBandedocta'=> $iBandedocta,
					);
			
			try
			{
				//CLog::grabarLogx( '[' . __FILE__ . ']' . "Antes de ejecutar");
				$resultAPI = $objAPI->consumirApi('guardarDatosSwitch',$arrApi);
				//CLog::grabarLogx( '[' . __FILE__ . ']' . $resultAPI);
				//VERIFICA QUE SE HAYA EJECUTADO CORRECTAMENTE
				if($resultAPI)
				{
					//INDICADOR QUE ASIGNA ESTATUS 1, OSEA QUE ES CORRECTO Y SU DESCRIPCION
					$arrDatos['estatus'] = 1;
					$arrDatos['MensajeDescrip'] = "EXITO";

					foreach($resultAPI['registros'] as $reg)
					{
						$arrDatos['respuesta']= $reg["valor"];
					}
				}
				else
				{
					// SI EXISTE UN ERROR EN LA CONSULTA MOSTRARA EL SIGUIENTE MENSAJE
					$arrDatos['estatus'] = ERR__;
					$arrDatos['MensajeDescrip'] = "Se presento un problema al ejecutar la consulta";

					throw new Exception("tCExpedienteIdentificacion.php\tguardarDatosSwitch"."\tError al ejecutar la consulta \t"." | " . pg_errormessage() );
				}

			}
			catch (Exception $e)
			{
				//CACHA LA EXCEPCION POR LA QUE FALLO LA EJECUCION DEL QUERY Y LO MANDA COMO PARAMETRO PARA ESCRIBIR LA DESCRIPCION DEL ERROR.
				$mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
				CLog::escribirLog( '[' . __FILE__ . ']Error Consulta: '.$mensaje);
			}

			return $arrDatos;
		}
		
		
}

//Funci�n para hacer trim a los datos consultados de tipo string
function trimmer($value)
{
	if (!is_numeric($value)) {
		$value = utf8_encode(trim($value));
	}
	return $value;
}

?>
