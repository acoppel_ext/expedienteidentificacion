<?php
//consultaCurpRenapo
error_reporting(1);
include_once ('global2.php');
include_once ('CLogImpresion.php');
include_once('librerias/nusoap.php');
include_once('InvocaServicioBusTramite.php');
include_once("ObtencionRespuesta.php");
include_once("CMetodoGeneral.php");

date_default_timezone_set('America/Mexico_City');

class CServicioBusTramites
{
	public function servicioEjecutarAplicacion($idServicio, $idservidor, $parametros)
	{	

		$reg=array();
		$datos = array("respondioServicio"=>0,"folioServicioAfore"=>0,"descripcionRespuesta"=>"");
		$response = array();
		$invocaServicioBusTramite = new InvocaServicioBusTramite();
		$objGeneral= new CMetodoGeneral();

		$invocaServicioBusTramite->idServicio = $idServicio;
		$invocaServicioBusTramite->parametros = $parametros;
		$objGeneral->grabarLogx("Datos".$parametros);
		$cnxPgBusTramites =  new PDO( "pgsql:host=".IPSERVIDORBUSTRAMITES.";port=5432;dbname=".BASEDEDATOSBUSTRAMITES, USUARIOBUSTRAMITES, PASSWORDBUSTRAMITES);																																					
		
		if($cnxPgBusTramites)
		{
			$cSql = "select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio,'0')";
			
			$objGeneral->grabarLogx("consulta".$cSql);
			//Ejecuta la consulta
			$resulSet = $cnxPgBusTramites->query($cSql);
						
			//Verifica que se haya ejecutado correctamente
			if($resulSet)
			{	
				foreach($resulSet as $reg)
				{			
					$ipServidor  = $reg['ipservidor'];
					$puerto      = $reg['puerto'];
					$url = $reg['urlservicio'];
					$protocolo   =  $reg['protocolo'];
				}

				$mensaje =  "Se ejecuto la consulta Correctamente";

				$urlServicio = $protocolo . $ipServidor . ":" . $puerto . $url ;
				
				$client = new SoapClient($urlServicio, array('trace' => true, 'exceptions' => true));		
				try 
				{
		        	$response = $client->ejecutarAplicacion($invocaServicioBusTramite);
		        	$datos = $response->respuesta;
				} 
				catch (SoapFault $fault) 
				{ 
					//echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
					$datos['descripcionRespuesta']='Se presento problemas al consultar servicio cliente';
					$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error al ejecutar el servicio: SOAPFault: '.$fault->faultcode."-".$fault->faultstring);
				}
						 	
			}
			else
			{
				// Si existe un error en la consulta mostrar� el siguiente mensaje 
				//echo "Se presento un problema al ejecutar la consulta" ;
				$datos['descripcionRespuesta']='Se presento problemas al consultar servicio servidor.';
				$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error  al ejecutar la Consulta' );
				//throw new Exception("CServiciosBusTramite.php\"."\tError al ejecutar la consulta \t"."  . pg_errormessage() );
			}
		}
		else
		{
			$arrErr = $cnxPgBusTramites->errorInfo();
			$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			$arrResp['descripcion'] = "No abrio Conexion";
		}
		
		$cnxPgBusTramites = null;

		return($datos);

	}

	public function servicioObtenerRespuesta($idServicio, $folioServicioAfore)
	{	

		$reg=array();
		$datos =array("respondioServicio"=>0,"descripcionRespuesta"=>"");
		$response = array();
		$obtencionRespuesta = new ObtencionRespuesta();
		$obtencionRespuesta->folioServicioAfore = $folioServicioAfore;

		$cnxPgBusTramites =  new PDO( "pgsql:host=".IPSERVIDORBUSTRAMITES.";port=5432;dbname=".BASEDEDATOSBUSTRAMITES, USUARIOBUSTRAMITES, PASSWORDBUSTRAMITES);																																					
		
		if($cnxPgBusTramites)
		{
			$cSql="select ipservidor,puerto,urlservicio,protocolo from fnobtenerinformacionurlservicio($idServicio)";

			//Ejecuta la consulta
			$resulSet = $cnxPgBusTramites->query($cSql);
						
			//Verifica que se haya ejecutado correctamente
			if($resulSet)
			{	
				foreach($resulSet as $reg)
				{			
					$ipServidor  = $reg['ipservidor'];
					$puerto      = $reg['puerto'];
					$url = $reg['urlservicio'];
					$protocolo   =  $reg['protocolo'];
				}

				$mensaje =  "Se ejecuto la consulta Correctamente";
				$urlServicio = $protocolo . $ipServidor . ":" . $puerto . $url ;

				$client = new SoapClient($urlServicio, array('trace' => true, 'exceptions' => true));	

				try 
				{
		        	$response = $client->obtenerRespuesta($obtencionRespuesta);
		        	$datos = $response->respuesta;		        
				} 
				catch (SoapFault $fault) 
				{ 
					//echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
					$datos['descripcionRespuesta']='Se presento problemas al consultar servicio.';
					$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioObtenerRespuesta ] Error al ejecutar el servicio: SOAPFault: '.$fault->faultcode.'-'.$fault->faultstring);
				}					
				
			}
			else
			{
				// Si existe un error en la consulta mostrar� el siguiente mensaje 
				$datos['descripcionRespuesta']='Se presento problemas al consultar servicio.';
				$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioObtenerRespuesta ] Error al ejecutar la Consulta ');
				//throw new Exception("CServiciosBusTramite.php\"."\tError al ejecutar la consulta \t"."  . pg_errormessage() );
			}
		}
		else
		{
			$arrErr = $cnxPgBusTramites->errorInfo();
			$objGeneral->grabarLogx( '[' . __FILE__ . '] [ servicioObtenerRespuesta ] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			$arrResp['descripcion'] = "No abrio Conexion";
		}


		$cnxPgBusTramites = null;

		return($datos);

	}

}
?>
