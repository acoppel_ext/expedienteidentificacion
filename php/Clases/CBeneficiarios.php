<?php

/**
* 
*/
include_once("global.php");
include_once("CLog.php");
class CBeneficiarios
{
	
	public static function consultarCatalogoParentesco(){
		$datos = new stdClass();
		$arrParentesco = array();
		$cnxOdbc =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);		
		$cSql = null;		
		$i = 0;
        $idParentesco = 0;
        $descripcion = "";
		if ($cnxOdbc) 
		{
			$cSql = "SELECT idparentesco, trim(descripcion) AS descripcion FROM fnconsultarcatalogoparentesco();";			
			$resultSet = $cnxOdbc->query($cSql);			
			if ($resultSet) 
			{

				foreach ($resultSet as $resultado) 
				{														
					$arrParentesco[] = array_map('trim',$resultado);
				}
				$datos->parentescos = $arrParentesco;
				$datos->codigoRespuesta = OK__;
				$datos->descripcion = "";
			}	
			else
			{
				$datos->codigoRespuesta = ERR__;
				$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de parentescos, por favor, reporte a Mesa de Ayuda.";
				//Enviamos el error al log
				$arrErr = $cnxOdbc->errorInfo();
				CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			}
		} 
		else 
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al abrir conexi&oacute;n a la base de datos, por favor, reporte a Mesa de Ayuda.";
			 //Enviamos el error al log
			 $arrErr = $cnxOdbc->errorInfo();
			 CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}
		
		return $datos;		

	}
}

?>