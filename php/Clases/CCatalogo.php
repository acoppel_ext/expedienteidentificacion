<?php
/**
*  Clase utilizada para la consulta de catálogos
*/
include_once("global.php");
include_once("CLog.php");
include_once("Capirestexpedienteidentificacion.php");

class CCatalogo
{
	//Función utilizada para consultar el catálogo de teléfono
	public static function consultarCatalogoTelefono()
	{
		$datos = new stdClass();
		$arrTelefonos = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();
		$i = 0;
        $idTelefono = 0;
		$descripcion = "";

		$resultAPI = $objApi->consumirApi('consultarCatalogoTelefono',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrTelefonos[] = array_map('trim',$resultado);
			}
			$datos->telefono = $arrTelefonos;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de telefonos, por favor, reporte a Mesa de Ayuda.";
			//Enviamos el error al log
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoTelefono');
		}

		return $datos;
	}

	//Función para consultar los servicios de correo
	public static function consultarCatalogoCorreoElectronico()
	{
		$datos = new stdClass();
		$arrCorreos = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$resultAPI = $objApi->consumirApi('consultarCatalogoCorreoElectronico',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrCorreos[] = array_map('trim',$resultado);
			}

			$datos->correo = $arrCorreos;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de telefonos, por favor, reporte a Mesa de Ayuda.";
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoCorreoElectronico');
		}

		return $datos;
	}

    //Función para consultar el catálogo de ocupaciones
	public static function consultarCatalogoOcupacion()
	{
		$datos = new stdClass();
		$arrOcupacion = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$resultAPI = $objApi->consumirApi('consultarCatalogoOcupacion',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrOcupacion[] = array_map('trim',$resultado);
			}
			$datos->ocupacion = $arrOcupacion;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de ocupaci&oacute;n, por favor, reporte a Mesa de Ayuda.";
			//Enviamos el error al log
			$arrErr = $cnxOdbc->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
		}

		return $datos;
	}

	//Función para consultar el catálogo de actividad o giro del negocio
	public static function consultarCatalogoActividadGiroNegocio()
	{
		$datos = new stdClass();
		$arrActividadGiroNegocio = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$resultAPI = $objApi->consumirApi('consultarCatalogoActividadGiroNegocio',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrActividadGiroNegocio[] = array_map('trim',$resultado);
			}

			$datos->actividadgiro = $arrActividadGiroNegocio;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";

		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de actividad o giro del negocio, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoActividadGiroNegocio');
		}


		return $datos;
	}

	//Función para consultar el catálogo de entidades de nacimiento
	public static function consultarCatalogoEntidadNacimiento()
	{
		$datos = new stdClass();
		$arrEntidadNacimiento = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$resultAPI = $objApi->consumirApi('consultarCatalogoEntidadNacimiento',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrEntidadNacimiento[] = array_map('trim',$resultado);
			}
			$datos->entidadNacimiento = $arrEntidadNacimiento;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de actividad o giro del negocio, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoEntidadNacimiento');
		}

		return $datos;
	}

	//Función para consultar el catálogo de identificaciones oficiales
	public static function consultarCatalogoIdentificacionesOficiales($sFecha)
	{
		$datos = new stdClass();
		$arrIdentificacionOficial = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array('sFecha' => $sFecha);

		$resultAPI = $objApi->consumirApi('consultarCatalogoIdentificacionesOficiales',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrIdentificacionOficial[] = array_map('trim', $resultado);
			}
			$datos->identificacionesOficiales = $arrIdentificacionOficial;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo identificaciones oficiales, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoIdentificacionesOficiales');
		}

		return $datos;
	}

	//Función para consultar el catálogo de genero
	public static function consultarCatalogoGenero()
	{
		$datos = new stdClass();
		$arrGenero = array();
		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$resultAPI = $objApi->consumirApi('consultarCatalogoGenero',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrGenero[] = array_map('trim', $resultado);
			}
			$datos->generos = $arrGenero;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de genero, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoGenero');
		}

		return $datos;
	}

	//Función para consultar el catálogo de parentesco
	public static function consultarCatalogoParentesco()
	{
		$datos = new stdClass();
		$arrParentesco = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$i = 0;
        $idParentesco = 0;
		$descripcion = "";

		$resultAPI = $objApi->consumirApi('consultarCatalogoParentesco',$arrAPI);

		if ($resultAPI['estatus'] = 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrParentesco[] = array_map('trim',$resultado);
			}
			$datos->parentescos = $arrParentesco;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de parentescos, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoParentesco');
		}

		return $datos;

	}

	//Función que consulta el catálogo de nivel de estudios
	public static function consultarCatalogoNivelEstudio()
	{
		$datos = new stdClass();
		$arrNivelEstudio = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$i = 0;
        $idTelefono = 0;
        $descripcion = "";

		$resultAPI = $objApi->consumirApi('consultarCatalogoNivelEstudio',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{
			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrNivelEstudio[] = array_map('trim',$resultado);
			}
			$datos->nivelEstudio = $arrNivelEstudio;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de niveles de estudio, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoNivelEstudio');
		}


		return $datos;
	}

	//Función que consultar el catálogo de nacionalidades
	public static function consultarCatalogoNacionalidad()
	{
		$datos = new stdClass();
		$arrNacionalidad = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$i = 0;
        $idTelefono = 0;
		$descripcion = "";

		$resultAPI = $objApi->consumirApi('consultarCatalogoNacionalidad',$arrAPI);
		//$cSql = " SELECT idnacionalidad, descripcion  FROM fnconsultarcatalogonacionalidadei(); ";
		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrNacionalidad[] = array_map('trim',$resultado);
			}
			$datos->nacionalidad = $arrNacionalidad;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de niveles de estudio, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoNacionalidad');
		}


		return $datos;
	}

	//Función que consulta el catálogo de comprobante de domicilio
	public static function consultarCatalogoComprobanteDomicilio()
	{
		$datos = new stdClass();
		$arrComprobanteDomicilio = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();

		$i = 0;
        $idTelefono = 0;
        $descripcion = "";

		//$cSql = " SELECT idcomprobante, descripcion FROM fnconsultarcatalogocomprobantedomicilio(); ";
		$resultAPI = $objApi->consumirApi('consultarCatalogoComprobanteDomicilio',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrComprobanteDomicilio[] = array_map('trim',$resultado);
			}
			$datos->comprobanteDomicilio = $arrComprobanteDomicilio;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de niveles de estudio, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoComprobanteDomicilio');
		}

		return $datos;
	}

	public static function consultarCatalogoParentescoEi($cFolio)
	{
		$datos = new stdClass();
		$arrParentescos = array();

		$objApi = new Capirestexpedienteidentificacion();
		$arrAPI = array();
		$arrAPI = array("iFolio"=>$cFolio);

		$resultAPI = $objApi->consumirApi('obtenercatalogoparentesco',$arrAPI);

		if ($resultAPI['estatus'] == 1)
		{

			foreach ($resultAPI['registros'] as $resultado)
			{
				$arrParentescos[] = array_map('caracteresEspeciales',$resultado);
			}
			$datos->parentescos = $arrParentescos;
			$datos->codigoRespuesta = OK__;
			$datos->descripcion = "Catalogo consultado correctamente";
		}
		else
		{
			$datos->codigoRespuesta = ERR__;
			$datos->descripcion = "Ocurri&oacute; un error al consultar el cat&aacute;logo de parentescos, por favor, reporte a Mesa de Ayuda.";

			CLog::escribirLog( '[' . __FILE__ . '] Error consulta: consultarCatalogoParentescoEi');
		}

		return $datos;
	}


}
function caracteresEspeciales($resultado){
	return utf8_encode($resultado);
}
?>