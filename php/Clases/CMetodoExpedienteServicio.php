<?php
include_once("global.php");
include_once("CLog.php");
include_once("InvocaServicioBusTramite.php");

class CMetodosExpedienteServicio
{
	function unirImagenesFormatoServ($sRutaPdf,$sRutaFirmaTrab,$sRutaFirmaProm)
	{
		CLog::escribirLog("Ruta del documento->".$sRutaPdf);
		CLog::escribirLog("Ruta de la imagen Firma Trabajador-> ".$sRutaFirmaTrab);
		CLog::escribirLog("Ruta de la imagen Firma Promotor-> ".$sRutaFirmaProm);
		//Se crean tres objetos
		$imagenDocumento = new Imagick($sRutaPdf);
		$imagenFirmaTrab = new Imagick($sRutaFirmaTrab);
		$imagenFirmaProm = new Imagick($sRutaFirmaProm);
		//SE ASIGNA LA RESOLUCION DEL PDF
		$imagenDocumento->setResolution(200,200);
		
		//SE LEE EL PDF Y LAS IMAGENES A ADJUNTAR
		$imagenDocumento->readImage($sRutaPdf);
		$imagenFirmaTrab->readImage($sRutaFirmaTrab);
		$imagenFirmaProm->readImage($sRutaFirmaProm);
		
		$arrResp = array();
		$arrResp["estado"] = -1;
		
		$imagenDocumento->setImageFormat('jpeg');
		$imagenDocumento->setCompression(Imagick::COMPRESSION_JPEG);
		$imagenDocumento->setCompressionQuality(100);
		
		//Se modifica el tamaño de la firma
		$imagenFirmaTrab->resizeImage(556, 300, Imagick::FILTER_BOX, 1);
		$imagenFirmaProm->resizeImage(556, 300, Imagick::FILTER_BOX, 1);
		
		//La segunda imagen se pone arriba de la primera imagen
		$imagenDocumento->compositeImage($imagenFirmaTrab, Imagick::COMPOSITE_BUMPMAP, 920, 1760);
		$imagenDocumento->compositeImage($imagenFirmaProm, Imagick::COMPOSITE_BUMPMAP, 130, 1760);
		
		if($imagenDocumento->writeImage($sRutaPdf))
		{
			CLog::escribirLog("SE UNIERON IMAGENES CON EXITO");
			$arrResp["estado"] = 1;
		}
		else{
			CLog::escribirLog("ERROR AL UNIR FIRMAS");
		}
		return $arrResp;
	}
	
	//funcion para actualizar o insertar en la tabla stmppublicarimagenservicio y posteriormente se genere la imagen del formato pdf
	function actualizarpublicarimagenservicioei($iFolio, $iOpcion)
	{
		global $cnxDb;
		$arrResp = array();
		$arrResp["estado"] = -1;
		
		try
		{
			//Se abre una conexion
			$cnxDb =  new PDO( "pgsql:host=".IPAFOGLOB.";port=5432;dbname=".BASEDEDATOSAFOGLOB, USUARIOAFOGLOB, PASSWORDAFOGLOB);
			//$cnxDb =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);
			
			if($cnxDb)
			{
				$cSql = "select fnpublicafirmaservicios from fnpublicafirmaservicios($iFolio,$iOpcion);"; 
				CLog::escribirLog($cSql);
				//Ejecuta la consulta
				$resulSet = $cnxDb->query($cSql);

				if($resulSet)
				{
					$arrResp["estado"] = 1;
					CLog::escribirLog('[CMetodosExpedienteServicio::actualizarpublicarimagenservicioei] Estado:'.$arrResp["estado"]);
				}
			}
		}
		catch (Exception $e)
		{
			//Cacha la execcion por la que fallo la ejecucion del query y lo manda como parametro  para escribir la descriccion del error.
			 $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
			 CLog::escribirLog('[CMetodosExpedienteAfiliacion::actualizarfirmapublicacion]'. $mensaje);
		}

		$cnxDb = null;
		return $arrResp;
	}

	public static function consumirServicioEjecutarAplicacion($idServicio,$idServidor,$parametros)
	{
		$datos= array();	
		$datos = CMetodosExpedienteServicio::servicioEjecutarAplicacion($idServicio, $idServidor, $parametros);

		return ($datos);			
	}
	
	public static function servicioEjecutarAplicacion($idServicio, $idservidor, $parametros)
	{	

		$reg=array();
		$datos = array("respondioServicio"=>0,"folioServicioAfore"=>0,"descripcionRespuesta"=>"");
		$response = array();
		$invocaServicioBusTramite = new InvocaServicioBusTramite();

		$invocaServicioBusTramite->idServicio = $idServicio;
		$invocaServicioBusTramite->parametros = $parametros;
		//$objGeneral->grabarLogx("Datos".$parametros);
		$cnxPgBusTramites =  new PDO( "pgsql:host=".IPSERVIDORBUSTRAMITES.";port=5432;dbname=".BASEDEDATOSBUSTRAMITES, USUARIOBUSTRAMITES, PASSWORDBUSTRAMITES);										
		
		if($cnxPgBusTramites)
		{
			$cSql="select ipservidor,puerto,urlservicio,protocolo from fnobtenerdatosservicio($idservidor)";

			//Ejecuta la consulta
			$resulSet = $cnxPgBusTramites->query($cSql);
			
			//Verifica que se haya ejecutado correctamente
			if($resulSet)
			{	
				foreach($resulSet as $reg)
				{			
					$ipServidor  = $reg['ipservidor'];
					$puerto      = $reg['puerto'];
					$url = $reg['urlservicio'];
					$protocolo   =  $reg['protocolo'];
				}

				$mensaje =  "Se ejecuto la consulta Correctamente";

				$urlServicio = $protocolo . $ipServidor . ":" . $puerto . $url ;

				$client = new SoapClient($urlServicio,
										  array('trace' => true, 'exceptions' => true));		
				try 
				{
		        	$response = $client->ejecutarAplicacion($invocaServicioBusTramite);
		        	$datos = $response->respuesta;
				} 
				catch (SoapFault $fault) 
				{ 
					//echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
					$datos['descripcionRespuesta']='Se presento problemas al consultar servicio';
					CLog::escribirLog( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error al ejecutar el servicio: SOAPFault: '.$fault->faultcode."-".$fault->faultstring);
				}
						 	
			}
			else
			{
				// Si existe un error en la consulta mostrará el siguiente mensaje 
				//echo "Se presento un problema al ejecutar ĺa consulta" ;
				$datos['descripcionRespuesta']='Se presento problemas al consultar servicio.';
				CLog::escribirLog( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error  al ejecutar la Consulta ' );
				//throw new Exception("CServiciosBusTramite.php\"."\tError al ejecutar la consulta \t"."  . pg_errormessage() );
			}
		}
		else
		{
			$arrErr = $cnxPgBusTramites->errorInfo();
			CLog::escribirLog( '[' . __FILE__ . '] [ servicioEjecutarAplicacion ] Error: ' . $arrErr[0] . '-' . $arrErr[1] . '-' . $arrErr[2]);
			$arrResp['descripcion'] = "No abrio Conexion";
		}
		
		$cnxPgBusTramites = null;

		return($datos);

	}
	public function obtenerXML($array)
	{
		$xml = "";
		$xml = "<map>";
		foreach($array as $key => $value)
		{
			$mykey = $key;
			$myvalue= $value;
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";  	
		}
		$xml .= "</map>";
		return $xml;
	}
}

?> 
