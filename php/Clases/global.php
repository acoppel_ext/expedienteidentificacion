<?php

//$configaraciones = simplexml_load_file("../webconfig.xml");
$configaraciones = simplexml_load_file("../../conf/webconfig.xml");

$ipServidor = $configaraciones->Spa;
$sUsuario = $configaraciones->Usuario;
$sBasedeDatos = $configaraciones->Basededatos;
$sPassword = $configaraciones->Password;

$ipAdmonAfore = $configaraciones->ipAdmonAfore;
$usuarioAdmonAfore = $configaraciones->usuarioAdmonAfore;
$baseDeDatosAdmonAfore = $configaraciones->baseDeDatosAdmonAfore;
$passwordAdmonAfore = $configaraciones->passwordAdmonAfore;

$ipServidorInfx = $configaraciones->Ipinformix;
$sUsuarioInfx = $configaraciones->UsuarioInf;
$sBasedeDatosInfx = $configaraciones->BasededatosInf;
$sPasswordInfx = $configaraciones->PasswordInf;

$ipServidorBusTramites = $configaraciones->IpBusTramites;
$sUsuarioBusTramites = $configaraciones->UsuarioBusTramites;
$sBasedeDatosBusTramites = $configaraciones->BasededatosBusTramites;
$sPasswordBusTramites = $configaraciones->PasswordBusTramites;

//Llena las variables define apartir de la lectura del xml
define('OK__',		1);
define('DEFAULT__',	0);
define('ERR__',		-1);

define('ARCHIVO_CONF_WS',	'../wsmoduloafore/wsconf.dat');
define('path_wsdl', 		'../wsmoduloafore/wsModuloAfore.wsdl');

define("IPAFOGLOB", $ipServidor);
define("BASEDEDATOSAFOGLOB", $sBasedeDatos);
define("USUARIOAFOGLOB", $sUsuario);
define("PASSWORDAFOGLOB", $sPassword);

define("IPADMONAFORE", $ipAdmonAfore);
define("BASEDEDATOSADMONAFORE", $baseDeDatosAdmonAfore);
define("USUARIOADMONAFORE", $usuarioAdmonAfore);
define("PASSWORDADMONAFORE", $passwordAdmonAfore);


define("IPINFX", $ipServidorInfx);
define("BASEDEDATOSINFX", $sBasedeDatosInfx);
define("USUARIOINFX", $sUsuarioInfx);
define("PASSWORDINFX", $sPasswordInfx);

define("IPSERVIDORBUSTRAMITES", $ipServidorBusTramites);
define("BASEDEDATOSBUSTRAMITES", $sBasedeDatosBusTramites);
define("USUARIOBUSTRAMITES", $sUsuarioBusTramites);
define("PASSWORDBUSTRAMITES", $sPasswordBusTramites);

function leerParametrosWs()
{
	$sUrlWs = file_get_contents(ARCHIVO_CONF_WS);
	$sUrlWs = SUBSTR($sUrlWs, 0, - 1);
	return $sUrlWs;
}

?>
