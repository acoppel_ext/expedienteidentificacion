<?php
//CURPRENAPO
include_once ("global.php");
define('RUTA_LOGX',					'/sysx/progs/afore/log/ConsultaCurpRenapo');
define("RUTA_SALIDA","/sysx/progs/web/salida/");

class CMetodoGeneral
{	
	function __construct()
	{
		date_default_timezone_set('America/Mazatlan');
	}
	function __destruct()
	{
	}
	
	var $cnxDb;
	var $arrError;
	
	public function grabarLogx($cadLogx)
	{
		$cIpCliente = $this->getRealIP();
		$rutaLog =  RUTA_LOGX .  '-' . date("Y-m-d") . ".log"; 
		$cad = date("Y-m|H:i:s|") . getmypid() . "|" . $cIpCliente . "| " . $cadLogx . "\n";
		$file = fopen($rutaLog, "a");
		if( $file )
		{
			fwrite($file, $cad);
		}
		fclose($file);
	}

    public function getRealIP() 
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
           
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
       
        return $_SERVER['REMOTE_ADDR'];
    }  

	public function leerXmlServ($archivo, $idElem)
	{
		$res = "";
		if( file_exists($archivo) )
		{
			$datosXml = simplexml_load_file($archivo, "SimpleXMLElement",LIBXML_NOCDATA);
			if($datosXml)
			{
				echo "leyo xml 0000 <br>";
				print_r($datosXml);

				foreach($datosXml->Servidor as $elem)
				{
					echo "<br> " . $idElem .  ' Vs ' .  $elem->key . "<br>";
					print_r($elem);

					if( $elem->id == $idElem )
						$res = $elem;
				}
			}
			else
			{
				$this->grabarLogx("formato XML invalido");
				$res = false; 
			}
		} 
		else
		{
			$res = "El archivo " . $archivo . " no existe";
			$this->grabarLogx($res);
			$res = false; 
		}
		return $res;
	}

	public function obtenerConexionBdPostgres($DireccionIp, $BaseDato, $UsrBaseDato, $PasswdBaseDato)
	{
		global $cnxDb;
		if( pg_connection_status($cnxDb) != PGSQL_CONNECTION_OK )
		{
			$cadConexion = "host=" . $DireccionIp . " dbname=" . $BaseDato . " user=" . $UsrBaseDato . " password=" . $PasswdBaseDato;
			$cnxDb = pg_pconnect($cadConexion);
			if( $cnxDb )
				$this->grabarLogx('[obtenerConexionBdPostgres] Conexion establecida');
		}
		
		return $cnxDb;
	}

	public function obtenerXML($array)
	{
		$xml = "";
		$xml = "<map>";
		foreach($array as $key => $value)
		{
			$mykey = $key;
			$myvalue= $value;
			$xml .= "<entry><key>".trim($mykey)."</key>"."<value>".trim($myvalue)."</value></entry>";  	
		}
		$xml .= "</map>";
		return $xml;
	}

	/*public function leerXmlServ($archivo, $idElem)
	{
		$res = "";
		if( file_exists($archivo) )
		{
			$datosXml = simplexml_load_file($archivo, "SimpleXMLElement",LIBXML_NOCDATA);
			if($datosXml)
			{
				echo "leyo xml 0000 <br>";
				print_r($datosXml);

				foreach($datosXml->Servidor as $elem)
				{
					echo "<br> " . $idElem .  ' Vs ' .  $elem->key . "<br>";
					print_r($elem);

					if( $elem->id == $idElem )
						$res = $elem;
				}
			}
			else
			{
				$this->grabarLogx("formato XML invalido");
				$res = false; 
			}
		} 
		else
		{
			$res = "El archivo " . $archivo . " no existe";
			$this->grabarLogx($res);
			$res = false; 
		}
		return $res;
	}*/

	public function consumirServicioEjecutarAplicacion($idServicio,$idServidor,$parametros)
	{

		$servicioBusTramite = new CServicioBusTramites();	
		$datos= array();	
		$datos = $servicioBusTramite->servicioEjecutarAplicacion($idServicio, $idServidor, $parametros);

		return ($datos);			
	}

	public function consumirServicioObtenerRespuesta($idServicio,$folioServicioAfore)
	{
		$servicioBusTramite = new CServicioBusTramites();	
		$datos= array();	
		$datos = $servicioBusTramite->servicioObtenerRespuesta( $idServicio,$folioServicioAfore);

		return ($datos);	

	}

	public function obtenerCurpPromotor($iNumeroEmpleado)
	{
		global $cnxDb;
		$arrDatos['respuesta'] = 0;
		$arrDatos['curpPromotor'] = "";
		$arrDatos['descripcion'] = "";
		try
		{
			//Se abre una conexion
			$cnxBd =  new PDO( "pgsql:host=".IPSERVIDOR.";port=5432;dbname=".BASEDEDATOS, USUARIO, PASSWORD);

			//Valida si se abrio a bd
			if($cnxBd)
			{

				$cSql = 'select fnvalidapromotoractivo as curp from fnvalidapromotoractivo('.$iNumeroEmpleado.')'; 

				//Ejecuta la consulta
				$resulSet = $cnxBd->query($cSql);

				//Verifica que se haya ejecutado correctamente
				if($resulSet)
				{	
					//$arrDatos['fecha'] = $resulSet['sfechaalta'];
					foreach($resulSet as $reg)
					{						
						$arrDatos['curpPromotor'] = $reg['curp'];
					}
					 $sCurpPromotor =$arrDatos['curpPromotor'];
					
					if($sCurpPromotor != "" )
					{
						$arrDatos['respuesta'] = 1;
						$arrDatos['descripcion'] = "Curp Valido";

					}

				}
				else
				{
					$arrError = $cnxBd->errorInfo();
					// Si existe un error en la consulta mostrará el siguiente mensaje 
					$arrDatos['respuesta'] = -1;
					$arrDatos['descripcion'] = "Se presento un problema al ejecutar ĺa consulta";

					throw new Exception("CMetodosExpedienteAfiliacion.php\obtenerCurpPromotor"."\tError al ejecutar la consulta \t"." | " . $arrError[0] . '-' . $arrError[1] . '-' . $arrError[2] );
				}
				//cierra la conexion a base de datos
				
			}
			else
			{
				$arrDatos['respuesta'] = -1;
				$arrDatos['descripcion'] = "No abrio Conexion a PostGreSQL";
			}
		}
		catch (Exception $e)
		{
			//Cacha la excepcion por la que fallo la ejecucion del query y lo manda como parametro para escribir la descripcion del error.
		    $mensaje= 'Excepcion: ' . $e->getMessage() . ' Linea: ' . $e->getLine() .    '  Codigo: ' .  $e->getCode();
		}

		$cnxBd = null;
		
		return $arrDatos;
	}
}
	
?> 
