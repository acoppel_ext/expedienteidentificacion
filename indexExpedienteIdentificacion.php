<!DOCTYPE html>
<!--<meta charset="UTF-8">-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv='cache-control' content='no-cache'>
<meta http-equiv='expires' content='0'>
<meta http-equiv='pragma' content='no-cache'>
<html>
<head>
	<script type="text/javascript" src = "../jquery/jquery-1.9.1.js"></script>
	<script type="text/javascript" src = "../jquery/jquery-ui-1.10.3.custom.js"></script>
	<script type="text/javascript" src = "js/expedienteIdentificacion.js?v=2.0"></script>
	<link rel="stylesheet" type="text/css" href="../jquery/themes/redmond/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/EstilosAFOP.css">
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="stylesheet" type="text/css" media="screen" href="css/style-responsive.css" />
	<script type="text/javascript" src= "../jquery/jqGrid/js/jquery.jqGrid.src.js"></script>
	<script type="text/javascript" src="js/jquery.blockUI.js"></script>
	
	<link rel="stylesheet" type="text/css" href="../jquery/themes/redmond/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="../jquery/jqGrid/css/ui.jqgrid.css">	
	<link rel="stylesheet" type="text/css" media="screen" href="../jquery/themes/redmond/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="../jquery/jqGrid/css/ui.jqgrid.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="../jquery/jqGrid/plugins/ui.multiselect.css" />
	<script src="../jquery/ui/jquery.ui.core.js"></script>
	<script src="../jquery/ui/jquery.ui.widget.js"></script>
	<script src="../jquery/ui/jquery.ui.mouse.js"></script>
	<script src="../jquery/ui/jquery.ui.draggable.js"></script>
	<script src="../jquery/ui/jquery.ui.position.js"></script>
	<script src="../jquery/ui/jquery.ui.resizable.js"></script>
	<script src="../jquery/ui/jquery.ui.button.js"></script>
	<script src="../jquery/ui/jquery.ui.dialog.js"></script>
	<script src="../jquery/ui/jquery.ui.datepicker.js"></script>
	<script src="../jquery/ui/i18n/jquery.ui.datepicker-es.js"></script>
	<script src="../jquery/jqGrid/js/i18n/grid.locale-es.js" type="text/javascript"></script>
	<script src="../jquery/jqGrid/js/jquery.jqGrid.src.js" type="text/javascript"></script>
	<script type="text/javascript">

	$.jgrid.no_legacy_api = true;
	$.jgrid.useJSON = true;
	</script>
	<script>
		$(document).ready(function(){
			tipoConstancia();
		});
	</script>
	
<title>Expediente de Identificaci&oacute;n</title>
</head>
<body>
	<div id = "contenedorPrincipal">
		<table width = "100%">
			<tr align = "center">
				<td colspan = "6"  class = "Seccion">
					<label> Datos Generales </label>
				</td>
			</tr>
			<tr>

				<td>
					<label class="Etiqueta">Apellido Paterno</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtApellidoPaterno" maxlength="40"/>
				</td>

				<td>
					<label class="Etiqueta">NSS</label>
				</td>
				<td>
					<input type="text" class="CajaTexto"  id="txtNss" maxlength="11" />
				</td>
				<td>
					<label class="Etiqueta">Fecha Nacimiento</label>		
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtFechaNacimiento" maxlength="10" />
				</td>		
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Apellido Materno</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtApellidoMaterno" maxlength="40" />
				</td>
				<td>
					<label class="Etiqueta">CURP</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtCurp" maxlength="18" style = "width: 154px" />
				</td>
				
				<td>
					<label class="Etiqueta">Entidad Nacimiento</label>
				</td>
				<td>
					<select class="ComboBox" id="slcEntidadNacimiento" style = "width: 154px"></select>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Nombres</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtNombres" />
				</td>
				<td>
					<label class="Etiqueta">RFC</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtRfc" maxlength="13" />
				</td>
				<td>
					<label class="Etiqueta">Genero Trabajador</label>
				</td>
				<td>
					<select class="ComboBox" id="slcGeneroTrabajador" style = "width: 154px"></select>
				</td>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>
					<label class="Etiqueta">Nacionalidad</label>
				</td>
				<td>
					<select class="ComboBox" id="slcNacionalidad" style = "width: 154px"></select>
				</td>
			</tr>
				
			</tr>
		</table>
		<table width="100%">
			<tr align = "center">
				<td colspan = "6"  class = "Seccion">
					<label> Nivel de Estudio, Ocupaci&oacute;n y Actividad econ&oacute;mica </label>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Nivel de Estudios</label>
				</td>
				<td>
					<select class="ComboBox" id="slcNivelEstudios" style = "width: 154px"></select> 
				</td>
				<td>
					<label class="Etiqueta">Ocupaci&oacute;n o Profesi&oacute;n</label>
				</td>
				<td>
					<select class="ComboBox" id="slcOcupacionProfesion" style = "width: 154px"></select> 
				</td>
				<td>
					<label class="Etiqueta">Actividad o Giro del Negocio</label>
				</td>
				<td>
					<select class="ComboBox" id="slcActividadNegocio" style = "width: 154px"></select> 
				</td>
			</tr>
		</table>
		<table width="100%">
			<tr align = "center">
				<td colspan = "7"  class = "Seccion">
					<label> Datos de Contactaci&oacute;n</label>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Tel&eacute;fono 1</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtTel1" maxlength="10" />
				</td>
				<td>
					<select class="ComboBox" id="slcTel1" style = "width: 154px"></select>
				</td>
				<td>
					<label class="Etiqueta">Correo Electronico</label>
				</td>
				<td>
					<table>
						<tr>
							<td><input type="text" class="CajaTexto" id="txtCorreoElectronico" onchange="habilitaConfirmacionCorreo()" maxlength="99" /></td>
							<td><label id="lblA">@</label></td>
						</tr>
					</table>
				</td>
				<td id="ocultarotro">
					<select class="ComboBox" id="slcCorreoElectronico" style = "width: 154px" onchange="habilitaConfirmacionCorreo()">
						
					</select>
				</td>
				<td id="tdSwitch1" style="visibility: hidden;">
					<label class="switchBtn">
					<input type="checkbox" name="checkbox1" value="1" id="Edo" onclick="SwitchEdo(this)">
					<div class="slide round"></div>
					</label>
					<label class="leyendaDesc" id="ley1">Acepto recibir mi estado de cuenta a mi correo electrónico</label>
					<input type="text" class="CajaTexto" id="txtOtroDominio2" style="visibility: hidden;" Placeholder="ESPECIFICAR DOMINIO"/>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Tel&eacute;fono 2</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtTel2" maxlength="10" />
				</td>
				<td>
					<select class="ComboBox" id="slcTel2" style = "width: 154px"></select>
				</td>
				<!--
				<td>
					<label class="Etiqueta mobile">Confirmar Correo</label>
				</td>
				<td>
					<table>
						<tr>
							<td><input type="text" class="CajaTexto mobile" id="txtCorreoElectronicoConfirm" maxlength="40" /></td>
							<td><label class="mobile" id="lblA">@</label></td>
						</tr>
					</table>	
				</td>
				<td>
					<select class="ComboBox mobile" id="slcCorreoElectronicoConfirm" style = "width: 154px"></select>
				</td>
				<td>
					<input type="text" class="CajaTexto mobile" id="txtOtroDominioConfirm"  Placeholder="ESPECIFICAR DOMINIO"/>
				</td>
				-->
				<!-- ESTA MADRE SE HIZO PARA ACOMODAR EL SWITCH
				<td style="visibility: hidden;">
						<label class="Etiqueta">Correo Electronico</label>
					</td>
					<td style="visibility: hidden;">
						<table>
							<tr>
								<td><input type="text" class="CajaTexto" id="txtCorreoElectronico" maxlength="40" /></td>
								<td><label id="lblA">@</label></td>
							</tr>
						</table>
					</td>
					<td id="otrocorreoconfirma" style="visibility: hidden;">
						<select class="ComboBox" id="slcCorreoElectronicoconfirma" style = "width: 154px"></select>
					</td>
					<td id="mostrarotrocorreoconfirma" style="display:none;">
						<input type="text" class="CajaTexto" id="tomarcorreoconfirma"/>
					</td>
					-->
				
				<td>
					<label class="Etiqueta">Confirmar Correo</label>
				</td>
				<td id="confirm1" style="visibility:hidden;">
					<table>
						<tr>
							<td><input type="text" class="CajaTexto" id="txtCorreoElectronicoConfirma" maxlength="99" onchange="validarConfirmacionCorreo()"/></td>
							<td><label id="lblA">@</label></td>
						</tr>
					</table>	
				</td>
				<td  id="confirm2" style="visibility:hidden;">
					<select class="ComboBox" id="slcCorreoElectronicoConfirma" style = "width: 154px" onblur="validarConfirmacionCorreo()">
						
					</select>
				</td>
				
				<td id="tdSwitch2" style="visibility: hidden;">
					<label class="switchBtn">
					<input type="checkbox" name="checkbox2" value="1" id="Not" onclick="switchNoti(this)">
					<div class="slide round"></div>
					</label>
					<label class="leyendaDesc" id="ley2">Acepto recibir notificaciones sobre mi afore a mi correo</label>
					<input type="text" class="CajaTexto" id="txtOtroDominioConfirma" style="visibility: hidden;" Placeholder="ESPECIFICAR DOMINIO"/>
				</td>
			</tr>
			<tr>
				<td style="visibility: hidden;">
					<label class="Etiqueta">Tel&eacute;fono 3</label>
				</td>
				<td style="visibility: hidden;">
					<input type="text" class="CajaTexto" id="txtTel2" maxlength="10" />
				</td>
				<td style="visibility: hidden;">
					<select class="ComboBox" id="slcTel2" style = "width: 154px"></select>
				</td>
				<td>
					<label class="Etiqueta mobile">Confirmar Correo</label>
				</td>
				<td>
					<table>
						<tr>
							<td><input type="text" class="CajaTexto mobile" id="txtCorreoElectronicoConfirm" maxlength="99" /></td>
							<td><label class="mobile" id="lblA">@</label></td>
						</tr>
					</table>	
				</td>
				<td>
					<select class="ComboBox mobile" id="slcCorreoElectronicoConfirm" style = "width: 154px"></select>
				</td>
				
				<td>
					<input type="text" class="CajaTexto mobile" id="txtOtroDominioConfirm"  Placeholder="ESPECIFICAR DOMINIO"/>
				</td>
				<td colspan="2" style="display:none; text-align:center;" id="primermensaje" >
						<label class="" style="font-family: Lucida Sans Unicode;font-size: 13px;font-weight: bold;">Correo ingresado no cumple con validaciones</label>
				</td>
				<td colspan="2" style="display:none;" id="segundomensaje">
						<label class="" style="font-family: Lucida Sans Unicode;font-size: 13px; margin-left:22px; font-weight: bold;">Correo no cumple con validaciones de Dominio seleccionado</label>
				</td>
				<td colspan="2" style="display: none;" id="tercermensaje">
						<label class="" style="font-family: Lucida Sans Unicode;font-size: 13px; margin-left:22px; font-weight: bold;">Correo electrónico no coincide, favor de revisar los campos</label>
				</td> 
			</tr>
		</table>
		<table width="100%">
			<tr align = "center">
				<td colspan = "6"  class = "Seccion">
					<label>Domicilio</label>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">C&oacute;digo Postal(*)</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtCodigoPostal" maxlength="5" value = ''/>
					<button id="btnBuscarCodigoPostal"></button>
				</td>
				<td>
					<label class="Etiqueta"´>Pa&iacute;s(*)</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtPais" value="M&Eacute;XICO" />
				</td>
				<td>
					<label class="Etiqueta">Entidad Federativa(*)</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtEntidadFederativa" />
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Delegaci&oacute;n o Municipio(*)</label>
				</td>
				<td>
					<input class="CajaTexto" id="txtDeleMuni" />
				</td>
				<td>
					<label class="Etiqueta">Ciudad</label>
				</td>
				<td>
					<input class="CajaTexto" id="txtCiudad"/>
				</td>
				<td>
					<label class="Etiqueta">Colonia(*)</label>
				</td>
				<td>
					<input class="CajaTexto" id="txtColonia" />
				</td>

			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Calle(*)</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtCalle" maxlength="40" />
				</td>
				<td>
					<label class="Etiqueta">N&uacute;mero Exterior(*)</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtNumExterior" maxlength="10" />
				</td>
				<td>
					<label class="Etiqueta">N&uacute;mero Interior</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtNumInterior" maxlength="10" />
				</td>
			</tr>
		</table>
	<!--	<table width="100%">
			<tr align = "center">
				<td colspan = "6"  class = "Seccion">
					<label>Domicilio Laboral</label>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">C&oacute;digo Postal</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtCodigoPostalLaboral"/>
					<button id="btnBuscarCodigoPostalLaboral"></button> 
				</td>
				<td>
					<label class="Etiqueta"´>Pa&iacute;s</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtPaisLaboral" value="M&Eacute;XICO"/>
				</td>
				<td>
					<label class="Etiqueta">Entidad Federativa</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtEntidadFederativaLaboral" />
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Delegaci&oacute;n o Municipio</label>
				</td>
				<td>
					<input class="CajaTexto" id="txtDeleMuniLaboral" />
				</td>
				<td>
					<label class="Etiqueta">Ciudad</label>
				</td>
				<td>
					<input class="CajaTexto" id="txtCiudadLaboral"/>
				</td>
				<td>
					<label class="Etiqueta">Colonia</label>
				</td>
				<td>
					<input class="CajaTexto" id="txtColoniaLaboral"/>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta">Calle</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtCalleLaboral" maxlength="40"/>
				</td>
				<td>
					<label class="Etiqueta">N&uacute;mero Exterior</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtNumExteriorLaboral" />
				</td>
				<td>
					<label class="Etiqueta">N&uacute;mero Interior</label>
				</td>
				<td>
					<input type="text" class="CajaTexto" id="txtNumInteriorLaboral" />
				</td>
			</tr>
		</table> -->
		<div id = "divSolicitante" >
		</div>
		
	<table width="100%">
		<div>
			<tr align = "center" id="InformacionDocumentacion">
				<td colspan="6" class = "Seccion">
					<label> Informaci&oacute;n sobre la Documentaci&oacute;n </label>
				</td>
			</tr>
			<tr>
				<td>
					<label class="Etiqueta" id="lblIdentificacionOficial">Identificaci&oacute;n Oficial</label>
				</td>
				<td>
					<select class="ComboBox" id="slcIdentificacionOficial" style = "width: 154px"></select> 
				</td>
			<tr>
				<td >
					<label class="Etiqueta" id="lblComprobanteDomicilio">Comprobante de Domicilio</label>
				</td>
				<td >
					<select class="ComboBox" id="slcComprobanteDomicilio" style = "width: 154px"></select> 
				</td>
				<td >
					<label class="Etiqueta" id="lblFechaComprobante">Fecha del Comprobante</label>
				</td>
				<td >
					<input type="text" class="CajaTexto" id="dtFechaComprobante" maxlength="10" />
				</td>
			</tr>
		</div>
		</table>
		<td>
			<label class="Etiqueta" id = 'Obligatorios'>(*)Datos Obligatorios</label>
		</td>
		</br>
		</br>
		</br>
		<div id= "divCheckPromotor">
		</div>
		<div id="divPromotor">
		</div>
		<br>
			<input type="checkbox" id="chBoxTrabajadorfallecido"/>
			<label class="Etiqueta" id="lblRegistroFallecido">Registro de un trabajador fallecido</label>
		<br>
		<table width="100%">
			<tr align="right">

			<td colspan="5">
				<input id="btnCancelar" type="button"  value="Cancelar"/>
				<input id="btnCapturaBeneficiario" type="button"  value="Capturar Beneficiarios"/>
				<input id="btnGuardar" type="button"  value="Guardar"/>
				<input id="btnImprimir" type="button"  value="Imprimir Formato"/>
				<input id="btnDigitalizar" type="button"  value="Digitalizar"/>
			</td>
			</tr>
		</table>
		<table width="100%">
			<tr align="right">
				<td colspan="1">
					<div align="right"  id = "divRespuestaRenapo">
						<input style="cursor:default" autocomplete="off" type = "image" src="imagenes/Renapo.png" width = "132" height = "28"></input>
					</div>
				</td>
			</tr>
		</table>
	</div>
	
	<div id="snackbar">Some text some message..</div>
	<div id="carga"></div>
	<div id="divMensaje"></div>
	<div id="divPdf"></div>	
	<div id="divOculto" >	
		<input type="text" id="txtCpOculto" />
		<input type="text" id="txtColoniaOculto" />
		<input type="text" id="txtDeleMuniOculto" />
		<input type="text" id="txtEntidadFederativaOculto" />
		<input type="text" id="txtCiudadOculto" />

		<input type="text" id="txtCpLaboralOculto" />
		<input type="text" id="txtColoniaLaboralOculto" />
		<input type="text" id="txtDeleMuniLaboralOculto" />
		<input type="text" id="txtEntidadFederativaLaboralOculto" />
		<input type="text" id="txtCiudadLaboralOculto" />
		<input type="text" id="txtNssOculto" maxlength="11" />
		
	</div>
	<div id="divApplet"></div>
	<style>
	/* The snackbar - position it at the bottom and in the middle of the screen */
		#snackbar {
		visibility: hidden; /* Hidden by default. Visible on click */
		min-width: 250px; /* Set a default minimum width */
		margin-left: -125px; /* Divide value of min-width by 2 */
		background-color: #DF0000; /* Black background color */
		color: #fff; /* White text color */
		text-align: center; /* Centered text */
		border-radius: 2px; /* Rounded borders */
		padding: 16px; /* Padding */
		position: fixed; /* Sit on top of the screen */
		z-index: 1; /* Add a z-index if needed */
		left: 50%; /* Center the snackbar */
		bottom: 30px; /* 30px from the bottom */
		}

		/* Show the snackbar when clicking on a button (class added with JavaScript) */
		#snackbar.show {
		visibility: visible; /* Show the snackbar */
		/* Add animation: Take 0.5 seconds to fade in and out the snackbar.
		However, delay the fade out process for 2.5 seconds */
		-webkit-animation: fadein 0.5s, fadeout 0.5s 2.5s;
		animation: fadein 0.5s, fadeout 0.5s 2.5s;
		}

		/* Animations to fade the snackbar in and out */
		@-webkit-keyframes fadein {
		from {bottom: 0; opacity: 0;}
		to {bottom: 30px; opacity: 1;}
		}

		@keyframes fadein {
		from {bottom: 0; opacity: 0;}
		to {bottom: 30px; opacity: 1;}
		}

		@-webkit-keyframes fadeout {
		from {bottom: 30px; opacity: 1;}
		to {bottom: 0; opacity: 0;}
		}

		@keyframes fadeout {
		from {bottom: 30px; opacity: 1;}
		to {bottom: 0; opacity: 0;}
		}
	</style>
</body>
</html>